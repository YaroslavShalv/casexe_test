Yii 2 Template
===============================

Установка:
===============================

1. composer install
2. php init
3. php yii migrate
4. php install --username=admin --password=qwerty --email=admin@admin.ad

> php init можно автоматизировать, чтоб команда не запрашивала действий.
> 
> Для этого нужно добавить следующие параметры: 
>
> --env={Production|Development} - окружение
>
> --projectName={projectName} - название проекта
>
> --adminxNumber={number} - номер админки
>
> --all=1 - перезаписать все существующие файлы 


Структура директорий
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```

<?php
return [
    'class'           => \yii\web\UrlManager::class,
    'enablePrettyUrl' => true,
    'showScriptName'  => false,
    'rules'           => [
        'uploads/<width:\d+>x<height:\d+>/<model:[\w_]+>/<field:[\w-]+>/<dynamicPath:[\d\/]+>/<file:[\w-]+>.<ext:\w+>' => 'system/uploads',

        '<module:rest>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
        '<module:rest>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',

        '<controller:[\w\d-_//]+>/<action:[\w\d-_//]+>' => '<controller>/<action>',
    ]
];
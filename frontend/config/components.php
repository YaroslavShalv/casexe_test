<?php

return [
    'request'     => [
        'parsers' => [
            'application/json' => \yii\web\JsonParser::class,
        ]
    ],
    'user'        => [
        'identityClass'   => \common\models\User::class,
        'enableAutoLogin' => true,
        'loginUrl'        => null,
        'identityCookie'  => [
            'name' => '_frontendUser', // unique for frontend
            'path' => '/frontend/web'  // correct path for the frontend app.
        ],
    ],
    'session'     => [
        'name' => '_frontendSessionId', // unique for frontend
    ],
    'authManager' => [
        'class'           => \yii\rbac\DbManager::class,
        'itemTable'       => '{{%user_auth_item}}',
        'itemChildTable'  => '{{%user_auth_item_child}}',
        'assignmentTable' => '{{%user_auth_assignment}}',
        'ruleTable'       => '{{%user_auth_rule}}',
    ],
    'urlManager'  => require(__DIR__ . '/urlManager.php'),
];
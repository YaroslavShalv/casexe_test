<?php

namespace frontend\modules\rest;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\rest\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

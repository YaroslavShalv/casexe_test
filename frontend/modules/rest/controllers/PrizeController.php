<?php

namespace frontend\modules\rest\controllers;


use common\components\PrizeGenerator;
use common\helpers\HttpError;
use frontend\modules\rest\components\SiteRestController;

/**
 * Class PrizeController
 * @package frontend\modules\rest\controllers
 */
class PrizeController extends SiteRestController
{
    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * Получение случайного приза
     */
    public function actionGet()
    {
        $component = new PrizeGenerator();

        return $component->generatePrize();
    }


    /**
     * @throws \yii\web\NotFoundHttpException
     * Денежный приз конвертируется в баллы лояльности
     */
    public function actionConvert()
    {
        $id = \Yii::$app->request->post('id');
        if(empty($id)) {
            HttpError::the404('Приз не найден');
        }

        $component = new PrizeGenerator();

        $component->addConvertJob($id);
    }


    /**
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException Подтверждение приза
     */
    public function actionApprove()
    {
        $id = \Yii::$app->request->post('id');
        if(empty($id)) {
            HttpError::the404('Приз не найден');
        }

        $component = new PrizeGenerator();

        if($component->addApprove($id)) {
            return ['response' => 'success'];
        } else {
            HttpError::the400('Ошибка сохранения');
        }
    }


    /**
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDiscard()
    {
        $id = \Yii::$app->request->post('id');
        if(empty($id)) {
            HttpError::the404('Приз не найден');
        }

        $component = new PrizeGenerator();

        if($component->discard($id)) {
            return ['response' => 'success'];
        } else {
            HttpError::the400('Ошибка сохранения');
        }
    }
}
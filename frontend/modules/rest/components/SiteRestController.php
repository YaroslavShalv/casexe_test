<?php

namespace frontend\modules\rest\components;

use yii\filters\VerbFilter;
use yii\web\Controller;
use yii;
use yii\web\Response;

class SiteRestController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [

                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'only' => null,
                'rules' => [
                    // allow only AJAX requests using the post Method
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return !Yii::$app->user->isGuest; //Yii::$app->request->getIsAjax() &&
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }


}

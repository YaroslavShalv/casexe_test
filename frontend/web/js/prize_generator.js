let prizeGenerator = new PrizeGenerator();

function PrizeGenerator() {

    this.id = null;

    this.init = function () {
        let self = this;

        $("#randomPrize").on('click', function () {
            console.log('test');
            self.getRandomPrize();
        });

        $('#convert').on('click', function () {
           self.convertPrize();
        });

        $('#approve').on('click', function () {
           self.approvePrize();
        });

        $('#discard').on('click', function () {
           self.discardPrize();
        });
    };


    this.convertPrize = function () {
      let self = this;

      self.send('/rest/prize/convert', 'POST', {id: self.id}, function(data) {
            console.log(data);
      });

      self.hideModal();
    };


    this.approvePrize = function () {
      let self = this;

      self.send('/rest/prize/approve', 'POST', {id: self.id}, function(data) {
            console.log(data);
      });

      self.hideModal();
    };

    this.discardPrize = function () {
      let self = this;

      self.send('/rest/prize/discard', 'POST', {id: self.id}, function(data) {
            console.log(data);
      });

      self.hideModal();
    };


    this.showConversionButton = function () {
        $('#convert').removeClass('hidden');
    };

    this.hideConversionButton = function () {
        $('#convert').addClass('hidden');
    };

    this.showModal = function () {
        $('#randomPrizeModal').modal('show')
    };

    this.hideModal = function () {
        $('#randomPrizeModal').modal('hide')
        self.hideConversionButton();
    };


    this.getRandomPrize = function () {
        let self = this;

        self.send('/rest/prize/get', 'GET', {}, function (data) {
            self.id = data.id;

            let text = '';
            if(data.prize_key == "itemPrize") {
                text = "Вы выигали приз:" + data.element_name;
            } else if(data.prize_key == "bonusPoints") {
                text = "Вы выигради " +  data.amount + "Бонусных балла";
            } else if(data.prize_key == "moneyPrize") {
                text = "Вы выигради денежный приз в размере " +  data.amount + " крон";
                self.showConversionButton();
            }
            $('#text').text(text);
            self.showModal()
        })
    };



    this.send = function (url, method, params, callback) {
        $.ajax({
            method: method,
            url: url,
            data: params,
            dataType: 'json'
        })
        .done(function( data ) {
            callback(data);
        })
        .fail(function (msg) {
            alert('something wrong')
        });
    };
    
}


$(document).ready(function () {
   prizeGenerator.init(); 
});
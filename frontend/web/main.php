<?php

use common\components\settings\Setting;
use yii\helpers\Url;

$title           = Setting::get('general.title');
$description     = Setting::get('general.description');
$image           = Url::to(Setting::get('general.image'), true);
$facebook_app_id = Setting::get('general.facebook_app_id');
$counters        = Setting::get('general.counters');

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <meta name="title" content="<?= $title ?>" />
    <meta name="description" content="<?= $description ?>" />
    <link rel="image_src" href="<?= $image ?>" />
    <meta property="og:title" content="<?= $title ?>" />
    <meta property="og:url" content="<?= Url::to('/', true) ?>" />
    <meta property="og:description" content="<?= $description ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?= $title ?>" />
    <meta property="og:image" content="<?= $image ?>" />
    <meta property="fb:app_id" content="<?= $facebook_app_id ?>" />

    <meta name="mrc__share_title" content="<?= $title ?>">
    <meta name="mrc__share_description" content="<?= $description ?>" />

    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0, minimal-ui" />
</head>
<body>
<?= $counters ?>
</body>
</html>
<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <a id="randomPrize" class="btn btn-lg btn-success" href="#">Выдать случайный приз</a>
</div>


<div class="modal fade" id="randomPrizeModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вы выиграли приз!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="text"></div>
            </div>
            <div class="modal-footer">
                <button id="discard" type="button" class="btn btn-secondary" data-dismiss="modal">Отказатся</button>
                <button id="approve" type="button" class="btn btn-primary">Принять</button>
                <button id="convert" type="button" class="btn btn-success hidden">Конвертировать в баллы</button>
            </div>
        </div>
    </div>
</div>
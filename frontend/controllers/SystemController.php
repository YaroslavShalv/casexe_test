<?php

namespace frontend\controllers;

use common\helpers\HttpError;
use common\helpers\Param;
use common\helpers\UploadsAction;
use common\models\User;
use frontend\base\controllers\Controller;
use Yii;
use yii\web\ErrorAction;

/**
 * Class SystemController
 *
 * @package frontend\controllers
 */
class SystemController extends Controller
{
    /** @inheritdoc */
    public function actions()
    {
        return [
            'error'   => [
                'class' => ErrorAction::class,
            ],
        ];
    }

}
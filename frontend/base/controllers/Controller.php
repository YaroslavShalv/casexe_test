<?php

namespace frontend\base\controllers;

use frontend\assets\MainAsset;
use yii\web\Controller as YiiController;

/**
 * Frontend base controller
 *
 * @package frontend\base\controllers
 */
abstract class Controller extends YiiController
{
    /** @var MainAsset */
    protected $mainAsset;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->mainAsset               = $this->view->registerAssetBundle(MainAsset::class);
        $this->view->params['baseUrl'] = $this->mainAsset->baseUrl;

        return parent::beforeAction($action);
    }
}
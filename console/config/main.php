<?php
$params = array_merge(require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'));

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'components'          => require(__DIR__ . '/components.php'),
    'modules'             => require(__DIR__ . '/modules.php'),
    'params'              => $params,
    'controllerMap'       => [
        'migrate' => [
            'class'               => \yii\console\controllers\MigrateController::class,
            'templateFile'        => '@console/base/views/migration.php',
            'migrationNamespaces' => [
                'common\migrations',
                'backend\modules\impex\migrations',
            ],
        ],
    ],
];

<?php

return [
    'admin'           => [
        'class'           => \yii\web\User::class,
        'identityClass'   => \backend\models\Admin::class,
        'enableAutoLogin' => true,
    ],
    'authManager'     => [
        'class'           => \yii\rbac\DbManager::class,
        'itemTable'       => '{{%admin_auth_item}}',
        'itemChildTable'  => '{{%admin_auth_item_child}}',
        'assignmentTable' => '{{%admin_auth_assignment}}',
        'ruleTable'       => '{{%admin_auth_rule}}',
    ],
    'authUserManager' => [
        'class'           => \yii\rbac\DbManager::class,
        'itemTable'       => '{{%auth_item}}',
        'itemChildTable'  => '{{%auth_item_child}}',
        'assignmentTable' => '{{%auth_assignment}}',
        'ruleTable'       => '{{%auth_rule}}',
    ],
    'user'            => [
        'class'           => \yii\web\User::class,
        'identityClass'   => \common\models\User::class,
        'enableAutoLogin' => true,
    ],
    'cache'           => [
        'class'     => \yii\caching\FileCache::class,
        'cachePath' => '@frontend/runtime/cache'
    ],
];
<?php

/**
 * Class m000000_000004_create_table_setting
 */
class m000000_000004_create_table_setting extends \common\db\Migration
{
    /**
     * @var string Table name for migrate
     */
    protected $_tableName = '{{%setting}}';

    protected $_table_status_key = false;
    protected $_table_created_at = false;
    protected $_table_updated_at = false;

    /**
     * Migration Up
     */
    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'section'  => $this->string(50)->notNull(),
            'key'      => $this->string(50)->notNull(),
            'name'     => $this->string(100)->notNull(),
            'hint'     => $this->string(),
            'value'    => $this->text(),
            'type_key' => $this->smallInteger(2)->notNull()->defaultValue(0),
            'position' => $this->integer()->notNull()->defaultValue(1),
            'variants' => $this->text(),
            'rules'    => $this->text(),
        ], $this->_tableOptions);

        $this->addPrimaryKey('setting_pk1', $this->_tableName, ['section', 'key']);
    }

    /**
     * Migration down
     */
    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `prizes_history`.
 */
class m181205_225335_create_prizes_history_table extends Migration
{
    protected $_tableName = '{{%prizes_history}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->comment("ID пользователя"),
            'prize_key' => $this->string(31)->comment('Тип приза'),
            'amount' => $this->integer()->comment('Количество'),
            'status_key' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->createIndex( 'idx-prizes_history-user_id', $this->_tableName, 'user_id');
        $this->createIndex( 'idx-prizes_history-prize_key', $this->_tableName, 'prize_key');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-prizes_history-user_id', $this->_tableName);
        $this->dropIndex('idx-prizes_history-prize_key', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items`.
 */
class m181205_220234_create_items_table extends Migration
{
    protected $_tableName = '{{%items}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey(),
            'prize' => $this->string(255)->comment('Назване приза'),
            'status_key' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->insert($this->_tableName, [
            'prize' => 'Утюг',
            'status_key' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'prize' => 'Телевизор',
            'status_key' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'prize' => 'Пылесос',
            'status_key' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'prize' => 'Кофеварка',
            'status_key' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'prize' => 'Видеокарта GTX 660 Ti',
            'status_key' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}

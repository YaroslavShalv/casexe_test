<?php

use yii\db\Migration;

/**
 * Handles the creation of table `loyality_points`.
 */
class m181205_225354_create_loyality_points_table extends Migration
{
    protected $_tableName = '{{%loyality_points}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer()->comment('ID пользователя'),
            'points' => $this->integer()->comment('Очки лояльности'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);


        $this->createIndex( 'idx-loyality_points-user_id', $this->_tableName, 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-loyality_points-user_id', $this->_tableName);
        $this->dropTable($this->_tableName);
    }
}

<?php

/**
 * Class m000000_000005_add_general_settings
 */
class m000000_000005_add_general_settings extends \common\components\settings\SettingsMigration
{
    /**
     * Массив новых настроек для внесения в базу
     *
     * @var array
     */
    protected $_rows = [
        [
            self::FIELD_SECTION => 'general',
            self::FIELD_KEY     => 'title',
            self::FIELD_NAME    => 'Заголовок',
            self::FIELD_HINT    => 'Заголовок для шаринга',
            self::FIELD_VALUE   => '{ProjectTemplate}', #не менять это значение!
            self::FIELD_RULES   => [
                ['string', 'max' => 255],
            ]
        ],
        [
            self::FIELD_SECTION => 'general',
            self::FIELD_KEY     => 'description',
            self::FIELD_NAME    => 'Описание',
            self::FIELD_HINT    => 'Описание для шаринга',
            self::FIELD_VALUE   => 'Описание',
            self::FIELD_TYPE    => self::TYPE_TEXTAREA,
            self::FIELD_RULES   => [
                ['string', 'max' => 255],
            ]
        ],
        [
            self::FIELD_SECTION => 'general',
            self::FIELD_KEY     => 'image',
            self::FIELD_NAME    => 'Картинка',
            self::FIELD_HINT    => 'Картинка для шаринга',
            self::FIELD_VALUE   => '',
            self::FIELD_TYPE    => self::TYPE_FILE,
            self::FIELD_RULES   => [
                [
                    'image',
                    'extensions' => 'png, jpg, jpeg, gif',
                    'minWidth'   => 50,
                    'maxWidth'   => 3500,
                    'minHeight'  => 50,
                    'maxHeight'  => 3500,
                ],
            ]
        ],
        [
            self::FIELD_SECTION => 'general',
            self::FIELD_KEY     => 'facebook_app_id',
            self::FIELD_NAME    => 'ID приложения',
            self::FIELD_HINT    => 'ID Facebook приложения для шаринга',
            self::FIELD_VALUE   => '',
            self::FIELD_TYPE    => self::TYPE_TEXT,
            self::FIELD_RULES   => [
                ['string', 'max' => 255],
            ]
        ],
        [
            self::FIELD_SECTION => 'general',
            self::FIELD_KEY     => 'counters',
            self::FIELD_NAME    => 'Счетчики',
            self::FIELD_HINT    => 'Счетчики',
            self::FIELD_VALUE   => '',
            self::FIELD_TYPE    => self::TYPE_TEXTAREA,
            self::FIELD_RULES   => [
                ['string'],
            ]
        ],
    ];
}

<?php

use yii\db\Migration;

/**
 * Handles adding item_id to table `prizes_history`.
 */
class m181206_003939_add_item_id_column_to_prizes_history_table extends Migration
{
    protected $_tableName = '{{%prizes_history}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->_tableName, 'prize_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->_tableName, 'prize_id');
    }
}

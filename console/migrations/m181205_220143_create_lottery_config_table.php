<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lottery_config`.
 */
class m181205_220143_create_lottery_config_table extends Migration
{
    protected $_tableName = '{{%lottery_config}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'key' => $this->string(255)->comment('Ключ'),
            'value' => $this->string(255)->comment('Значение'),
            'description' => $this->string(255)->comment('Описание'),
            'status_key' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->insert($this->_tableName, [
            'key' => 'total_money',
            'value' => '200000',
            'description' => 'Общее количество денег для розыгрыша',
            'status_key' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'key' => 'max_money_prize',
            'value' => '2000',
            'description' => 'Максимальное количество денег, которое может выиграть пользователь.',
            'status_key' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'key' => 'min_money_prize',
            'value' => '20',
            'description' => 'Минимальное количество денег, которое может выиграть пользователь',
            'status_key' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'key' => 'loyaloty_point_ratio',
            'value' => '10',
            'description' => 'Коэфициент пересчёта денег в очки',
            'status_key' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'key' => 'min_point_prize',
            'value' => '200',
            'description' => 'Минимальное количество очков лояльности',
            'status_key' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->insert($this->_tableName, [
            'key' => 'max_points_prize',
            'value' => '20000',
            'description' => 'Максимальное количество очков лояльности',
            'status_key' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `converting_job`.
 */
class m181206_023518_create_converting_job_table extends Migration
{
    protected $_tableName = '{{%converting_job}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->_tableName, [
            'id' => $this->primaryKey()->unsigned(),
            'user_id' => $this->integer(),
            'amount' => $this->integer(),
            'result' => $this->integer(),
            'status_key' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}

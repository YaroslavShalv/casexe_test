<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use common\db\Migration;

/**
* Миграция <?= $className ?>
*/
class <?= $className ?> extends Migration
{
    protected $_tableName = '{{%}}';

    /**
    * @return bool|void
    */
    public function safeUp()
    {
    }

    /**
    * @return bool|void
    */
    public function safeDown()
    {
        parent::safeDown();
    }
}
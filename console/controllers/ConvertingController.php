<?php

namespace console\controllers;

use common\components\ConvertingComponent;
use common\components\LoyalityPoints;
use common\models\ConvertingJob;
use yii\console\Controller;

/**
 * Class ConvertingController
 *
 * @package console\controllers
 */
class ConvertingController extends Controller
{

    public function actionConvert()
    {
        /** @var ConvertingJob[] $models */
        $models = ConvertingJob::find()->where(['status_key' => ConvertingJob::STATUS_NEW])->all();

        $convertinComponent = new ConvertingComponent();
        $loyalotyPoints = new LoyalityPoints();

        foreach($models as $model) {
            $model->result = $convertinComponent->convert($model->amount);
            $model->status_key = ConvertingJob::STATUS_WORKED;
            $loyalotyPoints->addBonus($model->result, $model->user_id);
            $model->save();
        }
    }

}
<?php

namespace console\controllers;


use common\models\prizes\MoneyPrize;
use common\models\PrizesHistory;
use yii\console\Controller;

/**
 * Class ConvertingController
 *
 * @package console\controllers
 */
class MoneyController extends Controller
{

    public function actionToBank()
    {
        $model = new MoneyPrize();

        /** @var PrizesHistory[] $prizesHistory */
        $prizesHistory = PrizesHistory::find()
            ->where(['status_key' => PrizesHistory::SEND_TO_BANK])
            ->limit(5)
            ->all();

        foreach ($prizesHistory as $prizeHistory) {
            $result = $model->sendToBank($prizeHistory->amount, $prizeHistory->user_id);
            if($result) {
                $prizeHistory->status_key = PrizesHistory::STATUS_ENABLED;
                $prizeHistory->save();
            }
        }
    }
}
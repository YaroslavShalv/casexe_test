<?php

namespace console\controllers;

use backend\models\Admin;
use common\helpers\WordHelper;
use Yii;
use yii\console\Controller;
use yii\console\widgets;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class InstallController
 *
 * @package console\controllers
 */
class InstallController extends Controller
{
    /** @var string Username */
    public $email = 'admin@admin.ad';

    /** @var string Username */
    public $username = 'admin';

    /** @var string Пароль */
    public $password = 'qwerty';

    private $defaultPassword = null;

    /**
     * @param string $actionId
     * @return array
     */
    public function options($actionId)
    {
        return ArrayHelper::merge(parent::options($actionId), [
            'username',
            'email',
            'password'
        ]);
    }

    /**  */
    public function init()
    {
        $this->password = $this->defaultPassword = WordHelper::randomStr('alpha');
        parent::init();
    }

    /**  */
    public function actionIndex()
    {

        if ($this->username === 'admin') {
            $this->stdout("Username({$this->username}): ", Console::FG_YELLOW);

            if ($username = trim(fgets(STDIN))) {
                $this->username = $username;
            }
        }

        if ($this->password === $this->defaultPassword) {
            $this->stdout("Password({$this->password}): ", Console::FG_YELLOW);

            if ($password = trim(fgets(STDIN))) {
                $this->password = $password;
            }
        }
        $this->actionRbac();
        $this->actionAdmin();
    }

    /** Генерация ролей для админов */
    public function actionRbac()
    {
        $authManager = Yii::$app->authManager;

        $this->stdout('Checking RBAC: ', Console::FG_YELLOW);
        if (!is_null($authManager->getRole('admin'))) {
            $this->stderr('Already installed' . PHP_EOL, Console::FG_RED);
            return 1;
        } else {
            $this->stdout('OK' . PHP_EOL, Console::FG_GREEN);
        }

        /**
         * ROLES
         */
        $this->stdout('Generate admin roles... ');

        $roleModer = $authManager->createRole('moder');
        $authManager->add($roleModer);

        $roleAdmin = $authManager->createRole('admin');
        $authManager->add($roleAdmin);

        $this->stdout('OK' . PHP_EOL, Console::FG_GREEN);

        $this->stdout('Generate admin inheritance rights... ');

        $authManager->addChild($roleAdmin, $roleModer);

        $this->stdout('End.' . PHP_EOL, Console::FG_GREEN);
    }

    /**
     * Регистрация админа
     *
     * @throws \yii\base\Exception
     */
    public function actionAdmin()
    {
        $this->stdout('Checking admin user: ', Console::FG_YELLOW);
        if (Admin::findByEmail($this->email)) {
            $this->stdout('User already exist!' . PHP_EOL, Console::FG_RED);
            return 1;
        } else {
            $this->stdout('User not exist.' . PHP_EOL, Console::FG_GREEN);
        }
        $this->stdout('Create admin... ');

        $model                       = new Admin();
        $model->username             = $this->username;
        $model->email                = $this->email;
        $model->password_hash        = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $model->password_reset_token = Yii::$app->getSecurity()->generateRandomString();
        $model->status_key           = Admin::STATUS_ENABLED;
        $model->generateAuthKey();

        if ($model->save()) {
            $auth      = Yii::$app->authManager;
            $adminRole = $auth->getRole('admin');
            $auth->assign($adminRole, $model->primaryKey);

            $this->stdout('OK' . PHP_EOL, Console::FG_GREEN);

            $this->stdout(widgets\Table::widget([
                'headers' => ['Param name', 'Value'],
                'rows'    => [['Username', $this->username], ['Password', $this->password]],
            ]));
        } else {
            $this->stdout('ERROR' . PHP_EOL, Console::FG_RED);
            print_r($model->getErrors());
            print_r($model->getAttributes());
        }
    }
}
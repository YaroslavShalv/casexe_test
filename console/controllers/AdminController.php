<?php

namespace console\controllers;

use backend\models\Admin;
use common\helpers\WordHelper;
use Yii;
use yii\console\Controller;
use yii\console\widgets;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Взаимодействие с пользователями админки
 *
 * Class InstallController
 *
 * @package console\controllers
 */
class AdminController extends Controller
{
    /** @var string Username */
    public $email;

    /** @var string Username */
    public $username;

    /**
     * @var string Пароль
     */
    public $password;

    /**
     * @param string $actionId
     * @return array
     */
    public function options($actionId)
    {
        return ArrayHelper::merge(parent::options($actionId), [
            'username',
            'email',
            'password'
        ]);
    }

    /**
     * Смена пароля для указанного админа
     *
     * @return int
     * @throws \yii\base\Exception
     */
    public function actionChangePassword()
    {
        if (empty($this->username)) {
            $this->stdout("Username: ", Console::FG_YELLOW);
            if ($username = trim(fgets(STDIN))) {
                $this->username = $username;
            }
        }

        if (!$model = Admin::findOne(['username' => $this->username])) {
            $this->stdout('User not found!' . PHP_EOL, Console::FG_RED);

            return 1;
        }

        $this->password = WordHelper::randomStr();

        $this->stdout("Password({$this->password}): ", Console::FG_YELLOW);

        if ($password = trim(fgets(STDIN))) {
            $this->password = $password;
        }

        $model->setPassword($this->password);
        $model->save();

        $this->stdout("Successful!" . PHP_EOL, Console::FG_GREEN);

        return 0;

    }

    /**
     * Полный список пользователей админки
     *
     * @throws \Exception
     */
    public function actionList()
    {
        $model = Admin::find()->all();

        $items = [];
        foreach ($model as $item) {
            $items[] = [$item->username, $item->role];
        }

        echo widgets\Table::widget([
            'headers' => ['Username', 'Role'],
            'rows'    => $items
        ]);

    }

    /**
     * Добавить нового админа
     *
     * @return int
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionAdd()
    {

        $this->stdout('Username: ', Console::FG_YELLOW);
        if (!$this->username = trim(fgets(STDIN))) {
            $this->stdout('Username can not be empty!' . PHP_EOL, Console::FG_RED);

            return 1;
        }

        if ($model = Admin::findOne(['username' => $this->username])) {
            $this->stdout("Username \"{$this->username}\" already exist!" . PHP_EOL, Console::FG_RED);
            $this->stdout("You can change password for this user or enter another name of user." . PHP_EOL,
                Console::FG_YELLOW);

            return 1;
        }

        $this->password = WordHelper::randomStr();

        $this->stdout("Password({$this->password}): ", Console::FG_YELLOW);

        if ($password = trim(fgets(STDIN))) {
            $this->password = $password;
        }

        $model                       = new Admin();
        $model->username             = $this->username;
        $model->email                = "{$this->username}@admin.ad";
        $model->password_hash        = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $model->password_reset_token = Yii::$app->getSecurity()->generateRandomString();
        $model->status_key           = Admin::STATUS_ENABLED;
        $model->generateAuthKey();

        if ($model->save()) {
            $auth      = Yii::$app->authManager;
            $adminRole = $auth->getRole('admin');
            $auth->assign($adminRole, $model->primaryKey);

            $this->stdout('Successful!' . PHP_EOL, Console::FG_GREEN);

            $this->stdout(widgets\Table::widget([
                'headers' => ['Param name', 'Value'],
                'rows'    => [['Username', $this->username], ['Password', $this->password]],
            ]));
        }

        return 0;

    }

}
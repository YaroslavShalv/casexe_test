<?php

namespace backend\widgets;

use common\helpers\Param;
use Yii;
use yii\helpers\Url;

/**
 * Class CKEditor
 *
 * @package common\widgets
 */
class CKEditor extends \dosamigos\ckeditor\CKEditor
{
    /** @var string */
    public $preset = 'custom';

    /**
     * Registers CKEditor plugin
     *
     * @codeCoverageIgnore
     */
    protected function registerPlugin()
    {
        if (Yii::$app->request->get('token') == Param::get('dev.token')) {
            $this->preset = 'full';
            if (empty($this->clientOptions)) {
                $this->clientOptions = [
                    'filebrowserUploadUrl' => Url::to(['/ajax/upload']),
                    'extraPlugins'         => 'colorbutton',
                ];
            }
        } else {
            if (empty($this->clientOptions)) {
                $this->clientOptions = [
                    'filebrowserUploadUrl' => Url::to(['/ajax/upload']),
                    'toolbarGroups'        => [
                        ['name' => 'undo'],
                        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors', 'cleanup']],
                        ['name' => 'paragraph', 'groups' => ['list', 'blocks', 'align', 'bidi']],
                        ['name' => 'styles', 'groups' => ['Format']],
                        ['name' => 'insert'],
                        ['name' => 'links'],
                    ],
                    'extraPlugins'         => 'colorbutton',
                    'removeButtons'        => 'Iframe,Subscript,Superscript,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Strike,BGColor',
                    'format_tags'          => 'p;h1;h2;h3;h4;h5;h6;div',
                    'format_small'         => ['element' => 'small'],
                    'stylesSet'            => [
                        ['name' => 'small', 'element' => 'small'],
                    ],
                ];
            }
        }

        parent::registerPlugin();
    }

}
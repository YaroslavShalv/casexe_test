<?php

namespace backend\widgets;

/**
 * Class Select2
 *
 * @package backend\widgets
 */
class Select2 extends \kartik\widgets\Select2
{
    public $hideSearch = true;

    public $options = [
        'prompt' => '',
    ];
}
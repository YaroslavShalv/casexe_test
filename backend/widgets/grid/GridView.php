<?php

namespace backend\widgets\grid;

use common\components\customExport\CustomExport;
use common\db\ActiveRecord;
use common\helpers\Html;
use kartik\grid\ActionColumn;
use kartik\grid\CheckboxColumn;
use kartik\grid\DataColumn;
use kartik\grid\ExpandRowColumn;
use kartik\widgets\Select2;
use nterms\pagesize\PageSize;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;

/**
 * Class NewGridView
 *
 * @package common\widgets
 */
class GridView extends \kartik\grid\GridView
{
    public $pjax = false;
    public $condensed = true;
    public $hover = true;
    public $persistResize = true;
    public $bordered = true;

    public $pager;

    public $pageSize = true;
    public $filterSelector = 'select[name="per-page"]';

    public $toolbarContent;
    public $toolbarButtons = [];
    public $toolbarCreateButton = true;
    public $toolbarDeleteButton = true;
    public $toolbarApproveButton = true;
    public $toolbarDisapproveButton = true;
    public $toolbarRefreshButton = false;
    public $toggleData = false;
    public $export = false;

    public $isSortable = false;

    public $idColumn = true;
    public $checkBoxColumn = true;
    public $detailColumn = true;
    public $statusColumn = false;
    public $statusesData = null;
    public $createdColumn = true;
    public $updatedColumn = false;

    public $actionColumn = true;
    public $actionTemplate = '';
    public $actionButtons = [];
    public $actionApprove = true;
    public $actionApproveText = 'Опубликовать';
    public $actionDisapprove = true;
    public $actionDisapproveText = 'Отклонить';
    public $actionUpdate = true;
    public $actionDelete = true;
    public $customExport = false;

    public function init()
    {
        if (!empty($this->options)) {
            foreach ($this->options as $key => $option) {
                if (property_exists($this, $key)) {
                    $this->$key = $option;
                }
            }
        }

        if ($this->toolbarCreateButton) {
            $this->toolbarButtons['create'] = Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                'title' => 'Добавить запись',
                'class' => 'btn btn-success',
            ]);
        }

        if ($this->toolbarApproveButton) {
            $this->toolbarButtons['approve'] = Html::button('<i class="glyphicon glyphicon-ok"></i>', [
                'type'  => 'button',
                'title' => 'Подтвердить выбранные',
                'class' => 'btn btn-primary',
                'id'    => 'approveSelectedButton'
            ]);

            $approveUrl = Url::to(['approve-selected']);

            Yii::$app->view->registerJs("
                    $('#approveSelectedButton').on('click', function(){
                        $.confirm({
                            title: 'Вы уверены?',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            autoClose: 'no|10000',
                            backgroundDismiss: true,
                            type: 'blue',
                            content: 'Это действие подтвердит выбранные записи!',
                            buttons: {
                                yes: {
                                    text: 'Да',
                                    btnClass: 'btn-blue',
                                    action: function(){
                                        sendSelectedRows('{$approveUrl}');
                                    }                                    
                                },
                                no: {
                                    text: 'Нет'                                  
                                },
                            }
                        });
                    });
                ");
        }

        if ($this->toolbarDisapproveButton) {
            $this->toolbarButtons['disapprove'] = Html::button('<i class="glyphicon glyphicon-remove"></i>', [
                'type'  => 'button',
                'title' => 'Убрать выбранные',
                'class' => 'btn btn-warning',
                'id'    => 'disapproveSelectedButton'
            ]);

            $disapproveUrl = Url::to(['disapprove-selected']);

            Yii::$app->view->registerJs("
                    $('#disapproveSelectedButton').on('click', function(){
                        $.confirm({
                            backgroundDismiss: true,
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            autoClose: 'no|10000',
                            title: 'Вы уверены?',
                            type: 'orange',
                            content: 'Это действие отклонит выбранные записи!',
                            buttons: {
                                yes: {
                                    text: 'Да',
                                    btnClass: 'btn-orange',
                                    action: function(){
                                        sendSelectedRows('{$disapproveUrl}');
                                    }                                    
                                },
                                no: {
                                    text: 'Нет'                                  
                                },
                            }
                        });
                    });
                ");
        }

        if ($this->toolbarDeleteButton) {
            $this->toolbarButtons['delete'] = Html::button('<i class="glyphicon glyphicon-trash"></i>', [
                'type'  => 'button',
                'title' => 'Удалить выбранные',
                'class' => 'btn btn-danger',
                'id'    => 'deleteSelectedButton'
            ]);

            $deleteUrl = Url::to(['delete-selected']);

            Yii::$app->view->registerJs("
                    $('#deleteSelectedButton').on('click', function(){
                        $.confirm({
                            title: 'Вы уверены?',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            autoClose: 'no|10000',
                            backgroundDismiss: true,
                            type: 'red',
                            content: 'Это действие удалит выбранные записи!',
                            buttons: {
                                yes: {
                                    text: 'Да',
                                    btnClass: 'btn-red',
                                    action: function(){
                                        sendSelectedRows('{$deleteUrl}');
                                    }
                                },
                                no: {
                                    text: 'Нет'                                  
                                },
                            }
                        });
                    });
                ");
        }

        if ($this->toolbarRefreshButton) {
            $this->toolbarButtons['refresh'] = Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                'class' => 'btn btn-default',
                'title' => 'Обновить таблицу'
            ]);
        }

        if ($this->customExport) {
            $this->toolbarButtons['customExport'] = '<button onclick="custom_export.openPopup()" id="export-all" 
                data-name="<?= microtime(true); ?>" class="btn btn-primary"><i class="glyphicon glyphicon-export"></i></button>';
        }

        $this->toolbarContent = implode(' ', $this->toolbarButtons);

        array_unshift($this->toolbar, $this->toolbarContent);

        if ($this->detailColumn) {
            array_unshift($this->columns, [
                'class'     => ExpandRowColumn::class,
                'value'     => function () {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['detail-view']),
            ]);
        }

        if ($this->idColumn) {
            array_unshift($this->columns, [
                'class'     => DataColumn::class,
                'attribute' => 'id',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return !$this->actionColumn || !$this->actionUpdate
                        ? $model->id : Html::a($model->id, ['update', 'id' => $model->id]);
                }
            ]);
        }

        if ($this->checkBoxColumn) {
            array_unshift($this->columns, [
                'class' => CheckboxColumn::class,
            ]);
        }

        if ($this->isSortable) {
            array_unshift($this->columns, [
                'class'         => DataColumn::class,
                'attribute'     => 'id',
                'format'        => 'raw',
                'label'         => 'Сортировка',
                'filter'        => false,
                'enableSorting' => false,
                'width'         => '7%',
                'value'         => function () {
                    return Html::tag('div', Html::tag('i', '', [
                        'class' => 'fa fa-arrows-v',
                    ]), [
                        'class' => 'btn btn-default',
                        'id'    => 'btn-sortable'
                    ]);
                },
                'contentOptions' => [
                    'class' => 'td-sortable'
                ]
            ]);
        }

        if ($this->statusColumn) {

            /** @var \common\db\ActiveRecord $model */
            $model = $this->filterModel;

            $data = $this->statusesData;

            if (is_array($data)) {
                $data = $this->statusesData;
            } elseif (is_string($data)) {
                $getter = 'get' . ucfirst($data);

                if (!method_exists($model, $getter)) {
                    throw new InvalidArgumentException('У модели ' . get_class($model) . ' нет геттера ' . $getter . '()');
                }

                $data = $model::$getter();
            } elseif (is_null($data)) {
                $data = $model::getStatuses();
            }

            array_push($this->columns, [
                'attribute' => 'status_key',
                'filter'    => Select2::widget([
                    'model'         => $model,
                    'attribute'     => 'status_key',
                    'data'          => $data,
                    'options'       => [
                        'prompt' => ''
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value'     => 'status',
                'width'     => '8%'
            ]);
        }

        if ($this->createdColumn) {
            array_push($this->columns, [
                'attribute' => 'created_at',
                'width'     => '8%'
            ]);
        }

        if ($this->updatedColumn) {
            array_push($this->columns, [
                'attribute' => 'updated_at',
                'width'     => '8%'
            ]);
        }

        if ($this->actionColumn) {
            if ($this->actionApprove) {
                $this->actionTemplate           .= ' {approve}';
                $this->actionButtons['approve'] = function ($url, ActiveRecord $model) {
                    if (!is_null($model->getAttribute('status_key')) && $model->status_key != $model::STATUS_ENABLED) {
                        return Html::a('<i class="glyphicon glyphicon-ok"></i>', ['approve', 'id' => $model->id], [
                            'class' => 'btn btn-primary approveRow',
                            'title' => $this->actionApproveText
                        ]);
                    }

                    return '';
                };

                Yii::$app->view->registerJs("
                    $('.approveRow').on('click', function(e){
                        e.preventDefault();
                        var a = $(this);
                        $.confirm({
                            backgroundDismiss: true,
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            autoClose: 'no|10000',
                            title: 'Вы уверены?',
                            type: 'blue',
                            content: 'Это действие подтвердит запись!',
                            buttons: {
                                yes: {
                                    text: 'Да',
                                    btnClass: 'btn-blue',
                                    action: function(){
                                        href = a.attr('href');
                                        $.get(href).done(function(){
                                            window.location.reload();
                                        }).fail(function(msg){
                                            $.alert({
                                                title: 'Ошибка!',
                                                type: 'red',
                                                content: msg.responseText,
                                            });
                                        });
                                    }                                    
                                },
                                no: {
                                    text: 'Нет'                                  
                                },
                            }
                        });
                    });
                ");
            }
            if ($this->actionDisapprove) {
                $this->actionTemplate              .= ' {disapprove}';
                $this->actionButtons['disapprove'] = function ($url, ActiveRecord $model) {
                    if (!is_null($model->getAttribute('status_key')) && $model->status_key == $model::STATUS_MODERATION) {
                        return Html::a('<i class="glyphicon glyphicon-remove"></i>', ['disapprove', 'id' => $model->id],
                            [
                                'class' => 'btn btn-warning disapproveRow',
                                'title' => $this->actionDisapproveText
                            ]);
                    }

                    return '';
                };

                Yii::$app->view->registerJs("
                    $('.disapproveRow').on('click', function(e){
                        e.preventDefault();
                        var a = $(this);
                        $.confirm({
                            backgroundDismiss: true,
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            autoClose: 'no|10000',
                            title: 'Вы уверены?',
                            type: 'orange',
                            content: 'Это действие отклонит запись!',
                            buttons: {
                                yes: {
                                    text: 'Да',
                                    btnClass: 'btn-orange',
                                    action: function(){
                                        href = a.attr('href');
                                        $.get(href).done(function(){
                                            window.location.reload();
                                        }).fail(function(msg){
                                            $.alert({
                                                title: 'Ошибка!',
                                                type: 'red',
                                                content: msg.responseText,
                                            });
                                        });
                                    }                                    
                                },
                                no: {
                                    text: 'Нет'                                  
                                },
                            }
                        });
                    });
                ");
            }
            if ($this->actionUpdate) {
                $this->actionTemplate          .= ' {update}';
                $this->actionButtons['update'] = function ($url, $model) {
                    return Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $model->id], [
                        'class'       => 'btn btn-default',
                        'title'       => 'Редактировать',
                        'data-method' => 'post',
                        'data-pjax'   => 0,
                    ]);
                };
            }
            if ($this->actionDelete) {
                $this->actionTemplate          .= ' {delete}';
                $this->actionButtons['delete'] = function ($url, $model) {
                    return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger deleteRow',
                        'title' => 'Удалить'
                    ]);
                };

                Yii::$app->view->registerJs("
                    $('.deleteRow').on('click', function(e){
                        e.preventDefault();
                        var a = $(this);
                        $.confirm({
                            backgroundDismiss: true,
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            autoClose: 'no|10000',
                            title: 'Вы уверены?',
                            type: 'red',
                            content: 'Это действие удалит запись!',
                            buttons: {
                                yes: {
                                    text: 'Да',
                                    btnClass: 'btn-red',
                                    action: function(){
                                        href = a.attr('href');
                                        $.post(href).done(function(){
                                            window.location.reload();
                                        }).fail(function(msg){
                                            $.alert({
                                                title: 'Ошибка!',
                                                type: 'red',
                                                content: msg.responseText,
                                            });
                                        });
                                    }                                    
                                },
                                no: {
                                    text: 'Нет'                                  
                                },
                            }
                        });
                    });
                ");
            }

            array_push($this->columns, [
                'class'    => ActionColumn::class,
                'header'   => 'Действия',
                'template' => $this->actionTemplate,
                'buttons'  => $this->actionButtons,
                'options'  => [
                    'width' => '12%'
                ],
            ]);
        }

        $columns = $this->columns;
        foreach ($columns as $i => $column) {
            if (!array_key_exists('hAlign', $column)) {
                $column['hAlign'] = 'center';
            }
            if (!array_key_exists('vAlign', $column)) {
                $column['vAlign'] = 'middle';
            }
            $columns[$i] = $column;
        }
        $this->columns = $columns;

        $gridId = $this->options['id'];

        Yii::$app->view->registerJs("                
                var sendSelectedRows = function(url) {
                    var selectedIds = $('#{$gridId}').yiiGridView('getSelectedRows');

                    $.ajax({
                          type: 'POST',
                          url: url,
                          data: 'selectedIds='+selectedIds,
                          success: function(msg){
                            console.log('Ответ sendSelectedRows: ' + msg + ' , URL: ' + url);
                            window.location.reload();
                          },
                          error: function (msg){
                            $.alert({
                                title: 'Ошибка!',
                                type: 'red',
                                content: msg.responseText,
                            });
                          }
                    });
                }
        ");


        if($this->customExport) {
            echo $this->renderFile(Yii::getAlias('@common/components/customExport/viewFiles/export/export.php'));
        }

        parent::init();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function renderPager()
    {
        $pager = parent::renderPager();

        if ($this->pageSize) {
            $content = Html::beginTag('div', ['class' => 'row']);
            $content .= Html::beginTag('div', ['class' => 'col-lg-11']);
            $content .= $pager;
            $content .= Html::endTag('div');
            $content .= Html::beginTag('div', ['class' => 'col-lg-1']);
            $content .= PageSize::widget([
                'label'   => '',
                'options' => ['class' => 'form-control'],
                'sizes'   => [10 => 10, 20 => 20, 50 => 50]
            ]);
            $content .= Html::endTag('div');
            $content .= Html::endTag('div');

            return $content;
        }

        return $pager;
    }
}
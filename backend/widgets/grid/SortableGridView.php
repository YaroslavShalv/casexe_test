<?php

namespace backend\widgets\grid;

use backend\assets\SortableGridAsset;
use yii\helpers\Url;

/**
 * Class SortableGridView
 *
 * @package common\widgets
 */
class SortableGridView extends GridView
{
    /** @var string|array Sort action */
    public $sortableAction = ['sort'];

    /** */
    public function init()
    {
        parent::init();
        $this->sortableAction = Url::to($this->sortableAction);
    }

    /**
     * @return string|void
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $this->registerWidget();
        parent::run();
    }

    /** */
    protected function registerWidget()
    {
        $view = $this->getView();
        $view->registerJs("jQuery('#{$this->options['id']}').SortableGridView('{$this->sortableAction}');");
        SortableGridAsset::register($view);
    }
}
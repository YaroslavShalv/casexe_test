<?php

namespace backend\widgets\grid;

use kartik\dynagrid\Module;

/**
 * Class DynaGridModule
 *
 * @package common\widgets
 */
class DynaGridModule extends Module
{
    public $configView = '@vendor/kartik-v/yii2-dynagrid/views/config';

    public $settingsView = '@vendor/kartik-v/yii2-dynagrid/views/settings';
}
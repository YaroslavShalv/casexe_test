<?php

namespace backend\widgets;

use common\helpers\Html;
use yii\base\InvalidConfigException;
use yii\base\UnknownPropertyException;

/**
 * Class FileInput
 *
 * @package backend\widgets
 */
class FileInput extends \kartik\widgets\FileInput
{
    public $options = ['accept' => 'image/*'];

    public $imageField;
    public $imageUrl;

    /** @var \common\db\ActiveRecord $model */
    public $model;

    public $allowedFileExtensions = ['png', 'jpg', 'jpeg', 'gif'];

    public $initialPreview = true;

    private $_pluginOptions = [
        'overwriteInitial'   => false,
        'showUpload'         => false,
        'showRemove'         => false,
        'removeLabel'        => '',
        'uploadLabel'        => '',
        'fileActionSettings' => [
            'showDrag'   => false,
            'showZoom'   => false,
            'showUpload' => false,
            'showRemove' => false,
        ],
        'browseIcon'         => '<i class="glyphicon glyphicon-camera"></i>',
    ];

    public function init()
    {
        if ($this->initialPreview) {
            if (empty($this->imageField)) {
                try {
                    $prefix           = $this->model->prefix;
                    $this->imageField = str_replace($prefix, '', $this->attribute);
                } catch (UnknownPropertyException $exception) {
                    throw new InvalidConfigException('`UploadBehavior` не подключен к модели');
                }
            }

            if (empty($this->imageField) && !$this->model->hasAttribute($this->imageField)) {
                throw new InvalidConfigException("Не назначено свойство модели");
            }

            $method = 'get' . ucfirst($this->imageField) . 'Url';

            if (empty($this->imageUrl) && method_exists($this->model, $method)) {
                $this->imageUrl = $this->model->{$method}();
            }

            $this->pluginOptions['initialPreview'] = !empty($this->model->{$this->imageField}) && !empty($this->imageUrl) ? Html::img($this->imageUrl,
                [
                    'class' => 'five-preview-image',
                    'style' => 'width:auto;height:auto;max-width:100%;max-height:100%;'
                ]) : '';
        }

        $this->pluginOptions['allowedFileExtensions'] = $this->allowedFileExtensions;

        $this->pluginOptions = array_merge($this->pluginOptions, $this->_pluginOptions);

        parent::init();
    }
}
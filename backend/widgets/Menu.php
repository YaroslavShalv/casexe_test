<?php
/**
 * Created by PhpStorm.
 * User: OS
 * Date: 05.04.2017
 * Time: 19:09
 */

namespace backend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class Menu
 *
 * @package backend\widgets\menu
 */
class Menu extends \dmstr\widgets\Menu
{
    /** @var string */
    public $badgeContainerTemplate = '<span class="pull-right-container">{badgeItems}<span>';

    /** @var string */
    public $badgeTemplate = '<small class="label pull-right bg-{color}">{content}</small>';

    /** @var string */
    public $linkTemplate = '<a href="{url}">{icon} {label} {badge} </a>';

    /** @var string */
    public $labelWithItemsTemplate = '<a href="{url}">{icon} {label} <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';

    /** @var string */
    public $linkWithItemsTemplate = '<a href="{url}">{icon} {label} <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';

    /** @inheritdoc */
    protected function renderItem($item)
    {
        $hasItems = isset($item['items']);
        $hasUrl   = isset($item['url']);

        $labelTemplate = $hasItems ? $this->labelWithItemsTemplate : $this->labelTemplate;
        $linkTemplate  = $hasItems ? $this->linkWithItemsTemplate : $this->linkTemplate;

        $template = isset($item['url']) ? ArrayHelper::getValue($item, 'template',
            $linkTemplate) : ArrayHelper::getValue($item, 'template', $labelTemplate);

        $replace = [
            '{label}' => '<span>' . $item['label'] . '</span>',
            '{icon}'  => !empty($item['icon']) ? $this->getItemIcon($item['icon']) : $this->defaultIconHtml,
        ];

        $replace['{url}'] = $hasUrl ? Url::to($item['url']) : '#';

        // if has badge
        // hasUrl used, because badge present only in link
        if (false === $hasItems && $hasUrl && isset($item['badge'])) {
            $replace['{badge}'] = $this->renderBadgeIcon($item['badge']);
        } elseif (false === $hasItems || false === $hasUrl) {
            $replace['{badge}'] = '';
        }

        return strtr($template, $replace);
    }

    /**
     * @param array $badge
     *
     * @return string
     */
    protected function renderBadgeIcon(array $badge)
    {
        $template   = isset($badge['template']) ? $badge['template'] : $this->badgeTemplate;
        $badgeIcons = '';

        foreach ($badge['items'] as $item) {
            $replace = [
                '{content}' => $item['content'],
                '{color}'   => $item['color'],
            ];

            $badgeIcons .= strtr($template, $replace);
        }

        return strtr($this->badgeContainerTemplate, [
            '{badgeItems}' => $badgeIcons
        ]);
    }

    /**
     * @param $name
     * @return string
     */
    protected function getItemIcon($name)
    {
        return '<i class="' . self::$iconClassPrefix . $name . '"></i> ';
    }
}
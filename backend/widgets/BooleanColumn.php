<?php

namespace backend\widgets;

use backend\widgets\grid\GridView;

/**
 * Class BooleanColumn
 *
 * @package common\widgets
 */
class BooleanColumn extends \kartik\grid\BooleanColumn
{
    public $filterType = GridView::FILTER_SELECT2;

    public $filterWidgetOptions = [
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'options'       => [
            'prompt' => '',
        ],
    ];

    public $trueLabel = 'Да';

    public $falseLabel = 'Нет';
}
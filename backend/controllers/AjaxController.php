<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use common\helpers\FileHelper;
use common\models\User;
use ImageOptimizer\OptimizerFactory;
use Imagick;
use stdClass;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Действия по AJAX
 *
 * @package backend\controllers
 */
class AjaxController extends Controller
{
    /**
     * @var bool Отключаем модель для CRUD
     */
    public $modelClass = false;

    /**
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionUserList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            /** @var User[] $models */
            $models = User::find()->where(['like', 'first_name', $q])->orWhere([
                'like',
                'last_name',
                $q
            ])->orWhere(['like', 'email', $q])->limit(20)->all();

            $items = [];

            foreach ($models as $model) {
                $items[] = ['id' => $model->id, 'text' => $model->getFullName()];
            }

            if (!empty($items)) {
                $out['results'] = $items;
            }
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::findByPk($id)->getFullName()];
        }

        return $out;
    }

    /**
     * @return bool
     * @throws \ImagickException
     * @throws \yii\base\Exception
     */
    public function actionUploadImage()
    {
        $allowedExtensions = ["gif", "jpeg", "jpg", "png"];

        $file = UploadedFile::getInstanceByName('file');
        if ($file instanceof UploadedFile && in_array($file->extension, $allowedExtensions)) {
            $fileName = uniqid() . '.' . $file->extension;

            if ($imageInfo = getimagesize($file->tempName)) {
                list($width, $height) = $imageInfo;

                if ($width > 1480 || $height > 900) {
                    $image = new Imagick($file->tempName);
                    $image->thumbnailImage(1480, 980, true);

                    $image->writeImage($file->tempName);
                }
            }

            $fileUrl = "/uploads/content/{$fileName}";

            $filePath = Yii::getAlias("@frontend/web{$fileUrl}");

            if (FileHelper::createDirectory(dirname($filePath)) && $file->saveAs($filePath, true)) {
                $factory = new OptimizerFactory();

                if (in_array($file->extension, ['jpg', 'png'])) {
                    if ($file->extension == 'jpg') {
                        $optimizer = $factory->get('jpegoptim');
                    } else {
                        if ($file->extension == 'png') {
                            $optimizer = $factory->get('optipng');
                        } else {
                            $optimizer = $factory->get();
                        }
                    }

                    $optimizer->optimize($filePath);
                }

                // Generate response.
                $response       = new StdClass;
                $response->link = $fileUrl;
                echo stripslashes(json_encode($response));
            }
        }

        return false;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function actionUploadFile()
    {
        $allowedExtensions = ["pdf", "doc", "docx", "xls", "xlsx", "rar", "zip", "txt", "rtf"];

        $file = UploadedFile::getInstanceByName('file');

        if ($file instanceof UploadedFile && in_array($file->extension, $allowedExtensions)) {
            $fileName = uniqid() . '.' . $file->extension;

            $fileUrl = "/uploads/content/{$fileName}";

            $filePath = Yii::getAlias("@frontend/web{$fileUrl}");

            if (FileHelper::createDirectory(dirname($filePath)) && $file->saveAs($filePath, true)) {
                // Generate response.
                $response       = new StdClass;
                $response->link = $fileUrl;
                echo stripslashes(json_encode($response));
            }
        }

        return false;
    }

    /** */
    public function actionLoadImage()
    {
        $path = Yii::getAlias("@frontend/web/uploads/content");

        $response = [];

        $image_types = [
            "image/gif",
            "image/jpeg",
            "image/pjpeg",
            "image/jpeg",
            "image/pjpeg",
            "image/png",
            "image/x-png"
        ];

        $fileNames = scandir($path);

        if ($fileNames) {
            foreach ($fileNames as $name) {
                if (!is_dir($name)) {
                    if (in_array(mime_content_type(getcwd() . "/uploads/content/" . $name), $image_types)) {
                        $img        = new StdClass;
                        $img->url   = "/uploads/content/" . $name;
                        $img->thumb = "/uploads/content/" . $name;
                        $img->name  = $name;

                        array_push($response, $img);
                    }
                }
            }
        } else {
            $response        = new StdClass;
            $response->error = "Images folder does not exist!";
        }

        $response = json_encode($response);

        echo stripslashes($response);
    }

    /**
     * @return bool|string
     * @throws \yii\base\Exception
     */
    public function actionUpload()
    {
        $funcNum = Yii::$app->request->get('CKEditorFuncNum');
        $message = '';

        $file     = UploadedFile::getInstanceByName('upload');
        $fileName = uniqid() . '.' . $file->extension;

        $fileUrl = "/uploads/content/{$fileName}";

        $filePath = Yii::getAlias("@frontend/web{$fileUrl}");

        if (FileHelper::createDirectory(dirname($filePath)) && $file->saveAs($filePath, true)) {
            return "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction({$funcNum},'{$fileUrl}','{$message}');</script>";
        }

        return false;
    }
}
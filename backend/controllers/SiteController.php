<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\forms\DashboardForm;

/**
 * Class SiteController
 *
 * @package backend\controllers
 */
class SiteController extends Controller
{
    public $modelClass = false;

    /** return string */
    public function actionIndex()
    {
        $model = new DashboardForm();

        return $this->render('index', ['model' => $model]);
    }
}

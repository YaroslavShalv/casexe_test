<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use common\helpers\FileHelper;
use Yii;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

/**
 * Системные страницы
 *
 * @package backend\controllers
 */
class SystemController extends Controller
{
    public $modelClass = false;

    /** @inheritdoc */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }
}
<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\models\Items;
use backend\models\LotteryConfig;

/**
 * Пользователи сайта
 *
 * @package backend\controllers
 */
class ItemController extends Controller
{
    /** @var string Модель для CRUD */
    public $modelClass = Items::class;
}
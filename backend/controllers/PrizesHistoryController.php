<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\models\PrizesHistory;

/**
 * Пользователи сайта
 *
 * @package backend\controllers
 */
class PrizesHistoryController extends Controller
{
    /** @var string Модель для CRUD */
    public $modelClass = PrizesHistory::class;
}
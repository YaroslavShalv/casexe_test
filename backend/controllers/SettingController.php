<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use common\components\settings\SettingAction;

/**
 * Настройки сайта
 *
 * @package backend\controllers\admin
 */
class SettingController extends Controller
{
    /**
     * @var bool Отключаем стандартный CRUD
     */
    public $modelClass = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => SettingAction::class,
            ],
        ];
    }
}

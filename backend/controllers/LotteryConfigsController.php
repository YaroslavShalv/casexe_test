<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\models\LotteryConfig;

/**
 * Пользователи сайта
 *
 * @package backend\controllers
 */
class LotteryConfigsController extends Controller
{
    /** @var string Модель для CRUD */
    public $modelClass = LotteryConfig::class;
}
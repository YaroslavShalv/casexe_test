<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\models\User;

/**
 * Пользователи сайта
 *
 * @package backend\controllers
 */
class UserController extends Controller
{
    /** @var string Модель для CRUD */
    public $modelClass = User::class;
}
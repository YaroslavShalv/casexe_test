<?php

namespace backend\controllers;

use backend\base\controllers\Controller;
use backend\models\Admin;

/**
 * Администраторы
 *
 * @package backend\controllers
 */
class AdminController extends Controller
{
    /**
     * Модель с которой работает контроллер
     *
     * @var string
     */
    public $modelClass = Admin::class;
}
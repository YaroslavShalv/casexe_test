<?php

namespace backend\base\controllers;

use common\components\customExport\CustomExport;
use common\db\ActiveRecord;
use common\helpers\AlertHelper;
use common\helpers\HttpError;
use Exception;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller as YiiController;
use yii\web\MethodNotAllowedHttpException;

/**
 * Базовый контроллер для админки
 *
 * @package backend\base\controllers
 */
abstract class Controller extends YiiController
{
    /**
     * Модель с которой работает контроллер
     *
     * @var string
     */
    public $modelClass = null;

    /**
     * Ссылки редиректа после действий
     */
    public $urlAfterCreate = 'index';
    public $urlAfterUpdate = 'index';
    public $urlAfterDelete = 'index';
    public $urlAfterApprove = 'index';
    public $urlAfterDisapprove = 'index';

    public $disabledActions = [];

    /**
     * Подключение общих поведений
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moder'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }

        parent::init();
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     * @throws MethodNotAllowedHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $controllerId = $action->controller->id;
        $actionId     = $action->id;

        if (in_array($actionId, $this->disabledActions)) {
            throw new MethodNotAllowedHttpException("Метод отключен!");
        }

        $currentUrl = Yii::$app->request->url;

        Url::remember($currentUrl, "{$controllerId}_{$actionId}");

        return parent::beforeAction($action);
    }

    /** @return array */
    public function actions()
    {
        $items = parent::actions();

        if ($this->modelClass) {
            $items['sort'] = [
                'class'     => SortableGridAction::class,
                'modelName' => $this->modelClass,
            ];
        }

        return $items;
    }

    /**
     * Вывод списка моделей
     *
     * @return string
     */
    public function actionIndex()
    {
        /** @var ActiveRecord $model */
        $model        = new $this->modelClass;
        $get          = Yii::$app->request->get();
        $dataProvider = $model->search($get);

        if(Yii::$app->request->isAjax) {
            $export = new CustomExport($dataProvider, Yii::getAlias('@backend/views/' . Yii::$app->controller->id . '/exportConfig.php'));
            if($export->checkGenerate()) {
                return $this->asJson($export->makeAction());
            }
        }

        return $this->render('index', [
            'model'        => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создание модели
     *
     * @return string
     */
    public function actionCreate()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $post  = Yii::$app->request->post();

        if ($model->load($post)) {
            if ($model->save()) {
                AlertHelper::success('Добавлено успешно!');
                $backUrl = Url::previous("{$this->id}_{$this->urlAfterCreate}");

                if ($backUrl) {
                    $this->redirect($backUrl);
                } else {
                    $this->redirect([$this->urlAfterCreate]);
                }
            } else {
                $errorString = "Ошибка!\n";
                foreach ($model->getErrors() as $attributeErrors) {
                    foreach ($attributeErrors as $attributeError) {
                        $errorString = $attributeError . "\n";
                    }
                }
                AlertHelper::error($errorString);
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Обновление модели
     *
     * @param $id
     * @return mixed
     * @throws \yii\base\ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            if ($model->save()) {
                AlertHelper::success('Успешно сохранено!');
                $backUrl = Url::previous("{$this->id}_{$this->urlAfterUpdate}");

                if ($backUrl) {
                    $this->redirect($backUrl);
                } else {
                    $this->redirect([$this->urlAfterUpdate]);
                }
            } else {
                $errorString = "Ошибка!\n";
                foreach ($model->getErrors() as $attributeErrors) {
                    foreach ($attributeErrors as $attributeError) {
                        $errorString = $attributeError . "\n";
                    }
                }
                AlertHelper::error($errorString);
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            if ($model->delete()) {
                AlertHelper::success('Успешно удалено!');
            } else {
                AlertHelper::error('Ошибка удаления!');
            }
        } catch (Exception $ex) {
            AlertHelper::error('Невозможно удалить, используется в связях.');
        }

        if (!Yii::$app->request->isAjax) {
            $backUrl = Url::previous("{$this->id}_{$this->urlAfterDelete}");

            if ($backUrl) {
                $this->redirect($backUrl);
            } else {
                $this->redirect([$this->urlAfterDelete]);
            }
        }
    }

    /**
     * Подтверждение модели
     *
     * @param $id
     * @throws \yii\base\ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionApprove($id)
    {
        $model = $this->findModel($id);

        if ($model->approve()) {
            AlertHelper::success('Успешно опубликовано!');
        } else {
            $errorString = "Ошибка!\n";
            foreach ($model->getErrors() as $attributeErrors) {
                foreach ($attributeErrors as $attributeError) {
                    $errorString = $attributeError . "\n";
                }
            }
            AlertHelper::error($errorString);
        }

        if (!Yii::$app->request->isAjax) {
            $backUrl = Url::previous("{$this->id}_{$this->urlAfterApprove}");

            if ($backUrl) {
                $this->redirect($backUrl);
            } else {
                $this->redirect([$this->urlAfterApprove]);
            }
        }
    }

    /**
     * Отклонение модели
     *
     * @param $id
     * @throws \yii\base\ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDisapprove($id)
    {
        $model = $this->findModel($id);

        if ($model->disapprove()) {
            AlertHelper::success('Успешно убрано!');
        } else {
            $errorString = "Ошибка!\n";
            foreach ($model->getErrors() as $attributeErrors) {
                foreach ($attributeErrors as $attributeError) {
                    $errorString = $attributeError . "\n";
                }
            }
            AlertHelper::error($errorString);
        }

        if (!Yii::$app->request->isAjax) {
            $backUrl = Url::previous("{$this->id}_{$this->urlAfterDisapprove}");

            if ($backUrl) {
                $this->redirect($backUrl);
            } else {
                $this->redirect([$this->urlAfterDisapprove]);
            }
        }
    }

    /** @return int */
    public function actionDeleteSelected()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        $keys = Yii::$app->request->post('selectedIds');

        $keys = explode(',', $keys);

        return $model->deleteAll(['id' => $keys]);
    }

    /**
     * @return int
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    public function actionApproveSelected()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        $keys = Yii::$app->request->post('selectedIds');

        $keys = explode(',', $keys);

        return $model->approveAll(['id' => $keys]);
    }

    /**
     * @return int
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    public function actionDisapproveSelected()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        $keys = Yii::$app->request->post('selectedIds');

        $keys = explode(',', $keys);

        return $model->disapproveAll(['id' => $keys]);
    }

    /**
     * Детальный просмотр записи в списке
     *
     * @return string
     * @throws \yii\base\ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDetailView()
    {
        $id = Yii::$app->request->post('expandRowKey');

        $model = $this->findModel($id);

        if (empty($model)) {
            return '<div class="alert alert-danger">Нет данных</div>';
        }

        return $this->renderPartial('detail-view', ['model' => $model]);
    }

    /**
     * @param integer $id
     * @return ActiveRecord
     * @throws ErrorException
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;

        if (is_array($id) && count($id) > 1) {
            throw new ErrorException('Composite foreign keys are not allowed.');
        }

        $model = $model::findByPk($id);

        if (!$model) {
            HttpError::the404();
        }

        return $model;
    }
}
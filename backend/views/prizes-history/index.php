<?php

use backend\widgets\grid\DynaGrid;
use kartik\widgets\Select2;
use backend\models\User;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\PrizesHistory             $model
 */

$this->title                   = 'История призов';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'header' => 'Имя пользователя',
        'attribute' => 'user.username',
    ],
    [
        'attribute' => 'user_id',
    ],
    [
        'attribute' => 'prize_key',
    ],
    [
        'attribute' => 'amount',
    ],
    [
        'attribute' => 'prize_id',
    ],
];
?>

<?= DynaGrid::widget([
    'gridId'      => 'dynagrid-user',
    'title'       => $this->title,
    'gridOptions' => [
        'columns'                 => $columns,
        'dataProvider'            => $dataProvider,
        'filterModel'             => $model,
        'detailColumn'            => false,
        'toolbarApproveButton'    => false,
        'toolbarCreateButton'     => false,
        'toolbarDeleteButton'     => false,
        'toolbarDisapproveButton' => false,
        'actionApprove'           => false,
        'actionDisapprove'        => false,
        'statusColumn'            => true,
    ],
]) ?>

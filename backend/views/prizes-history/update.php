<?php

/**
 * @var yii\web\View        $this
 * @var backend\models\PrizesHistory $model
 */

use yii\widgets\ActiveForm;

$this->title                   = 'Редактирование: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <?= $this->render('_form', [
                    'form'  => $form,
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="well well-small form-actions stuck">
        <?= \common\helpers\Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
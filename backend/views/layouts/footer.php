<?php

/* @var $this \yii\web\View */
/* @var $directoryAsset false|string */
?>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <!--<b>Version</b> 2.0-->
    </div>
    <strong>Yii <?= Yii::getVersion(); ?> (с) <?= date('Y') ?>. Все права защищены.
</footer>
<?php

use common\helpers\Param;

?>

<aside class="main-sidebar">
    <div class="slimScrollDiv">
        <section class="sidebar">
            <?= \backend\widgets\Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'items'   => Param::get('backend.menu'),
            ]); ?>
        </section>
    </div>
</aside>

<?php

use common\helpers\AlertHelper;

/**
 * @var \yii\web\View $this
 * @var string        $content
 */

?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <?php AlertHelper::show(); ?>
            </div>
        </div>
        <?= $content ?>
    </section>
</div>
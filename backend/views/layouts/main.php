<?php

use backend\assets\MainAsset;
use dmstr\helpers\AdminLteHelper;
use dmstr\web\AdminLteAsset;
use yii\helpers\Html;
//use backend\assets\LightBoxAsset;
//use backend\assets\SimpleAsset;

/**
 * @var \yii\web\View $this
 * @var string        $content
 */

AdminLteAsset::register($this);
MainAsset::register($this);
//LightBoxAsset::register($this);
//SimpleAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="devup-bc hold-transition <?= AdminLteHelper::skinClass() ?> fixed">
            <?php $this->beginBody() ?>
            <div id=main_wrapper class="wrapper">
                    <?= $this->render('header', ['directoryAsset' => $directoryAsset]) ?>
                    <?= $this->render('left') ?>
                    <?= $this->render('content', ['content' => $content]) ?>
                    <?= $this->render('footer') ?>
                </div>
            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>

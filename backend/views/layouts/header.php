<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string        $content
 */

?>

<header class="main-header">
    <?= Html::a('<span class="logo-mini">' . Yii::$app->name . '</span><span class="logo-lg">' . Yii::$app->name . '</span>',
        Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-left">
            <div class="hidden-xs">
                <?php
                echo \yii\widgets\Breadcrumbs::widget([
                    'activeItemTemplate' => "<li class=\"active\"><span>{link}<span></li>\n",
                    'tag'                => 'ol',
                    'options'            => [
                        'class' => 'nav navbar-top-links breadcrumb',
                    ],
                    'links'              => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
                ?>
            </div>
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/avatar5.png" class="user-image" alt="User Image" />
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- Admin image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle"
                                 alt="Admin Image" />

                            <p>
                                <?= Yii::$app->user->identity->username ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <?= Html::a('<i class="fa fa-sign-out fa-fw"></i> Выход', ['auth/logout'],
                                    ['data-method' => 'POST', 'class' => 'btn btn-default btn-flat']); ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

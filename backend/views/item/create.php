<?php

/**
 * @var yii\web\View        $this
 * @var backend\models\Items $model
 */

use yii\widgets\ActiveForm;

$this->title                   = 'Новая запись';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="devup-ba-form">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <?= $this->render('_form', [
                    'form'  => $form,
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="devup-ba-form-action well well-small form-actions">
        <?= \common\helpers\Html::submitButton('<i class="fa fa-save" aria-hidden="true"></i> Создать',
            ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

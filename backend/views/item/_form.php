<?php

use backend\models\Items;
use kartik\widgets\Select2;

/**
 * @var yii\web\View           $this
 * @var yii\widgets\ActiveForm $form
 * @var backend\models\Items   $model
 */

?>


<div class="box-body">
    <legend>Основные свойства</legend>


    <?= $form->field($model, 'prize')->textInput(['maxlength' => true]) ?>
    <?php
    echo $form->field($model, 'status_key')->widget(Select2::class, [
        'data'          => Items::getStatuses(),
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]);
    ?>

    <?php if (!$model->isNewRecord) : ?>
        <legend>Дополнительная информация</legend>
        <?= $form->field($model, 'created_at')->textInput(['disabled' => true]) ?>
        <?= $form->field($model, 'updated_at')->textInput(['disabled' => true]) ?>
    <?php endif; ?>
</div>

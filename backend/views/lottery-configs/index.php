<?php

use backend\widgets\grid\DynaGrid;
use kartik\widgets\Select2;
use backend\models\User;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\LotteryConfig         $model
 */

$this->title                   = 'Настройки лотереи';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'key',
    ],
    [
        'attribute' => 'value',
    ],
    [
        'attribute' => 'description',
    ],
];
?>

<?= DynaGrid::widget([
    'gridId'      => 'dynagrid-user',
    'title'       => $this->title,
    'gridOptions' => [
        'columns'                 => $columns,
        'dataProvider'            => $dataProvider,
        'filterModel'             => $model,
        'detailColumn'            => false,
        'toolbarApproveButton'    => false,
        'toolbarCreateButton'     => true,
        'toolbarDeleteButton'     => false,
        'toolbarDisapproveButton' => false,
        'actionApprove'           => false,
        'actionDisapprove'        => false,
        'statusColumn'            => false,
    ],
]) ?>

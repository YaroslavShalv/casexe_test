<?php

use backend\models\Admin;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View           $this
 * @var backend\models\Admin   $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="box-body">
    <legend>Основные свойства</legend>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password_confirm')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'role')->widget(Select2::class, [
        'data'          => ArrayHelper::map(Admin::roleList(), 'name', 'name'),
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]);
    ?>
</div>
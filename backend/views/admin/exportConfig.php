<?php

return [
    [
        'attribute' => 'username',
        'variableType' => 'string',
        'headerConfig' => ['font-style'=>'underline'],
        'dataConfig' => [],
    ],
    [
        'attribute' => 'email',
        'variableType' => 'string',
        'dataConfig' => ['color'=>'#00f'],
    ],
    [
        'attribute' => 'authAssignment.role.name',
        'header'    => 'Роль',
        'dataConfig' => ['fill'=>'#eee']
    ],
];

?>
<?php

/**
 * @var yii\web\View         $this
 * @var backend\models\Admin $model
 */

use yii\widgets\ActiveForm;

$this->title                   = 'Изменение: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Админы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>

    <div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <?= $this->render('_form', [
                'form'  => $form,
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>

    <div class="devup-ba-form-action well well-small form-actions">
    <?= \common\helpers\Html::submitButton('<i class="fa fa-update" aria-hidden="true"></i> Обновить',
        ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
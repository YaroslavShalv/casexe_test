<?php

use backend\widgets\grid\DynaGrid;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\Admin        $model
 */

$this->title                   = 'Админы';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'username',
    ],
    [
        'attribute' => 'email',
    ],
    [
        'attribute' => 'authAssignment.role.name',
        'header'    => 'Роль'
    ],
];
?>


<?= DynaGrid::widget([
    'gridId'      => 'dynagrid-admin',
    'title'       => $this->title,
    'gridOptions' => [
        'columns'        => $columns,
        'dataProvider'   => $dataProvider,
        'filterModel'    => $model,
        'export'         => false,
        'filterPosition' => false,
        'detailColumn'   => false,
        'customExport'   => true,
    ]
]) ?>

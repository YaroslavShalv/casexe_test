<?php

use backend\widgets\grid\DynaGrid;
use kartik\widgets\Select2;
use backend\models\User;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\User         $model
 */

$this->title                   = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'attribute' => 'username',
    ],
    [
        'attribute' => 'email',
    ],
    [
        'header' => 'Очки лояльности',
        'attribute' => 'loyalityPoints.points',
    ],
];
?>

<?= DynaGrid::widget([
    'gridId'      => 'dynagrid-user',
    'title'       => $this->title,
    'gridOptions' => [
        'columns'                 => $columns,
        'dataProvider'            => $dataProvider,
        'filterModel'             => $model,
        'detailColumn'            => false,
        'toolbarApproveButton'    => false,
        'toolbarCreateButton'     => false,
        'toolbarDisapproveButton' => false,
        'actionApprove'           => false,
        'actionDisapprove'        => false,
        'statusColumn'            => false,
    ],
]) ?>

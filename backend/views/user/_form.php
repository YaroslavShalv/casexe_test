<?php

use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var yii\widgets\ActiveForm $form
 * @var backend\models\User    $model
 */

?>


<div class="box-body">
    <legend>Основные свойства</legend>


    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord) : ?>
        <legend>Дополнительная информация</legend>
        <?= $form->field($model, 'auth_key')->textInput(['disabled' => true]) ?>
        <?= $form->field($model, 'created_at')->textInput(['disabled' => true]) ?>
        <?= $form->field($model, 'updated_at')->textInput(['disabled' => true]) ?>
    <?php endif; ?>
</div>

<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View       $this
 * @var backend\models\User $model
 */

?>

<div class="row">
    <div class="col-lg-3">
        <?= $model->image_link ? Html::img($model->getImageUrl()) : '' ?>
    </div>
    <div class="col-lg-9">
        <div class="box text-center box-info">
            <div class="box-header">
                <h3>Связи</h3>
            </div>
            <div class="box-body">
                <?= Html::a('Войти на сайт', "/system/login?token={$model->auth_key}", [
                    'title'  => 'Авторизоваться на сайте под этим пользователем',
                    'class'  => 'btn btn-sm btn-warning',
                    'target' => '_blank',
                    'data'   => [
                        'pjax' => false,
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
<?php

use common\helpers\Param;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Menu;

/**
 * @var yii\web\View                            $this
 * @var \common\components\settings\Setting     $settingModel
 * @var \common\components\settings\SettingForm $settingForm
 */

$this->title                   = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-lg-7">
        <div class="nav-tabs-custom">
            <?= Menu::widget([
                'options' => ['class' => 'nav nav-tabs'],
                'items'   => Param::get('settings.menu'),
            ]) ?>
            <div class="tab-content">
                <div class="tab-pane active">
                    <?php $form = ActiveForm::begin([
                        'options'     => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
                        'fieldConfig' => [
                            'template'     => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                            'labelOptions' => ['class' => 'col-sm-2 control-label'],
                        ],
                    ]); ?>
                    <?php foreach ($settingForm->getSettings() as $key => $settingModel) : ?>
                        <?php
                        if ($settingModel->type_key == $settingModel::TYPE_TEXT) {
                            $field = $form->field($settingForm, $key);
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_TEXTAREA) {
                            $field = $form->field($settingForm, $key)->textarea(['rows'  => 8,
                                                                                 'style' => 'resize: vertical;'
                            ]);
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_EDITOR) {
                            $field = $form->field($settingForm, $key)->widget(CKEditor::class, [
                                'options' => ['rows' => 6],
                                'preset'  => 'full'
                            ]);
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_SELECTBOX) {
                            $field = $form->field($settingForm, $key)->dropDownList($settingModel->getVariants());
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_SELECTBOX_MULTIPLE) {
                            $field = $form->field($settingForm, $key)->dropDownList($settingModel->getVariants(),
                                ['multiple' => true]);
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_CHECKBOX) {
                            $field = $form->field($settingForm, $key)->checkbox();
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_RADIO) {
                            $field = $form->field($settingForm, $key)->radio();
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_RADIOLIST) {
                            $field = $form->field($settingForm, $key)->radioList($settingModel->getVariants());
                        }
                        if ($settingModel->type_key == $settingModel::TYPE_FILE) {
                            $field = $form->field($settingForm, $key)->widget(FileInput::class, [
                                'pluginOptions' => [
                                    'initialPreview'         => [
                                        $settingModel->value,
                                    ],
                                    'initialPreviewAsData'   => true,
                                    'initialPreviewFileType' => 'image',
                                    'purifyHtml'             => true,
                                    'overwriteInitial'       => false,
                                    'showPreview'            => true,
                                    'showRemove'             => false,
                                    'showUpload'             => false
                                ]
                            ]);
                        }

                        echo $field->hint($settingModel->hint);
                        ?>
                    <?php endforeach; ?>

                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
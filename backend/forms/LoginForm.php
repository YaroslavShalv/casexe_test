<?php

namespace backend\forms;

use backend\models\Admin;
use Yii;
use yii\base\Model;

/**
 * Форма авторизации
 *
 * @package backend\forms
 */
class LoginForm extends Model
{
    /** @var */
    public $username;

    /** @var */
    public $password;

    /** @var bool */
    public $rememberMe = true;

    /** @var bool */
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'username'   => 'Логин',
            'password'   => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     * @param $attribute
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            /** @var Admin $userModel */
            if (Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Admin|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Admin::findOne(['username' => $this->username, 'status_key' => Admin::STATUS_ENABLED]);
        }

        return $this->_user;
    }
}

<?php

namespace backend\forms;

use common\helpers\Html;
use common\helpers\SystemHelper;
use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * Статистика
 *
 * @package backend\forms
 */
class DashboardForm extends Model
{
    /** @return string */
    public function getSystemStat()
    {
        try {
            $memoryUsage = SystemHelper::getMemoryUsage();
        } catch (\Exception $e) {
            $memoryUsage = 0;
        }
        try {
            $cpuUsage = SystemHelper::getCpuUsage();
        } catch (\Exception $e) {
            $cpuUsage = 0;
        }
        try {
            $hddUsage = SystemHelper::getHddUsage();
        } catch (\Exception $e) {
            $hddUsage = 0;
        }

        Yii::$app->view->registerJsFile("//www.google.com/jsapi");
        Yii::$app->view->registerJsFile("//www.gstatic.com/charts/loader.js");
        Yii::$app->view->registerJs("
            google.charts.load('current', {'packages':['gauge']});
            google.charts.setOnLoadCallback(drawChart);
        
            function drawChart() {
        
              var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Memory', {$memoryUsage}],
                ['CPU', {$cpuUsage}],
                ['HDD', {$hddUsage}]
              ]);
        
              var options = {
                width: 400, height: 120,
                redFrom: 90, redTo: 100,
                yellowFrom:75, yellowTo: 90,
                minorTicks: 5
              };
        
              var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
        
              chart.draw(data, options);
            }
        ");

        return Html::tag('div', '', [
            'id'    => 'chart_div',
            'style' => 'width: 400px; height: 120px;'
        ]);
    }

    /** @return string */
    public function getPhpVersion()
    {
        return phpversion();
    }

    /** @return array */
    public function getExtensions()
    {
        return get_loaded_extensions();
    }
}
<?php

namespace backend\models;

use common\db\ActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Модель администратора
 *
 * @property string         $username
 * @property string         $email
 * @property string         $first_name
 * @property string         $last_name
 * @property string         $password_hash
 * @property string         $password_reset_token
 * @property string         $auth_key
 * @property string         $status_key
 * @property AuthAssignment $authAssignment
 */
class Admin extends ActiveRecord implements IdentityInterface
{
    /** @var string Пароль */
    public $password;

    /** @var string Подтверждение пароля */
    public $password_confirm;

    /** @var string Роль администратора */
    public $role;

    /** @return string Название таблицы */
    public static function tableName()
    {
        return '{{%admin}}';
    }

    /** @return array Правила валидации */
    public function rules()
    {
        return [
            [['email', 'username'], 'required'],
            [['password', 'password_confirm'], 'required', 'on' => 'insert'],
            [['email', 'username'], 'string', 'max' => 255],
            ['username', 'unique'],
            ['email', 'unique'],
            [['email'], 'email'],
            [['role'], 'string', 'max' => 50],
            [
                [
                    'password_reset_token',
                    'first_name',
                    'last_name'
                ],
                'string',
                'max' => 255
            ],
            ['password', 'string', 'min' => 5, 'message' => 'Пароль должен быть не короче 5 символов'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать'],
        ];
    }

    /** @return array Надписи атрибутов */
    public function attributeLabels()
    {
        return [
            'id'                   => '#',
            'email'                => 'Email',
            'username'             => 'Логин',
            'first_name'           => 'Имя',
            'last_name'            => 'Фамилия',
            'password_reset_token' => 'Токен восстановления',
            'password_hash'        => 'Хеш пароля',
            'password'             => 'Пароль',
            'password_confirm'     => 'Подтверждение пароля',
            'role'                 => 'Роль',
            'created_at'           => 'Дата создания',
        ];
    }

    /** @return array|AuthItem[] Список ролей */
    public static function roleList()
    {
        return AuthItem::find()->select(['name', 'description'])->all();
    }

    /** @inheritdoc */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Поиск записи по емейлу
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Поиск записи по токену восстановления пароля
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire    = Yii::$app->params['admin.passwordResetTokenExpire'];
        $parts     = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /** @return bool */
    public function beforeValidate()
    {
        if ($this->email) {
            mb_strtolower($this->email, 'UTF-8');
        }

        return parent::beforeValidate();
    }

    /** Выставляем роль */
    public function afterFind()
    {
        $this->role = AuthAssignment::find()->select(['item_name AS name'])->where(['user_id' => $this->id])->scalar();

        parent::afterFind();
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (isset($this->password) && !empty($this->password)) {
            $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (isset($this->role) && !empty($this->role)) {
            $auth = Yii::$app->authManager;

            $role = $auth->getRole($this->role);
            $auth->revokeAll($this->id);
            $auth->assign($role, $this->id);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /** @inheritdoc */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /** Generates new password reset token */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /** Removes password reset token */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /** @return \yii\db\ActiveQuery */
    public function getAuthAssignment()
    {
        return $this->hasOne(AuthAssignment::class, ['user_id' => 'id']);
    }

    /** @inheritdoc */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /** @inheritdoc */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /** Generates "remember me" authentication key */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
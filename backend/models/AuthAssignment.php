<?php

namespace backend\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%admin_auth_assignment}}".
 *
 * @property string   $item_name
 * @property string   $user_id
 * @property integer  $created_at
 * @property AuthItem $role
 */
class AuthAssignment extends ActiveRecord
{
    /** @return string Название таблицы */
    public static function tableName()
    {
        return '{{%admin_auth_assignment}}';
    }

    /** @return array Правила валидации */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['created_at'], 'integer'],
            [['item_name', 'user_id'], 'string', 'max' => 64],
        ];
    }

    /** @return array Надписи атрибутов */
    public function attributeLabels()
    {
        return [
            'item_name'  => 'Item Name',
            'user_id'    => 'User ID',
            'created_at' => 'Created At',
        ];
    }

    /** @return \yii\db\ActiveQuery */
    public function getRole()
    {
        return $this->hasOne(AuthItem::class, ['name' => 'item_name']);
    }

}

<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * Модель таблицы "{{%items}}".
 *
 * @property integer $id 
 * @property string $prize Назване приза
 * @property integer $status_key 
 * @property string $created_at 
 * @property string $updated_at 
 */
class Items extends \common\models\Items
{

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'prize' => 'Назване приза',
        ]);
    }

    /**
     * Attributes filter
     */
    protected function filterAttributes()
    {
        $this->_mainQuery->andFilterWhere(['like', 'prize', $this->prize]);

        parent::filterAttributes();
    }

}

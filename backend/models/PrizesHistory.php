<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * Модель таблицы "{{%prizes_history}}".
 *
 * @property integer $id 
 * @property integer $user_id ID пользователя
 * @property string $prize_key Тип приза
 * @property integer $status_key 
 * @property string $created_at 
 * @property string $updated_at 
 */
class PrizesHistory extends \common\models\PrizesHistory
{

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'user_id' => 'ID пользователя',
            'prize_key' => 'Тип приза',
            'amount' => 'Количество',
        ]);
    }

    /**
     * Attributes filter
     */
    protected function filterAttributes()
    {
        $this->_mainQuery->andFilterWhere(['=', 'user_id', $this->user_id]);
        $this->_mainQuery->andFilterWhere(['like', 'prize_key', $this->prize_key]);

        parent::filterAttributes();
    }

}

<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * Модель таблицы "{{%converting_job}}".
 *
 * @property integer $id 
 * @property integer $user_id 
 * @property integer $amount 
 * @property integer $result 
 * @property integer $status_key 
 * @property string $created_at 
 * @property string $updated_at 
 */
class ConvertingJob extends \common\models\ConvertingJob
{

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'result' => 'Result',
        ]);
    }

    /**
     * Attributes filter
     */
    protected function filterAttributes()
    {
        $this->_mainQuery->andFilterWhere(['=', 'user_id', $this->user_id]);
        $this->_mainQuery->andFilterWhere(['=', 'amount', $this->amount]);
        $this->_mainQuery->andFilterWhere(['=', 'result', $this->result]);

        parent::filterAttributes();
    }

}

<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * Модель таблицы "{{%lottery_config}}".
 *
 * @property integer $id 
 * @property string $key Ключ
 * @property string $value Значение
 * @property integer $status_key 
 * @property string $created_at 
 * @property string $updated_at 
 */
class LotteryConfig extends \common\models\LotteryConfig
{

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'key' => 'Ключ',
            'value' => 'Значение',
        ]);
    }

    /**
     * Attributes filter
     */
    protected function filterAttributes()
    {
        $this->_mainQuery->andFilterWhere(['like', 'key', $this->key]);
        $this->_mainQuery->andFilterWhere(['like', 'value', $this->value]);

        parent::filterAttributes();
    }

}

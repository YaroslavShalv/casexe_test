<?php

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * Модель таблицы "{{%loyality_points}}".
 *
 * @property integer $id 
 * @property integer $user_id ID пользователя
 * @property integer $points Очки лояльности
 * @property string $created_at 
 * @property string $updated_at 
 */
class LoyalityPoints extends \common\models\LoyalityPoints
{

    /**
     * @return array Надписи атрибутов
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'user_id' => 'ID пользователя',
            'points' => 'Очки лояльности',
        ]);
    }

    /**
     * Attributes filter
     */
    protected function filterAttributes()
    {
        $this->_mainQuery->andFilterWhere(['=', 'user_id', $this->user_id]);
        $this->_mainQuery->andFilterWhere(['=', 'points', $this->points]);

        parent::filterAttributes();
    }

}

<?php

return [
    'dynagrid' => [
        'class' => \backend\widgets\grid\DynaGridModule::class,
    ],
    'gridview' => [
        'class' => \kartik\grid\Module::class,
    ],
];
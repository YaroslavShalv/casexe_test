<?php

$arrayMenu = [
    'backend.menu' => [
        [
            'label' => 'Пользователи',
            'icon'  => 'user',
            'url'   => ['/user/index'],
        ],
        [
            'label' => 'Админы',
            'icon'  => 'user-secret',
            'url'   => ['/admin/index'],
        ],
        [
            'label' => 'Призы',
            'icon'  => 'th-list',
            'url'   => ['/item/index'],
        ],
        [
            'label' => 'История выдачи призов',
            'icon'  => 'th-list',
            'url'   => ['/prizes-history/index'],
        ],
        [
            'label' => 'Настройки',
            'icon'  => 'cogs',
            'url'   => '#',
            'items' => [
                [
                    'label' => 'Основные настройки',
                    'icon'  => 'cogs',
                    'url'   => ['/setting/index', 'section' => 'general'],
                ],
                [
                    'label' => 'Настройки лотереи',
                    'icon'  => 'cogs',
                    'url'   => ['/lottery-configs/index', 'section' => 'general'],
                ],
            ]
        ],
    ],

    'settings.menu' => [
        ['label' => 'Основные', 'url' => ['/setting/index', 'section' => 'general']],
    ]
];


return $arrayMenu;
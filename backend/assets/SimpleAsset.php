<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class SimpleAsset
 *
 * @package backend\assets
 */
class SimpleAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/simple/';

    public $css = [

    ];

    public $js = [

    ];

    public $depends = [];
}
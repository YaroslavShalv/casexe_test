<?php

namespace backend\assets;

use yii\jui\JuiAsset;
use yii\web\AssetBundle;

/**
 * Class SortableGridAsset
 *
 * @package backend\assets
 */
class SortableGridAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/sortableGrid/';

    public $js = [
        'js/sortable.js',
    ];

    public $depends = [
        JuiAsset::class
    ];
}
$(document).ready(function () {
    $(window).stuck();
    var wrapper = $('.content-wrapper');
    var footer = $(wrapper).find('.devup-ba-form-action');

    if (footer.length) {
        new Waypoint({
            element: wrapper[0],
            offset: 'bottom-in-view',
            handler: function (direction) {
                var position = $('.devup-ba-form form > .row').outerHeight() + $(footer).outerHeight() - 2;

                if (position < $(footer).offset().top) {
                    $(footer).removeClass('stuck');
                }

                if (direction == 'up') {
                    $(footer).addClass('stuck');
                }
            }
        });
    }

    handleScroll(footer, wrapper);

});


function handleScroll(footer, wrapper) {
    if (footer.length && jQuery(window).scrollTop() + jQuery(window).height() != jQuery(document).height()) {
        jQuery(footer).addClass('stuck');
    }

    jQuery(window).scroll(
        debounce(function () {
            if (footer.length && jQuery(window).scrollTop() + jQuery(window).height() == jQuery(document).height()) {
                jQuery(footer).removeClass('stuck');
            }
        }, 250)
    );

    /*jQuery('body').on('expanded.pushMenu collapsed.pushMenu', function() {
        setTimeout(function() {
            handleResize(footer, wrapper);
        }, 350); // the animation takes 0.3s to execute, so we have to take the width, just after the animation ended
    });*/

    jQuery(window).resize(
        debounce(function () {
            handleResize(footer, wrapper);
        }, 250)
    );
}

function debounce(func, wait, immediate) {
    var timeout;

    return function () {
        var context = this,
            args = arguments;

        var later = function () {
            timeout = null;

            if (!immediate) {
                func.apply(context, args);
            }
        };

        var callNow = immediate && !timeout;

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);

        if (callNow) {
            func.apply(context, args);
        }
    };
}

function handleResize(footer, wrapper) {
    if (footer.length && jQuery(footer).hasClass('stuck')) {
        jQuery(footer).width(jQuery(wrapper).outerWidth());
    }
}
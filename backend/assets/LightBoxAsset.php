<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class LightBoxAsset
 *
 * @package backend\assets
 */
class LightBoxAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/lightbox/';

    public $css = [
        'css/lightbox.css',
    ];

    public $js = [
        'js/lightbox.js',
    ];

    public $depends = [];
}
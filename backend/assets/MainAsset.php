<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/main/';

    public $css = [
        'css/skins/skin-black.css',
        'css/confirm.css',
        'css/main.css',
    ];

    public $js = [
        'js/jquery.slimscroll.min.js',
        'js/jquery.stuck.js',
        'js/confirm.js',
        'js/waypoint.js',
        'js/main.js',
    ];

    public $depends = [];
}

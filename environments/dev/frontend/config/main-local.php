<?php

$config = [
    'homeUrl'    => '/',
    'components' => [
        'request'              => [
            'baseUrl'             => '',
            'cookieValidationKey' => '',
        ],
        'assetManager'         => [
            'appendTimestamp' => true,
            'linkAssets'      => true,
        ],
    ],
];

return $config;

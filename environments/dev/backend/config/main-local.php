<?php

$baseUrl = '/adminx24';

$config = [
    'homeUrl'    => $baseUrl,
    'name'       => '',
    'components' => [
        'request'      => [
            'baseUrl'             => $baseUrl,
            'cookieValidationKey' => '',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets'      => true,
        ],
    ],
];

$config['bootstrap'][]    = 'gii';
$config['modules']['gii'] = [
    'class'      => \yii\gii\Module::class,
    'allowedIPs' => require(__DIR__ . '/../../common/config/allowedIps.php'),
    'generators' => [
        'model'     => \common\gii\generators\model\Generator::class,
        'crud'      => \common\gii\generators\crud\Generator::class,
        'migration' => \common\gii\generators\migration\Generator::class,
    ],
];

return $config;

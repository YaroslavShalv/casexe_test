<?php
return [
    'homeUrl'    => '/',
    'components' => [
        'request'              => [
            'baseUrl'             => '',
            'cookieValidationKey' => '',
        ],
        'assetManager'         => [
            'appendTimestamp' => true,
            'linkAssets'      => false,
        ],
    ],
];

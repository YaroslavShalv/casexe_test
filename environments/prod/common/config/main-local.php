<?php

$config = [
    'name'       => '',
    'components' => [
        'db'     => require(__DIR__ . '/db-local.php'),
        'mailer' => [
            'class'    => \yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
        ],
    ],
];

$logDate = date('d.m.Y');

$config['bootstrap'][]       = 'log';
$config['components']['log'] = [
    'traceLevel' => 0,
    'targets'    => [
        [
            'class'   => \yii\log\FileTarget::class,
            'levels'  => ['error'],
            'logFile' => "@runtime/logs/error_{$logDate}.log",
            'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
            'except'  => [
                'yii\web\HttpException:404',
            ],
        ],
        [
            'class'   => \yii\log\FileTarget::class,
            'levels'  => ['warning'],
            'logFile' => "@runtime/logs/warning_{$logDate}.log",
            'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
            'except'  => [
                'yii\web\HttpException:404',
            ],
        ],
    ],
];

// configuration adjustments for 'dev' environment
$config['bootstrap'][]      = 'debug';
$config['modules']['debug'] = [
    'class'      => \yii\debug\Module::class,
    'allowedIPs' => require(__DIR__ . '/allowedIps.php'),
];

return $config;

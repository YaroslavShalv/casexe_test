<?php
return [
    'class'               => \yii\db\Connection::class,
    'dsn'                 => 'mysql:host=hostName;dbname=databaseName',
    'username'            => 'username',
    'password'            => 'password',
    'charset'             => 'utf8',
    'tablePrefix'         => 'casexe_',
    'enableSchemaCache'   => true,
    'schemaCacheDuration' => 3600,
];
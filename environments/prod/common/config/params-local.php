<?php
return [
    'dev' => [
        'token' => 'XX4OzwBoErroeWgO5nNQjXjqszEFUl'
    ],
    'log' => [
        'levels' => [
            'error',
            'warning',
        ],
        'vars'   => [
            '_GET',
            '_POST',
            '_FILES',
            '_COOKIE',
            '_SESSION',
            '_SERVER',
            '_REQUEST'
        ],
    ],
];

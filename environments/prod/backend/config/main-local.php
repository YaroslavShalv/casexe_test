<?php
$baseUrl = '/adminx';

$config = [
    'homeUrl'    => $baseUrl,
    'name'       => '',
    'components' => [
        'request'      => [
            'baseUrl'             => $baseUrl,
            'cookieValidationKey' => '',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets'      => false,
        ],
    ],
];

return $config;

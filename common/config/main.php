<?php
return [
    'timezone' => 'Europe/Moscow',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => require(__DIR__ . '/components.php'),
];

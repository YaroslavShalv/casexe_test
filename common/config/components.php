<?php

return [
    'cache'  => [
        'class' => \yii\caching\FileCache::class,
    ],
    'mailer' => [
        'class' => \yii\swiftmailer\Mailer::class,
    ],
];
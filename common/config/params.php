<?php
return [
    'adminEmail'                    => 'admin@example.com',
    'user.passwordResetTokenExpire' => 3600,


    /**
     * 1) CustomExport::DRIVER_CUSTOM
     * Поддерживает форматы: XLSX, CSV
     * Использует библиотеку PHPExcel https://github.com/PHPOffice/PHPExcel
     * Для CSV кастомный скрипт.
     * Нельзя задавать параметры полей
     * Каскадная запись
     *
     * 2) CustomExport::DRIVER_SPOUT
     * Поддерживает форматы: XLSX, CSV, ODS
     * Использует библиотеку Spout
     * Выгружает всё за раз, не может дописывать файл, поэтому в js common/components/customExport/viewFiles/export/export.js
     * лучше дать зачению step = null; (6 строка)
     * Нельзя задавать параметры полей
     *
     * 3) CustomExport::DRIVER_XLSX_WRITER
     * Поддерживает форматы: XLSX, CSV
     * Использует библиотеку XLSXWriter https://github.com/mk-j/PHP_XLSXWriter
     * Для CSV кастомный скрипт.
     * Можно задавать в конфигах параметры для заголовков и данных
     * Каскадная запись
     */
    'customExportDriver' => \common\components\customExport\CustomExport::DRIVER_XLSX_WRITER,
];

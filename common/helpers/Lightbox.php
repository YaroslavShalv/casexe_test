<?php

namespace common\helpers;

/**
 * Class Lightbox
 *
 * @package common\helpers
 */
class Lightbox
{
    /**
     * @param string     $imageUrl
     * @param integer    $id
     * @param array|null $params
     * @param string     $caption
     * @return string
     */
    public static function image($imageUrl, $id, $params = null, $caption = 'Картинка')
    {
        if (is_null($params)) {
            $params = self::defaultParams($caption);
        }

        return Html::a(Html::img($imageUrl, $params), $imageUrl, ['data-lightbox' => $id, 'data-title' => $caption]);
    }

    /**
     * @param string $caption
     * @return array
     */
    private static function defaultParams($caption)
    {
        return [
            'style' => [
                'width'  => '100px',
                'height' => '100px',
            ],
            'alt'   => $caption,
        ];
    }
}
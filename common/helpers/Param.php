<?php

namespace common\helpers;

use Yii;

/**
 * Class Param
 *
 * @package common\helpers
 */
class Param
{
    /**
     * Геттер параметров приложения
     *
     * @param string     $key
     * @param null|mixed $defaultValue
     *
     * @return mixed
     */
    public static function get($key, $defaultValue = null)
    {
        return ArrayHelper::getValue(Yii::$app->params, $key, $defaultValue);
    }
}
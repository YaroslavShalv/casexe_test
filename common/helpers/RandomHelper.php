<?php

namespace common\helpers;

/**
 * Class RandomHelper
 * @package common\helpers
 */
class RandomHelper
{
    /**
     * @param $min
     * @param $max
     * @return int
     */
    public static function getRandom($min, $max)
    {
        return rand($min, $max);
    }
}
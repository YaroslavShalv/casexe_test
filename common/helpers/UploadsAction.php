<?php

namespace common\helpers;

use Exception;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

/**
 * Class UploadsAction
 *
 * @package common\helpers
 */
class UploadsAction extends Action
{
    /**
     * @var array Allowable size list
     */
    public $sizeList = [
        "50x50",
        "50x0",
        "0x50",
        "100x100",
        "100x0",
        "0x100",
        "127x127",
        "127x0",
        "0x127",
        "300x300",
        "300x0",
        "0x300",
        "150x350",
        "150x0",
        "0x350",
        "350x150",
        "350x0",
        "0x150",
    ];

    /**
     * Disabling CSRF validation
     */
    public function init()
    {
        Yii::$app->request->enableCsrfValidation = false;

        if (!is_array($this->sizeList)) {
            throw new Exception("Please, set sizeList array!");
        }

        parent::init();
    }

    /**
     * Generates size for image file
     *
     * @param int    $width Image width
     * @param int    $height Image height
     * @param string $model Model name
     * @param string $field Field name
     * @param        $dynamicPath
     * @param        $file
     * @param        $ext
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($width = 50, $height = 50, $model, $field, $dynamicPath, $file, $ext)
    {

        if (!in_array("{$width}x{$height}", $this->sizeList) && ($width > 0 || $height > 0)) {
            throw new NotFoundHttpException('size not found');
        }

        $uploads      = Yii::getAlias(FileHelper::$alias . FileHelper::$uploadDir);
        $path         = "{$model}/{$field}/{$dynamicPath}";
        $originalFile = "{$uploads}/original/{$path}/{$file}.{$ext}";

        if (!file_exists($originalFile)) {
            throw new NotFoundHttpException('file not found');
        }

        $folder = FileHelper::normalizePath("{$uploads}/{$width}x{$height}/{$path}");

        try {
            FileHelper::createDirectory($folder);
        } catch (Exception $ex) {
        }

        $image = (new Imagine())->open($originalFile);

        if (!$width || !$height) {
            $ratio = $image->getSize()->getWidth() / $image->getSize()->getHeight();
            if ($width) {
                $height = ceil($width / $ratio);
            } else {
                $width = ceil($height * $ratio);
            }
        }

        $image->thumbnail(new Box($width, $height),
            ImageInterface::THUMBNAIL_OUTBOUND)->save("{$folder}/{$file}.{$ext}")->show($ext, ['quality' => 80]);
        exit;
    }
}
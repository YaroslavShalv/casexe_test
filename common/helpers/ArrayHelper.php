<?php

namespace common\helpers;

use yii\helpers\ArrayHelper as YiiArrayHelper;

/**
 * Class ArrayHelper
 *
 * @package common\helpers
 */
class ArrayHelper extends YiiArrayHelper
{
    /**
     * Список
     *
     * @param array $items
     * @param null  $key
     * @param null  $defaultValue
     *
     * @return array|mixed
     */
    public static function getList($items = [], $key = null, $defaultValue = null)
    {
        return !is_null($key) ? ArrayHelper::getValue((array)$items, $key, $defaultValue) : $items;
    }
}
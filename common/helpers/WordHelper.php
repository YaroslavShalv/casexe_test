<?php

namespace common\helpers;

/**
 * Class WordHelper
 *
 * @package common\helpers
 */
class WordHelper
{
    /**
     * Преобразование числа
     *
     * @param $n
     * @param $forms
     * @return mixed
     */
    public static function plural($n, $forms)
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }

    /**
     * @param $str
     * @return string
     */
    public static function translit($str)
    {
        $converter = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ь' => '',
            'ы' => 'y',
            'ъ' => '',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',

            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'E',
            'Ж' => 'Zh',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'Ch',
            'Ш' => 'Sh',
            'Щ' => 'Sch',
            'Ь' => '',
            'Ы' => 'Y',
            'Ъ' => '',
            'Э' => 'E',
            'Ю' => 'Yu',
            'Я' => 'Ya',

        ];

        return strtr($str, $converter);
    }

    /**
     * обрезание текста по заданной длине
     * с учетом целостности слов
     *
     * @param     $string
     * @param int $length
     * @return string
     */
    public static function shortText($string, $length = 9)
    {
        $string  = strip_tags($string);
        $arr_str = explode(" ", $string);
        $arr     = array_slice($arr_str, 0, $length);
        $new_str = implode(" ", $arr);
        if (count($arr_str) > $length) {
            $new_str .= '...';
        }
        return $new_str;
    }

    /**
     * @param        $string
     * @param        $total_length
     * @param string $fillchar
     * @return bool|string
     */
    public static function rjust($string, $total_length, $fillchar = ' ')
    {
        // if the string is longer than the total length allowed just return it
        if (strlen($string) >= $total_length) {
            return $string;
        }

        $total_length = intval($total_length);

        // total_length must be a number greater than 0
        if (!$total_length) {
            return $string;
        }

        // the $fillchar can't be empty
        if (!strlen($fillchar)) {
            return $string;
        }

        // make the fill character into padding
        while (strlen($fillchar) < $total_length) {
            $fillchar = $fillchar . $fillchar;
        }

        return substr($fillchar . $string, (-1 * $total_length));

    }

    /**
     * @param        $string
     * @param int    $width
     * @param string $break
     * @return null|string|string[]
     */
    public static function wordWrap($string, $width = 75, $break = ' ')
    {
        return preg_replace('/(\S{' . $width . '})/u', '$1' . $break, $string);
    }

    /**
     * @param $string
     * @return string
     */
    public static function camelCaseToUnderscore($string)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        return strtolower(implode('_', $ret));
    }

    /**
     * @param string $type
     * @param int    $length
     * @return int|string
     */
    public static function randomStr($type = 'alphanum', $length = 8)
    {
        switch ($type) {
            case 'basic'    :
                return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings             = [];
                $seedings['alpha']    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num']      = '0123456789';
                $seedings['nozero']   = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }

}
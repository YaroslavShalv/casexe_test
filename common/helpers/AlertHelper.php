<?php

namespace common\helpers;

use Yii;
use yii\base\BaseObject;

/**
 * Class AlertHelper
 *
 * @package common\helpers
 */
class AlertHelper extends BaseObject
{
    const TYPE_SUCCESS = 'success';
    const TYPE_ERROR   = 'error';
    const TYPE_INFO    = 'info';
    const TYPE_WARNING = 'warning';

    /**
     * Сеттер флеш сообения - Успех
     *
     * @param string $body
     */
    public static function success($body = '')
    {
        static::set(static::TYPE_SUCCESS, $body);
    }

    /**
     * Сеттер флеш сообения - Ошибка
     *
     * @param string $body
     */
    public static function error($body = '')
    {
        static::set(static::TYPE_ERROR, $body);
    }

    /**
     * Сеттер флеш сообения - Информация
     *
     * @param $body
     */
    public static function info($body = '')
    {
        static::set(static::TYPE_INFO, $body);
    }

    /**
     * Сеттер флеш сообения - Предупреждение
     *
     * @param $body
     */
    public static function warning($body = '')
    {
        static::set(static::TYPE_WARNING, $body);
    }

    /**
     * @param string $type
     * @param string $body
     */
    public static function set($type, $body = '')
    {
        if (!empty(Yii::$app->session)) {
            Yii::$app->session->setFlash($type, $body);
        }
    }

    /**
     * Геттер флеш сообщений
     */
    public static function show()
    {
        $flashes = Yii::$app->session->getAllFlashes();

        foreach ($flashes as $type => $body) {
            echo Html::tags([
                'div' => [
                    'options' => [
                        'class' => [
                            'alert',
                            'alert-dismissable',
                            'alert-' . static::getStyles($type, 'default')
                        ]
                    ],
                    'i'       => [
                        'options' => [
                            'class' => [static::getIcons($type, 'check')],
                            'style' => 'padding-right:10px'
                        ],
                    ],
                    'button'  => [
                        'options' => [
                            'type'         => 'button',
                            'class'        => 'close',
                            'data-dismiss' => 'alert',
                            'aria-hidden'  => 'true',
                        ],
                        'x'
                    ],
                    $body
                ],
            ]);
        }
    }

    /**
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getIcons($key = null, $defaultValue = null)
    {
        $items = [
            static::TYPE_SUCCESS => 'fa fa-check',
            static::TYPE_ERROR   => 'fa fa-ban',
            static::TYPE_INFO    => 'fa fa-info',
            static::TYPE_WARNING => 'fa fa-warning',
        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key, $defaultValue) : $items;
    }

    /**
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getStyles($key = null, $defaultValue = null)
    {
        $items = [
            static::TYPE_SUCCESS => 'success',
            static::TYPE_ERROR   => 'danger',
            static::TYPE_INFO    => 'info',
            static::TYPE_WARNING => 'warning',
        ];

        return !is_null($key) ? ArrayHelper::getValue($items, $key, $defaultValue) : $items;
    }
}
<?php

/**
 * @var yii\web\View                              $this
 * @var common\gii\generators\migration\Generator $generator
 * @var string                                    $tableName table name
 * @var string                                    $migrationName name of current migration
 * @var string                                    $baseClass base migration class name
 * @var array                                     $columns columns list for migration
 * @var array                                     $foreignKeys foreign keys for current table
 * @var array                                     $indexes indexes for current table
 */

echo "<?php\n";
?>

class <?= $migrationName ?> extends <?= $baseClass . "\n" ?>
{
    protected $_tableName = '<?= $tableName ?>';

    public function safeUp()
    {
        $this->createTable($this->_tableName, [
        <?php foreach ($columns as $name => $column) : ?>
        '<?= $name ?>' => <?= $column ?>,
        <?php endforeach; ?>
        ], $this->_tableOptions);

        <?php foreach ($foreignKeys as $foreignKey) : ?>
        <?= $foreignKey . "\n" ?>
        <?php endforeach; ?>
        <?php foreach ($indexes as $index) : ?>
        <?= $index . "\n" ?>
        <?php endforeach; ?>
    }
}

<?php

use common\helpers\Html;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;

/**
 * @var yii\web\View                              $this
 * @var yii\widgets\ActiveForm                    $form
 * @var common\gii\generators\migration\Generator $generator
 */

echo $form->field($generator, 'tableName')->textInput();
echo $form->field($generator, 'migrationName')->textInput();
echo $form->field($generator, 'migrationPath');
echo $form->field($generator, 'baseClass');
echo $form->field($generator, 'useTablePrefix')->checkbox();
echo $form->field($generator, 'db');

echo Html::beginTag('div', ['class' => 'container']);
echo Html::beginTag('div', ['class' => 'row']);
echo Html::beginTag('div', ['class' => 'col-lg-9']);
echo $form->field($generator, 'columns')->widget(MultipleInput::class, [
    'min'               => 1,
    'max'               => 30,
    'allowEmptyList'    => false,
    'addButtonPosition' => [MultipleInput::POS_HEADER, MultipleInput::POS_FOOTER],
    'columns'           => [
        [
            'name'  => 'name',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Название',
        ],
        [
            'name'  => 'type',
            'type'  => MultipleInputColumn::TYPE_DROPDOWN,
            'title' => 'Тип',
            'items' => [
                'primaryKey'    => 'pk',
                'integer'       => 'int',
                'smallInteger'  => 'smallint',
                'string'        => 'string',
                'text'          => 'text',
                'dateTime'      => 'datetime',
                'boolean'       => 'boolean',
                'float'         => 'float',
                'double'        => 'double',
                'decimal'       => 'decimal',
                'timestamp'     => 'timestamp',
                'time'          => 'time',
                'date'          => 'date',
                'bigInteger'    => 'bigint',
                'bigPrimaryKey' => 'bigpk',
                'binary'        => 'binary',
                'char'          => 'char',
                'money'         => 'money',
            ],
        ],
        [
            'name'  => 'length',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Длина',
        ],
        [
            'name'  => 'isNotNull',
            'type'  => MultipleInputColumn::TYPE_CHECKBOX,
            'title' => 'NOT NULL',
        ],
        [
            'name'  => 'isUnsigned',
            'type'  => MultipleInputColumn::TYPE_CHECKBOX,
            'title' => 'Без знака',
        ],
        [
            'name'  => 'isUnique',
            'type'  => MultipleInputColumn::TYPE_CHECKBOX,
            'title' => 'unique',
        ],
        [
            'name'  => 'default',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'По умолчанию',
        ],
        [
            'name'  => 'comment',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Комментарий',
        ],
    ],
]);

echo $form->field($generator, 'foreignKeys')->widget(MultipleInput::class, [
    'min'               => 0,
    'max'               => 30,
    'allowEmptyList'    => true,
    'addButtonPosition' => [MultipleInput::POS_HEADER, MultipleInput::POS_FOOTER],
    'columns'           => [
        [
            'name'  => 'name',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Название',
        ],
        [
            'name'  => 'column',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Поле',
        ],
        [
            'name'  => 'refTable',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Таблица',
        ],
        [
            'name'  => 'refColumn',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Поле',
        ],
        [
            'name'  => 'onUpdate',
            'type'  => MultipleInputColumn::TYPE_DROPDOWN,
            'title' => 'При обновлении',
            'items' => [
                'CASCADE'  => 'CASCADE',
                'RESTRICT' => 'RESTRICT',
                'SET NULL' => 'SET NULL',
            ],
        ],
        [
            'name'  => 'onDelete',
            'type'  => MultipleInputColumn::TYPE_DROPDOWN,
            'title' => 'При удалении',
            'items' => [
                'CASCADE'  => 'CASCADE',
                'RESTRICT' => 'RESTRICT',
                'SET NULL' => 'SET NULL',
            ],
        ],
    ],
]);

echo $form->field($generator, 'indexes')->widget(MultipleInput::class, [
    'min'               => 0,
    'max'               => 30,
    'allowEmptyList'    => true,
    'addButtonPosition' => [MultipleInput::POS_HEADER, MultipleInput::POS_FOOTER],
    'columns'           => [
        [
            'name'  => 'name',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Название',
        ],
        [
            'name'  => 'columns',
            'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
            'title' => 'Поля',
        ],
        [
            'name'  => 'isUnique',
            'type'  => MultipleInputColumn::TYPE_CHECKBOX,
            'title' => 'unique',
        ],
    ],
]);
echo Html::endTag('div');
echo Html::endTag('div');
echo Html::endTag('div');

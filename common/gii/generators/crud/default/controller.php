<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass      = StringHelper::basename($generator->modelClass);

/* @var $class ActiveRecordInterface */
$class               = $generator->modelClass;
$pks                 = $class::primaryKey();
$urlParams           = $generator->generateUrlParams();
$actionParams        = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use <?= ltrim($generator->modelClass, '\\') ?>;

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{
    /** @var string Модель для CRUD */
    public $modelClass = <?= $modelClass ?>::class;

    public $disabledActions = [

    ];
}

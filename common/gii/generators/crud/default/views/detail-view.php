<?php

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\crud\Generator */

$contentShortColumn = $generator->getTableSchema()->getColumn('content_short');
$contentFullColumn  = $generator->getTableSchema()->getColumn('content_full');
$imageColumn        = $generator->getTableSchema()->getColumn('image_link');

echo "<?php\n";
?>

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> $model
 */

?>

<div class="row">
<?php if ($generator->getTableSchema()->getColumn('image_link')): ?>
    <div class="col-lg-12">
        <div class="box text-center box-info">
            <div class="box-header">
                <h3>Картинка</h3>
            </div>
            <div class="box-body">
                <img src="<?= "<?= " ?>$model->getImageUrl() ?>" alt="">
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($contentShortColumn || $contentFullColumn) : ?>
    <div class="col-lg-12">
        <div class="box text-center box-info">
            <div class="box-header">
                <h3>Картинка</h3>
            </div>
            <div class="box-body">
<?php if ($contentShortColumn) : ?>
                <?= "<?= " ?>$model->content_short ?>
<?php endif; ?>
<?php if ($contentFullColumn) : ?>
                <?= "<?= " ?>$model->content_full ?>
<?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="col-lg-12">
        <div class="box text-center box-info">
            <div class="box-header">
                <h3></h3>
            </div>
            <div class="box-body">

            </div>
        </div>
    </div>
</div>
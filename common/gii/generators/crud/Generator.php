<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\gii\generators\crud;

use backend\widgets\BooleanColumn;
use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Generates CRUD
 *
 * @property array                       $columnNames Model column names. This property is read-only.
 * @property string                      $controllerID The controller ID (without the module ID prefix). This property
 *     is read-only.
 * @property array                       $searchAttributes Searchable attributes. This property is read-only.
 * @property boolean|\yii\db\TableSchema $tableSchema This property is read-only.
 * @property string                      $viewPath The controller view path. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{
    public $modelClass;
    public $controllerClass;
    public $viewPath;
    public $baseControllerClass = \backend\base\controllers\Controller::class;

    /** @inheritdoc */
    public function getName()
    {
        return 'CRUD Generator';
    }

    /** @inheritdoc */
    public function getDescription()
    {
        return 'This generator generates a controller and views that implement CRUD (Create, Read, Update, Delete)
            operations for the specified data model.';
    }

    /** @inheritdoc */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['controllerClass', 'modelClass', 'baseControllerClass'], 'filter', 'filter' => 'trim'],
            [['modelClass', 'controllerClass', 'baseControllerClass'], 'required'],
            [
                ['modelClass', 'controllerClass', 'baseControllerClass'],
                'match',
                'pattern' => '/^[\w\\\\]*$/',
                'message' => 'Only word characters and backslashes are allowed.'
            ],
            [['modelClass'], 'validateClass', 'params' => ['extends' => BaseActiveRecord::class]],
            [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::class]],
            [
                ['controllerClass'],
                'match',
                'pattern' => '/Controller$/',
                'message' => 'Controller class name must be suffixed with "Controller".'
            ],
            [
                ['controllerClass'],
                'match',
                'pattern' => '/(^|\\\\)[A-Z][^\\\\]+Controller$/',
                'message' => 'Controller class name must start with an uppercase letter.'
            ],
            [['controllerClass'], 'validateNewClass'],
            [['modelClass'], 'validateModelClass'],
            ['viewPath', 'safe'],
        ]);
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'modelClass'          => 'Model Class',
            'controllerClass'     => 'Controller Class',
            'viewPath'            => 'View Path',
            'baseControllerClass' => 'Base Controller Class',
        ]);
    }

    /** @inheritdoc */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'modelClass'          => 'This is the ActiveRecord class associated with the table that CRUD will be built upon.
                You should provide a fully qualified class name, e.g., <code>app\models\Post</code>.',
            'controllerClass'     => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
            'viewPath'            => 'Specify the directory for storing the view scripts for the controller. You may use path alias here, e.g.,
                <code>/var/www/basic/controllers/views/post</code>, <code>@app/views/post</code>. If not set, it will default
                to <code>@app/views/ControllerID</code>',
            'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',
        ]);
    }

    /** @inheritdoc */
    public function requiredTemplates()
    {
        return ['controller.php'];
    }

    /** @inheritdoc */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass']);
    }

    /** Checks if model class is valid */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk    = $class::primaryKey();
        if (empty($pk)) {
            $this->addError('modelClass', "The table associated with $class must have primary key(s).");
        }
    }

    /** @inheritdoc */
    public function generate()
    {
        $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');

        $files = [
            new CodeFile($controllerFile, $this->render('controller.php')),
        ];

        $viewPath     = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views';
        foreach (scandir($templatePath) as $file) {
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file"));
            }
        }

        return $files;
    }

    /** @return string the controller ID (without the module ID prefix) */
    public function getControllerID()
    {
        $pos   = strrpos($this->controllerClass, '\\');
        $class = substr(substr($this->controllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /** @return string the controller view path */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@backend/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pk    = $class::primaryKey();

        return $pk[0];
    }

    /**
     * Generates code for active field
     *
     * @param string $attribute
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function generateActiveField($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];
        if (stripos($column->name, '_key') !== false) {
            return "\$form->field(\$model, '$attribute')->dropDownList(\$model::get" . ucfirst(Inflector::pluralize(Inflector::id2camel(str_replace('_key',
                    '', $column->name), '_'))) . "(), ['prompt' => ''])";
        } elseif (stripos($column->name, '_link') !== false) {
            return "\$form->field(\$model, '$attribute')->widget(\\kartik\\widgets\\FileInput::class, [
				'options' => ['accept' => 'image/*'],
				'pluginOptions' => [
					'initialPreview' => !empty(\$model->{$column->name}) ? Html::img(\$model->get" . ucfirst(Inflector::id2camel(str_replace('_link',
                    '', $column->name), '_')) . "Url(), ['class'=>'file-preview-image']) : '',
					'overwriteInitial' => true,
					'showRemove' => false,
					'showUpload' => false
				]
			])";
        } elseif ($column->phpType === 'boolean') {
            return "\$form->field(\$model, '$attribute')->checkbox()";
        } elseif ($column->type === 'text') {
            return "\$form->field(\$model, '$attribute')->textarea(['rows' => 6])";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }
                return "\$form->field(\$model, '$attribute')->dropDownList(" . preg_replace("/\n\s*/", ' ',
                        VarDumper::export($dropDownOptions)) . ", ['prompt' => ''])";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
                return "\$form->field(\$model, '$attribute')->$input()";
            } else {
                return "\$form->field(\$model, '$attribute')->$input(['maxlength' => true])";
            }
        }
    }

    /**
     * Generates column format
     *
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    public function generateColumnFormat($column)
    {
        if (stripos($column->name, '_key') !== false && $column->name !== 'status_key') {
            return 'list';
        } elseif ($column->phpType === 'boolean') {
            return 'boolean';
        } elseif ($column->type === 'text') {
            return 'text';
        } elseif (stripos($column->name, '_at') !== false) {
            return 'datetime';
        } elseif (stripos($column->name, '_id') !== false) {
            return 'fk';
        } elseif (stripos($column->name, '_link') !== false) {
            return 'image';
        } elseif (stripos($column->name, 'email') !== false) {
            return 'email';
        } elseif (stripos($column->name, 'url') !== false) {
            return 'url';
        } else {
            return 'string';
        }
    }

    /**
     * Generates URL parameters
     *
     * @return string
     */
    public function generateUrlParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks   = $class::primaryKey();
        if (count($pks) === 1) {
            if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                return "'id' => (string)\$model->{$pks[0]}";
            } else {
                return "'id' => \$model->{$pks[0]}";
            }
        } else {
            $params = [];
            foreach ($pks as $pk) {
                if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                    $params[] = "'$pk' => (string)\$model->$pk";
                } else {
                    $params[] = "'$pk' => \$model->$pk";
                }
            }

            return implode(', ', $params);
        }
    }

    /**
     * Generates action parameters
     *
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks   = $class::primaryKey();
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks);
        }
    }

    /**
     * Generates parameter tags for phpdoc
     *
     * @return array parameter tags for phpdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks   = $class::primaryKey();
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * @return bool|\yii\db\TableSchema
     * @throws \yii\base\InvalidConfigException
     */
    public function getTableSchema()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, ActiveRecord::class)) {
            return $class::getTableSchema();
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getColumnNames()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, ActiveRecord::class)) {
            return $class::getTableSchema()->getColumnNames();
        } else {
            /* @var $model \yii\base\Model */
            $model = new $class();

            return $model->attributes();
        }
    }

    /**  */
    public function generateColumns()
    {
        $disallowedColumns = [
            'id',
            'created_at',
            'updated_at',
            'content_short',
            'content_full',
            'image_link',
            'preview_link',
            'position'
        ];
        $result            = [];

        foreach ($this->getTableSchema()->columns as $column) {
            if (!empty($column->name) && in_array($column->name, $disallowedColumns)) {
                continue;
            }

            $result[$column->name] = [
                'attribute' => '\'' . $column->name . '\'',
            ];

            $format = $this->generateColumnFormat($column);

            switch ($format) {
                case 'boolean' :
                    $result[$column->name]['class']      = BooleanColumn::class . '::class';
                    $result[$column->name]['trueLabel']  = '\'Да\'';
                    $result[$column->name]['falseLabel'] = '\'Нет\'';
                    break;
                case 'list' :
                    $camelName                       = Inflector::id2camel(str_replace('_key', '', $column->name), '_');
                    $result[$column->name]['filter'] = '$model::get' . ucfirst(Inflector::pluralize($camelName)) . '()';
                    $result[$column->name]['value']  = '\'' . $camelName . '\'';
                    break;
                case 'url' :
                    $result[$column->name]['format'] = '\'raw\'';
                    $result[$column->name]['value']  = "function(\$model){
                        return \\common\\helpers\\Html::a(\$model->{$column->name}, \$model->{$column->name}, ['target' => '_blank']);
                    }";
                    break;
                case 'text' :
                    continue;
                    break;
            }
        }

        return $result;
    }
}

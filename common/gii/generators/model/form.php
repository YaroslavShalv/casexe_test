<?php

use common\gii\generators\model\Generator;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator common\gii\generators\model\Generator */

echo $form->field($generator, 'tableName')->textInput(['table_prefix' => $generator->getTablePrefix()]);
echo $form->field($generator, 'modelClass');

echo $form->field($generator, 'ns');
echo $form->field($generator, 'db');
echo $form->field($generator, 'useTablePrefix')->checkbox();

echo $form->field($generator, 'generateRelations')->dropDownList([
    Generator::RELATIONS_NONE        => 'Без связей',
    Generator::RELATIONS_ALL         => 'Все связи',
    Generator::RELATIONS_ALL_INVERSE => 'Все связи с инверсией',
]);
echo $form->field($generator, 'generateLabelsFromComments')->checkbox();
echo $form->field($generator, 'generateFilterAttributes')->checkbox();
echo $form->field($generator, 'generateQuery')->checkbox();
echo $form->field($generator, 'queryNs');
echo $form->field($generator, 'queryClass');
echo $form->field($generator, 'queryBaseClass');

echo $form->field($generator, 'generateApiModel')->checkbox();
echo $form->field($generator, 'apiNs');

echo $form->field($generator, 'generateBackendModel')->checkbox();
echo $form->field($generator, 'generateUpdateUrl')->checkbox();
echo $form->field($generator, 'backendNs');

echo $form->field($generator, 'generateConsoleModel')->checkbox();
echo $form->field($generator, 'consoleNs');

echo $form->field($generator, 'generateFrontendModel')->checkbox();
echo $form->field($generator, 'frontendNs');

echo $form->field($generator, 'enableI18N')->checkbox();
echo $form->field($generator, 'messageCategory');
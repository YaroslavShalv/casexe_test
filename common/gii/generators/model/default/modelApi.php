<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator common\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

$filterRules = $generator->generateFilterRules($tableSchema);

$imageLinkColumn = $tableSchema->getColumn('image_link');

echo "<?php\n";
?>

namespace <?= $generator->apiNs ?>;

use yii\helpers\ArrayHelper;

/**
 * Модель таблицы "<?= $generator->generateTableName($tableName) ?>".
 *
<?php foreach ($tableSchema->columns as $column): ?>
 * @property <?= "{$column->phpType} \${$column->name}\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->ns, '\\') . '\\' . $className . "\n" ?>
{
    /**
     * @return array
     */
    public function fields()
    {
        $items = parent::fields();
<?php if ($imageLinkColumn) : ?>

        $items['image_link'] = 'imageUrl';
<?php endif; ?>

        return $items;
    }

    /**
     * @return array
     */
    public function excludeFields()
    {
        return ArrayHelper::merge(parent::excludeFields(), [
<?php if ($tableSchema->getColumn('status_key')) : ?>
            'status_key',
<?php endif; ?>
        ]);
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        $items = parent::extraFields();

<?php if ($tableSchema->getColumn('created_at')) : ?>
        $items['created_at'] = 'created';
<?php endif; ?>
<?php if ($tableSchema->getColumn('updated_at')) : ?>
        $items['updated_at'] = 'updated';
<?php endif; ?>

        return $items;
    }

<?php foreach ($relations as $name => $relation): ?>
    /**
     * @return \yii\db\ActiveQuery
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }

<?php endforeach; ?>
}

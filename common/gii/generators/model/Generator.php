<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\gii\generators\model;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\ColumnSchema;
use yii\db\Connection;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{
    const RELATIONS_NONE        = 'none';
    const RELATIONS_ALL         = 'all';
    const RELATIONS_ALL_INVERSE = 'all-inverse';

    public $templates = [
        'custom'  => '@common/gii/generators/model/custom',
        'default' => '@common/gii/generators/model/default'
    ];
    public $template = 'custom';

    public $db = 'db';
    public $ns = 'common\models';

    public $generateApiModel = true;
    public $generateBackendModel = true;
    public $generateUpdateUrl = true;
    public $generateFrontendModel = false;
    public $generateConsoleModel = true;
    public $apiNs = 'api\models';
    public $backendNs = 'backend\models';
    public $frontendNs = 'frontend\models';
    public $consoleNs = 'console\models';

    public $tableName;
    public $modelClass;
    public $baseClass = ActiveRecord::class;
    public $baseDbClass = \common\db\ActiveRecord::class;
    public $generateRelations = self::RELATIONS_ALL;
    public $generateLabelsFromComments = true;
    public $generateFilterAttributes = true;
    public $useTablePrefix = true;
    public $useSchemaName = true;
    public $generateQuery = false;
    public $queryNs = 'common\models\query';
    public $queryClass;
    public $queryBaseClass = ActiveQuery::class;

    public $excludeNames = ['id', 'created_at', 'updated_at', 'status_key', 'position'];
    protected $tableNames;
    protected $classNames;

    /** @inheritdoc */
    public function getName()
    {
        return 'Генератор модели';
    }

    /** @inheritdoc */
    public function getDescription()
    {
        return 'Генерирует модель для нужной таблицы.';
    }

    /**
     * Returns the message to be displayed when the newly generated code is saved successfully.
     * Child classes may override this method to customize the message.
     *
     * @return string the message to be displayed when the newly generated code is saved successfully.
     */
    public function successMessage()
    {
        return 'Код успешно сгенерирован.';
    }

    /** @inheritdoc */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'db',
                    'ns',
                    'tableName',
                    'modelClass',
                    'baseClass',
                    'queryNs',
                    'apiNs',
                    'backendNs',
                    'frontendNs',
                    'consoleNs',
                    'queryClass',
                    'queryBaseClass'
                ],
                'filter',
                'filter' => 'trim'
            ],
            [['ns', 'queryNs'], 'filter', 'filter' => function ($value) { return trim($value, '\\'); }],

            [['db', 'ns', 'tableName', 'baseClass', 'queryNs', 'queryBaseClass'], 'required'],
            [
                ['db', 'modelClass', 'queryClass'],
                'match',
                'pattern' => '/^\w+$/',
                'message' => 'Разрешены только буквенные символы.'
            ],
            [
                ['ns', 'baseClass', 'queryNs', 'apiNs', 'backendNs', 'frontendNs', 'consoleNs', 'queryBaseClass'],
                'match',
                'pattern' => '/^[\w\\\\]+$/',
                'message' => 'Разрешены только буквенные символы и обратные слеши.'
            ],
            [
                ['tableName'],
                'match',
                'pattern' => '/^([\w ]+\.)?([\w\* ]+)$/',
                'message' => 'Только буквенные символы, оционально пробелы и звездочка с точкой.'
            ],
            [['db'], 'validateDb'],
            [['ns', 'queryNs', 'apiNs', 'backendNs', 'frontendNs', 'consoleNs'], 'validateNamespace'],
            [['tableName'], 'validateTableName'],
            [['modelClass'], 'validateModelClass', 'skipOnEmpty' => false],
            [['baseClass'], 'validateClass', 'params' => ['extends' => ActiveRecord::class]],
            [['queryBaseClass'], 'validateClass', 'params' => ['extends' => ActiveQuery::class]],
            [
                ['generateRelations'],
                'in',
                'range' => [self::RELATIONS_NONE, self::RELATIONS_ALL, self::RELATIONS_ALL_INVERSE]
            ],
            [
                [
                    'generateLabelsFromComments',
                    'useTablePrefix',
                    'useSchemaName',
                    'generateQuery',
                    'generateFilterAttributes',
                    'generateApiModel',
                    'generateBackendModel',
                    'generateConsoleModel',
                    'generateFrontendModel',
                ],
                'boolean'
            ],
            [['enableI18N'], 'boolean'],
            [['messageCategory'], 'validateMessageCategory', 'skipOnEmpty' => false],
        ]);
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'ns'                         => 'Пространство имен',
            'apiNs'                      => 'Пространство имен для модели АПИ',
            'backendNs'                  => 'Пространство имен для модели Админки',
            'frontendNs'                 => 'Пространство имен для модели сайта',
            'consoleNs'                  => 'Пространство имен для модели консоли',
            'generateApiModel'           => 'Генерировать модель АПИ',
            'generateBackendModel'       => 'Генерировать модель админки',
            'generateUpdateUrl'          => 'Генерировать ссылку на изменение модели',
            'generateConsoleModel'       => 'Генерировать модель консоли',
            'generateFrontendModel'      => 'Генерировать модель сайта',
            'db'                         => 'Подключение к базе',
            'tableName'                  => 'Название таблицы',
            'modelClass'                 => 'Класс модели',
            'baseClass'                  => 'Базовый класс',
            'generateRelations'          => 'Генерировать связи',
            'generateLabelsFromComments' => 'Генерировать надписи из комментариев',
            'generateFilterAttributes'   => 'Генерировать фильтрацию по атрибутам модели',
            'generateQuery'              => 'Генерировать ActiveQuery для модели',
            'queryNs'                    => 'Пространство имен для ActiveQuery',
            'queryClass'                 => 'Класс ActiveQuery',
            'queryBaseClass'             => 'Базовый класс ActiveQuery',
            'useSchemaName'              => 'Использовать имя схемы',
            'messageCategory'            => 'Категория сообщений',
            'enableI18N'                 => 'Включить многоязычность',
            'useTablePrefix'             => 'Использовать префикс таблиц',
        ]);
    }

    /** @inheritdoc */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'tableName' => 'Название таблицы БД, с которой связан генерируемый класс модели, например: <code>user</code>.
                Название таблицы может указываться с названием схемы, например: <code>public.user</code>.
                Название таблицы может заканчиваться звездочкой, чтобы указать несколько названий таблиц, например: <code>dvlp_*</code>
                будет соответствовать таблицам, которые начинаются с названия <code>dvlp_</code>. В этом случае, несколько классов моделей
                будет генерироваться, по одному для каждого имени таблицы; и имена классов будут генерироваться из
                совпадающих символов. Например, для таблицы <code>dvlp_user</code> сгенерируется класс <code>User</code>.',
        ]);
    }

    /** @inheritdoc */
    public function autoCompleteData()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return [
                'tableName' => function () use ($db) {
                    return $db->getSchema()->getTableNames();
                },
            ];
        } else {
            return [];
        }
    }

    /** @inheritdoc */
    public function requiredTemplates()
    {
        return ['model.php'/*, 'query.php'*/];
    }

    /** @inheritdoc */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(),
            ['ns', 'db', 'baseClass', 'generateRelations', 'generateLabelsFromComments', 'queryNs', 'queryBaseClass']);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getTablePrefix()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return $db->tablePrefix;
        } else {
            return '';
        }
    }

    /** @inheritdoc */
    public function generate()
    {
        $files     = [];
        $relations = $this->generateRelations();
        $db        = $this->getDbConnection();
        foreach ($this->getTableNames() as $tableName) {
            // model :
            $modelClassName           = $this->generateClassName($tableName);
            $queryClassName           = ($this->generateQuery) ? $this->generateQueryClassName($modelClassName) : false;
            $tableSchema              = $db->getTableSchema($tableName);
            $params                   = [
                'tableName'      => $tableName,
                'className'      => $modelClassName,
                'queryClassName' => $queryClassName,
                'tableSchema'    => $tableSchema,
                'labels'         => $this->generateLabels($tableSchema),
                'rules'          => $this->generateRules($tableSchema),
                'relations'      => isset($relations[$tableName]) ? $relations[$tableName] : [],
            ];
            $params['queryClassName'] = $queryClassName;
            $params['modelClassName'] = $modelClassName;
            $files[]                  = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                        $this->ns)) . '/' . $modelClassName . '.php', $this->render('model.php', $params));

            // query :
            if ($queryClassName) {
                $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                            $this->queryNs)) . '/' . $queryClassName . '.php', $this->render('query.php', $params));
            }

            // api :
            if ($this->generateApiModel && $this->apiNs) {
                $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                            $this->apiNs)) . '/' . $modelClassName . '.php', $this->render('modelApi.php', $params));
            }

            // backend :
            if ($this->generateBackendModel && $this->backendNs) {
                $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                            $this->backendNs)) . '/' . $modelClassName . '.php',
                    $this->render('modelBackend.php', $params));
            }

            // console :
            if ($this->generateConsoleModel && $this->consoleNs) {
                $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                            $this->consoleNs)) . '/' . $modelClassName . '.php',
                    $this->render('modelConsole.php', $params));
            }

            // frontend :
            if ($this->generateFrontendModel && $this->frontendNs) {
                $files[] = new CodeFile(Yii::getAlias('@' . str_replace('\\', '/',
                            $this->frontendNs)) . '/' . $modelClassName . '.php',
                    $this->render('modelFrontend.php', $params));
            }
        }

        return $files;
    }

    /**
     * Generates the attribute labels for the specified table.
     *
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated attribute labels (name => label)
     */
    public function generateLabels($table)
    {
        $labels = [];
        foreach ($table->columns as $column) {
            if (in_array($column->name, $this->excludeNames)) {
                continue;
            }
            if ($this->generateLabelsFromComments && !empty($column->comment)) {
                $labels[$column->name] = $column->comment;
            } elseif (!strcasecmp($column->name, 'id')) {
                $labels[$column->name] = 'ID';
            } else {
                $label = Inflector::camel2words($column->name);
                if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                    $label = substr($label, 0, -3) . ' ID';
                }
                $labels[$column->name] = $label;
            }
        }

        return $labels;
    }

    /**
     * @param $table
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function generateRules($table)
    {
        $types   = [];
        $lengths = [];
        foreach ($table->columns as $column) {

            if ($column->autoIncrement || in_array($column->name, $this->excludeNames)) {
                continue;
            }
            if (!$column->allowNull && $column->defaultValue === null && !in_array($column->name, ['slug'])) {
                $types['required'][] = $column->name;
            }
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case 'double': // Schema::TYPE_DOUBLE, which is available since Yii 2.0.3
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][] = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $types['safe'][] = $column->name;
                    break;
                default: // strings
                    if ($column->size > 0) {
                        $lengths[$column->size][] = $column->name;
                    } else {
                        $types['string'][] = $column->name;
                    }
            }
        }
        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }
        foreach ($lengths as $length => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], 'string', 'max' => $length]";
        }

        $db = $this->getDbConnection();

        // Unique indexes rules
        try {
            $uniqueIndexes = $db->getSchema()->findUniqueIndexes($table);
            foreach ($uniqueIndexes as $uniqueColumns) {
                // Avoid validating auto incremental columns
                if (!$this->isColumnAutoIncremental($table, $uniqueColumns)) {
                    $attributesCount = count($uniqueColumns);

                    if ($attributesCount === 1) {
                        $rules[] = "[['" . $uniqueColumns[0] . "'], 'unique']";
                    } elseif ($attributesCount > 1) {
                        $labels      = array_intersect_key($this->generateLabels($table), array_flip($uniqueColumns));
                        $lastLabel   = array_pop($labels);
                        $columnsList = implode("', '", $uniqueColumns);
                        $rules[]     = "[['$columnsList'], 'unique', 'targetAttribute' => ['$columnsList'], 'message' => 'The combination of " . implode(', ',
                                $labels) . " and $lastLabel has already been taken.']";
                    }
                }
            }
        } catch (NotSupportedException $e) {
            // doesn't support unique indexes information...do nothing
        }

        // Exist rules for foreign keys
        foreach ($table->foreignKeys as $refs) {
            $refTable       = $refs[0];
            $refTableSchema = $db->getTableSchema($refTable);
            if ($refTableSchema === null) {
                // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                continue;
            }
            $refClassName = $this->generateClassName($refTable);
            unset($refs[0]);
            $attributes       = implode("', '", array_keys($refs));
            $targetAttributes = [];
            foreach ($refs as $key => $value) {
                $targetAttributes[] = "'$key' => '$value'";
            }
            $targetAttributes = implode(', ', $targetAttributes);
            $rules[]          = "[['$attributes'], 'exist', 'skipOnError' => true, 'targetClass' => $refClassName::class, 'targetAttribute' => [$targetAttributes]]";
        }

        return $rules;
    }

    /** Validates the [[db]] attribute. */
    public function validateDb()
    {
        if (!Yii::$app->has($this->db)) {
            $this->addError('db', 'There is no application component named "db".');
        } elseif (!Yii::$app->get($this->db) instanceof Connection) {
            $this->addError('db', 'The "db" application component must be a DB connection instance.');
        }
    }

    /**
     * Validates the namespace.
     *
     * @param string $attribute Namespace variable.
     */
    public function validateNamespace($attribute)
    {
        $value = $this->$attribute;
        $value = ltrim($value, '\\');
        $path  = Yii::getAlias('@' . str_replace('\\', '/', $value), false);
        if ($path === false) {
            $this->addError($attribute, 'Namespace must be associated with an existing directory.');
        }
    }

    /** Validates the [[modelClass]] attribute. */
    public function validateModelClass()
    {
        if ($this->isReservedKeyword($this->modelClass)) {
            $this->addError('modelClass', 'Class name cannot be a reserved PHP keyword.');
        }
        if ((empty($this->tableName) || substr_compare($this->tableName, '*', -1, 1)) && $this->modelClass == '') {
            $this->addError('modelClass', 'Model Class cannot be blank if table name does not end with asterisk.');
        }
    }

    /** Validates the [[tableName]] attribute. */
    public function validateTableName()
    {
        if (strpos($this->tableName, '*') !== false && substr_compare($this->tableName, '*', -1, 1)) {
            $this->addError('tableName', 'Asterisk is only allowed as the last character.');

            return;
        }
        $tables = $this->getTableNames();
        if (empty($tables)) {
            $this->addError('tableName', "Table '{$this->tableName}' does not exist.");
        } else {
            foreach ($tables as $table) {
                $class = $this->generateClassName($table);
                if ($this->isReservedKeyword($class)) {
                    $this->addError('tableName',
                        "Table '$table' will generate a class which is a reserved PHP keyword.");
                    break;
                }
            }
        }
    }

    /**
     * @param $tableName
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function generateTableName($tableName)
    {
        if (!$this->useTablePrefix) {
            return $tableName;
        }

        $db = $this->getDbConnection();
        if (preg_match("/^{$db->tablePrefix}(.*?)$/", $tableName, $matches)) {
            $tableName = '{{%' . $matches[1] . '}}';
        } elseif (preg_match("/^(.*?){$db->tablePrefix}$/", $tableName, $matches)) {
            $tableName = '{{' . $matches[1] . '%}}';
        }
        return $tableName;
    }

    /**
     * Generates filter rules for the specified table.
     *
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated filter rules
     */
    public function generateFilterRules($table)
    {
        $rules = [];

        if (!$this->generateFilterAttributes) {
            return $rules;
        }

        foreach ($table->columns as $column) {
            if (in_array($column->name, $this->excludeNames)) {
                continue;
            }
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $rules[$column->name] = "['=', '{$column->name}', \$this->{$column->name}]";
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                case Schema::TYPE_TIME:
                case Schema::TYPE_TEXT:
                    continue;
                    break;
                default:
                    $rules[$column->name] = "['like', '{$column->name}', \$this->{$column->name}]";
            }
        }

        return $rules;
    }

    /**
     * @param $className
     *
     * @return string route
     */
    public function getRoute($className)
    {
        $name = StringHelper::basename($className);
        return Inflector::camel2id($name);
    }

    /**
     * Checks if table has key fields
     *
     * @param \yii\db\TableSchema $table the table schema
     *
     * @return ColumnSchema[] key columns
     */
    public function getKeys($table)
    {
        $keyColumns = [];

        foreach ($table->columns as $column) {
            if (strpos($column->name,
                    '_key') && $column->type == Schema::TYPE_SMALLINT && $column->name != 'status_key') {
                $key                     = rtrim(substr($column->name, 0, -3), '_');
                $getterName              = Inflector::id2camel($key, '_');
                $keyColumns[$getterName] = $column;
            }
        }

        return $keyColumns;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getSchemaNames()
    {
        $db     = $this->getDbConnection();
        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }

    /**
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     */
    protected function generateRelations()
    {
        if ($this->generateRelations === self::RELATIONS_NONE) {
            return [];
        }

        $db = $this->getDbConnection();

        $relations = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                        continue;
                    }
                    unset($refs[0]);
                    $fks          = array_keys($refs);
                    $refClassName = $this->generateClassName($refTable);

                    // Add relation for this table
                    $link                                       = $this->generateRelationLink(array_flip($refs));
                    $relationName                               = $this->generateRelationName($relations, $table,
                        $fks[0], false);
                    $relations[$table->fullName][$relationName] = [
                        "return \$this->hasOne($refClassName::class, $link);",
                        $refClassName,
                        false,
                    ];

                    // Add relation for the referenced table
                    $hasMany                                             = $this->isHasManyRelation($table, $fks);
                    $link                                                = $this->generateRelationLink($refs);
                    $relationName                                        = $this->generateRelationName($relations,
                        $refTableSchema, $className, $hasMany);
                    $relations[$refTableSchema->fullName][$relationName] = [
                        "return \$this->" . ($hasMany ? 'hasMany' : 'hasOne') . "($className::class, $link);",
                        $className,
                        $hasMany,
                    ];
                }

                if (($junctionFks = $this->checkJunctionTable($table)) === false) {
                    continue;
                }

                $relations = $this->generateManyManyRelations($table, $junctionFks, $relations);
            }
        }

        if ($this->generateRelations === self::RELATIONS_ALL_INVERSE) {
            return $this->addInverseRelations($relations);
        }

        return $relations;
    }

    /**
     * @param $relations
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    protected function addInverseRelations($relations)
    {
        $relationNames = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($this->getDbConnection()->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $this->getDbConnection()->getTableSchema($refTable);
                    unset($refs[0]);
                    $fks = array_keys($refs);

                    $leftRelationName                                             = $this->generateRelationName($relationNames,
                        $table, $fks[0], false);
                    $relationNames[$table->fullName][$leftRelationName]           = true;
                    $hasMany                                                      = $this->isHasManyRelation($table,
                        $fks);
                    $rightRelationName                                            = $this->generateRelationName($relationNames,
                        $refTableSchema, $className, $hasMany);
                    $relationNames[$refTableSchema->fullName][$rightRelationName] = true;

                    $relations[$table->fullName][$leftRelationName][0]           = rtrim($relations[$table->fullName][$leftRelationName][0],
                            ';') . "->inverseOf('" . lcfirst($rightRelationName) . "');";
                    $relations[$refTableSchema->fullName][$rightRelationName][0] = rtrim($relations[$refTableSchema->fullName][$rightRelationName][0],
                            ';') . "->inverseOf('" . lcfirst($leftRelationName) . "');";
                }
            }
        }
        return $relations;
    }

    /**
     * @param $table
     * @param $fks
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    protected function isHasManyRelation($table, $fks)
    {
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge($uniqueKeys, $this->getDbConnection()->getSchema()->findUniqueIndexes($table));
        } catch (NotSupportedException $e) {
            // ignore
        }
        foreach ($uniqueKeys as $uniqueKey) {
            if (count(array_diff(array_merge($uniqueKey, $fks), array_intersect($uniqueKey, $fks))) === 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generates the link parameter to be used in generating the relation declaration.
     *
     * @param array $refs reference constraint
     * @return string the generated link parameter.
     */
    protected function generateRelationLink($refs)
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = "'$a' => '$b'";
        }

        return '[' . implode(', ', $pairs) . ']';
    }

    /**
     * @param $table
     * @return array|bool
     * @throws \yii\base\InvalidConfigException
     */
    protected function checkJunctionTable($table)
    {
        if (count($table->foreignKeys) < 2) {
            return false;
        }
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge($uniqueKeys, $this->getDbConnection()->getSchema()->findUniqueIndexes($table));
        } catch (NotSupportedException $e) {
            // ignore
        }
        $result = [];
        // find all foreign key pairs that have all columns in an unique constraint
        $foreignKeys = array_values($table->foreignKeys);
        for ($i = 0; $i < count($foreignKeys); $i++) {
            $firstColumns = $foreignKeys[$i];
            unset($firstColumns[0]);

            for ($j = $i + 1; $j < count($foreignKeys); $j++) {
                $secondColumns = $foreignKeys[$j];
                unset($secondColumns[0]);

                $fks = array_merge(array_keys($firstColumns), array_keys($secondColumns));
                foreach ($uniqueKeys as $uniqueKey) {
                    if (count(array_diff(array_merge($uniqueKey, $fks), array_intersect($uniqueKey, $fks))) === 0) {
                        // save the foreign key pair
                        $result[] = [$foreignKeys[$i], $foreignKeys[$j]];
                        break;
                    }
                }
            }
        }
        return empty($result) ? false : $result;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     *
     * @param array               $relations the relations being generated currently.
     * @param \yii\db\TableSchema $table the table schema
     * @param string              $key a base name that the relation name may be generated from
     * @param boolean             $multiple whether this is a has-many relation
     * @return string the relation name
     */
    protected function generateRelationName($relations, $table, $key, $multiple)
    {
        static $baseModel;
        if ($baseModel === null) {
            $baseModel = new ActiveRecord();
        }
        if (!empty($key) && strcasecmp($key, 'id')) {
            if (substr_compare($key, 'id', -2, 2, true) === 0) {
                $key = rtrim(substr($key, 0, -2), '_');
            } elseif (substr_compare($key, 'id', 0, 2, true) === 0) {
                $key = ltrim(substr($key, 2, strlen($key)), '_');
            }
        }
        if ($multiple) {
            $key = Inflector::pluralize($key);
        }
        $name = $rawName = Inflector::id2camel($key, '_');
        $i    = 0;
        while ($baseModel->hasProperty(lcfirst($name))) {
            $name = $rawName . ($i++);
        }
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$table->fullName][$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getTableNames()
    {
        if ($this->tableNames !== null) {
            return $this->tableNames;
        }
        $db = $this->getDbConnection();
        if ($db === null) {
            return [];
        }
        $tableNames = [];
        if (strpos($this->tableName, '*') !== false) {
            if (($pos = strrpos($this->tableName, '.')) !== false) {
                $schema  = substr($this->tableName, 0, $pos);
                $pattern = '/^' . str_replace('*', '\w+', substr($this->tableName, $pos + 1)) . '$/';
            } else {
                $schema  = '';
                $pattern = '/^' . str_replace('*', '\w+', $this->tableName) . '$/';
            }

            foreach ($db->schema->getTableNames($schema) as $table) {
                if (preg_match($pattern, $table)) {
                    $tableNames[] = $schema === '' ? $table : ($schema . '.' . $table);
                }
            }
        } elseif (($table = $db->getTableSchema($this->tableName, true)) !== null) {
            $tableNames[]                       = $this->tableName;
            $this->classNames[$this->tableName] = $this->modelClass;
        }

        return $this->tableNames = $tableNames;
    }

    /**
     * @param      $tableName
     * @param null $useSchemaName
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    protected function generateClassName($tableName, $useSchemaName = null)
    {
        if (isset($this->classNames[$tableName])) {
            return $this->classNames[$tableName];
        }

        $schemaName    = '';
        $fullTableName = $tableName;
        if (($pos = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

        $db         = $this->getDbConnection();
        $patterns   = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        if (strpos($this->tableName, '*') !== false) {
            $pattern = $this->tableName;
            if (($pos = strrpos($pattern, '.')) !== false) {
                $pattern = substr($pattern, $pos + 1);
            }
            $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
        }
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return $this->classNames[$fullTableName] = Inflector::id2camel($schemaName . $className, '_');
    }

    /**
     * Generates a query class name from the specified model class name.
     *
     * @param string $modelClassName model class name
     * @return string generated class name
     */
    protected function generateQueryClassName($modelClassName)
    {
        $queryClassName = $this->queryClass;
        if (empty($queryClassName) || strpos($this->tableName, '*') !== false) {
            $queryClassName = $modelClassName . 'Query';
        }
        return $queryClassName;
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    protected function getDbConnection()
    {
        return Yii::$app->get($this->db, false);
    }

    /**
     * Checks if any of the specified columns is auto incremental.
     *
     * @param \yii\db\TableSchema $table the table schema
     * @param array               $columns columns to check for autoIncrement property
     * @return boolean whether any of the specified columns is auto incremental.
     */
    protected function isColumnAutoIncremental($table, $columns)
    {
        foreach ($columns as $column) {
            if (isset($table->columns[$column]) && $table->columns[$column]->autoIncrement) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $table
     * @param $fks
     * @param $relations
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    private function generateManyManyRelations($table, $fks, $relations)
    {
        $db = $this->getDbConnection();

        foreach ($fks as $pair) {
            list($firstKey, $secondKey) = $pair;
            $table0 = $firstKey[0];
            $table1 = $secondKey[0];
            unset($firstKey[0], $secondKey[0]);
            $className0   = $this->generateClassName($table0);
            $className1   = $this->generateClassName($table1);
            $table0Schema = $db->getTableSchema($table0);
            $table1Schema = $db->getTableSchema($table1);

            $link                                              = $this->generateRelationLink(array_flip($secondKey));
            $viaLink                                           = $this->generateRelationLink($firstKey);
            $relationName                                      = $this->generateRelationName($relations, $table0Schema,
                key($secondKey), true);
            $relations[$table0Schema->fullName][$relationName] = [
                "return \$this->hasMany($className1::class, $link)->viaTable('" . $this->generateTableName($table->name) . "', $viaLink);",
                $className1,
                true,
            ];

            $link                                              = $this->generateRelationLink(array_flip($firstKey));
            $viaLink                                           = $this->generateRelationLink($secondKey);
            $relationName                                      = $this->generateRelationName($relations, $table1Schema,
                key($firstKey), true);
            $relations[$table1Schema->fullName][$relationName] = [
                "return \$this->hasMany($className0::class, $link)->viaTable('" . $this->generateTableName($table->name) . "', $viaLink);",
                $className0,
                true,
            ];
        }

        return $relations;
    }
}

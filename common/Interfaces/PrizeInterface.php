<?php

namespace common\interfaces;

use common\models\PrizesHistory;

/**
 * Interface PrizeCollectionItemInterface
 * @package common\interfaces
 */
interface PrizeInterface
{
    /**
     * @return mixed
     */
    public function randomPrize();

    /**
     * @return int|null
     */
    public static function getTotal();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     */
    public function setName($name);


    /**
     * @return string
     */
    public static function getPrizeType();

    /**
     * @return mixed
     */
    public function sendPrize();

    /**
     * @return boolean
     */
    public static function isAvailable();

    /**
     * @return integer
     */
    public function getAmount();

    /**
     * @return integer|null
     */
    public function getPrizeId();

    /**
     * @param $prizeHistory PrizesHistory
     * @return bool
     */
    public function savePrize($prizeHistory);

    /**
     * @param $prizeHistory PrizesHistory
     * @return bool
     */
    public function discardPrize($prizeHistory);
}
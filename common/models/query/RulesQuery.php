<?php

namespace common\models\query;

use common\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Video]].
 *
 * @see \common\models\Video
 */
class RulesQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return \common\models\Rules[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param $type
     *
     * @return $this
     */
    public function type($type)
    {
        $this->andWhere(['type_key' => $type]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Rules|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace common\models\prizes;


use common\helpers\ArrayHelper;
use common\helpers\RandomHelper;
use common\models\LotteryConfig;
use common\models\prizes\base\BasePrizeModel;
use common\models\PrizesHistory;

/**
 * Class MoneyPrize
 * @package common\models\prizes
 *
 * @property integer $currentRandom
 * @property array $configs
 */
class MoneyPrize extends BasePrizeModel
{
    const PRIZE_NAME = "Денежный приз";
    const PRIZE_KEY = "moneyPrize";

    const MIN_AMOUNT_KEY = 'min_money_prize';
    const MAX_AMOUNT_KEY = 'max_money_prize';
    const TOTAL_MONEY_KEY = 'total_money';


    public $currentRandom = 0;
    public $configs;

    /**
     * BonusPointsPrize constructor.
     */
    public function __construct()
    {
        $this->setName(self::PRIZE_NAME);
        $this->configs = self::getConfigs();
    }

    /**
     * @return mixed
     */
    public function randomPrize()
    {
        $min = ArrayHelper::getValue($this->configs, self::MIN_AMOUNT_KEY, 0);
        $max = ArrayHelper::getValue($this->configs, self::MAX_AMOUNT_KEY, 200);

        return $this->currentRandom = RandomHelper::getRandom($min, $max);
    }

    /**
     * @return int|null
     */
    public static function getTotal()
    {
        $configs = self::getConfigs();
        $total = ArrayHelper::getValue($configs,self::TOTAL_MONEY_KEY, 0);

        return $total;
    }

    /**
     * @return string
     */
    public static function getPrizeType()
    {
        return self::PRIZE_KEY;
    }

    /**
     * @return mixed
     */
    public function sendPrize()
    {
        /** @var LotteryConfig $total */
        $total = LotteryConfig::find()->where(['key' => self::TOTAL_MONEY_KEY])->one();
        if(empty($total)) {
            return false;
        }

        $total->value = (string)($total->value - $this->getAmount());
        return $total->save();
    }

    /**
     * @return boolean
     */
    public static function isAvailable()
    {
        $configs = self::getConfigs();
        $total = ArrayHelper::getValue($configs,self::TOTAL_MONEY_KEY, 0);
        $min = ArrayHelper::getValue($configs,self::MIN_AMOUNT_KEY, 0);

        return $total > $min;
    }

    /**
     * @return integer
     */
    public function getAmount()
    {
        return $this->currentRandom;
    }

    /**
     * @return integer|null
     */
    public function getPrizeId()
    {
        return null;
    }


    /**
     * @param $id
     * @return bool
     */
    public static function returnMoney($id)
    {
        $model = PrizesHistory::findByPk($id);

        /** @var LotteryConfig $total */
        $total = LotteryConfig::find()->where(['key' => self::TOTAL_MONEY_KEY])->one();
        if(empty($total)) {
            $total = new LotteryConfig();
            $total->key = self::TOTAL_MONEY_KEY;
            $total->value = 0;
            $total->save();
        }

        $total->value = (string)($total->value + $model->amount);
        return $total->save();
    }


    /**
     * @param $prizeHistory PrizesHistory
     * @return boolean
     */
    public function savePrize($prizeHistory)
    {
        $prizeHistory->status_key = PrizesHistory::SEND_TO_BANK;
        return $prizeHistory->save();
    }

    /**
     * @param PrizesHistory $prizeHistory
     * @return bool
     */
    public function discardPrize($prizeHistory)
    {
        $prizeHistory->status_key = PrizesHistory::STATUS_DISABLED;
        $prizeHistory->save();

        return MoneyPrize::returnMoney($prizeHistory->id);
    }


    /**
     * @param $amount
     * @param $userId
     * @return bool
     */
    public function sendToBank($amount, $userId)
    {
        $url = 'http://bank.cz/api/add';
        $params = [
            'user_id' => $userId,
            'amount' => $amount,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $params);
            $head = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return true; //Fake request
    }
}
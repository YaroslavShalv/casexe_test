<?php

namespace common\models\prizes;


use common\helpers\HttpError;
use common\helpers\RandomHelper;
use common\models\Items;
use common\models\prizes\base\BasePrizeModel;
use common\models\PrizesHistory;

/**
 * Class ItemPrize
 * @package common\models\prizes
 *
 * @property Items $currentItem
 */
class ItemPrize extends BasePrizeModel
{
    const PRIZE_NAME = "Предмет";
    const PRIZE_KEY = "itemPrize";

    public $currentItem;

    /**
     * BonusPointsPrize constructor.
     */
    public function __construct()
    {
        $this->setName(self::PRIZE_NAME);
    }

    /**
     * @return mixed
     */
    public function randomPrize()
    {
        $randomKey = RandomHelper::getRandom(0, (self::getTotal() - 1));

        $this->currentItem = Items::find()
            ->where(['status_key' => Items::STATUS_ENABLED])
            ->limit(1)
            ->offset($randomKey)->one();

        return $this->currentItem;
    }

    /**
     * @return int|null
     */
    public static function getTotal()
    {
        return Items::find()->where(['status_key' => Items::STATUS_ENABLED])->count();
    }

    /**
     * @return string
     */
    public static function getPrizeType()
    {
        return self::PRIZE_KEY;
    }


    /**
     * @return string
     */
    public function getItemName()
    {
        return !empty($this->currentItem) ? $this->currentItem->prize : "";
    }

    /**
     * @return mixed
     */
    public function sendPrize()
    {
        $this->currentItem->status_key = Items::STATUS_RESERVED;
        $this->currentItem->save();
    }

    /**
     * @param $prizeHistory PrizesHistory
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function savePrize($prizeHistory)
    {
        $prizeHistory->status_key = PrizesHistory::STATUS_ENABLED;
        $prizeHistory->save();

        $item = Items::findByPk($prizeHistory->prize_id);
        if(empty($item)) {
            HttpError::the404('Приз не найден');
        }

        $item->status_key = Items::STATUS_MODERATION;
        return $item->save();
    }


    /**
     * @param PrizesHistory $prizeHistory
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function discardPrize($prizeHistory)
    {
        $prizeHistory->status_key = PrizesHistory::STATUS_DISABLED;
        return $prizeHistory->save();

        $item = Items::findByPk($prizeHistory->prize_id);
        if(empty($item)) {
            HttpError::the404('Приз не найден');
        }

        $item->status_key = Items::STATUS_ENABLED;
        return $item->save();
    }


    /**
     * @return boolean
     */
    public static function isAvailable()
    {
        return self::getTotal() > 0;
    }

    /**
     * @return integer
     */
    public function getAmount()
    {
        return 1;
    }

    /**
     * @return integer|null
     */
    public function getPrizeId()
    {
        return !empty($this->currentItem) ? $this->currentItem->id : 0;
    }
}
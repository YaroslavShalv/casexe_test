<?php
namespace common\models\prizes\base;

use common\helpers\ArrayHelper;
use common\interfaces\PrizeInterface;
use common\models\LotteryConfig;
use common\models\PrizesHistory;

/**
 * Class BasePrizesModel
 * @package common\models\prizes\base'
 *
 * @property string $name
 * @property int|null $total
 *
 */
abstract class BasePrizeModel implements PrizeInterface
{
    public $name;


    /**
     * @return mixed
     */
    abstract public function randomPrize();

    /**
     * @return int|null
     */
    abstract static public function getTotal();

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    abstract public static function getPrizeType();

    /**
     * @return mixed
     */
    abstract public function sendPrize();

    /**
     * @return boolean
     */
    abstract static public function isAvailable();

    /**
     * @return integer
     */
    abstract public function getAmount();

    /**
     * @return integer|null
     */
    abstract public function getPrizeId();

    /**
     * @param $prizeHistory PrizesHistory
     * @return bool
     */
    abstract public function savePrize($prizeHistory);

    /**
     * @param $prizeHistory PrizesHistory
     * @return bool
     */
    abstract public function discardPrize($prizeHistory);


    /**
     * @return array
     */
    public static function getConfigs()
    {
        $configs = LotteryConfig::find()->all();
        return ArrayHelper::map($configs, 'key', 'value');
    }


    /**
     * @return string
     */
    public function getItemName()
    {
        return "";
    }
}
<?php
namespace common\models\prizes;


use common\components\LoyalityPoints;
use common\helpers\ArrayHelper;
use common\helpers\RandomHelper;
use common\models\prizes\base\BasePrizeModel;
use common\models\PrizesHistory;

/**
 * Class BonusPointsPrize
 * @package common\models\prizes
 * @property string $name
 * @property integer $currentRandom
 * @property array $configs
 */
class BonusPointsPrize extends BasePrizeModel
{
    const PRIZE_NAME = "Бонусные баллы";
    const PRIZE_KEY = "bonusPoints";

    const MIN_AMOUNT_KEY = 'min_point_prize';
    const MAX_AMOUNT_KEY = 'max_points_prize';

    public $currentRandom = 0;
    public $configs;

    /**
     * BonusPointsPrize constructor.
     */
    public function __construct()
    {
        $this->setName(self::PRIZE_NAME);
        $this->configs = self::getConfigs();
    }


    /**
     * @return integer
     */
    public function randomPrize()
    {
        $min = ArrayHelper::getValue($this->configs, self::MIN_AMOUNT_KEY, 0);
        $max = ArrayHelper::getValue($this->configs, self::MAX_AMOUNT_KEY, 20000);

        return $this->currentRandom = RandomHelper::getRandom($min, $max);
    }

    /**
     * @return int|null
     */
    public static function getTotal()
    {
        return null;
    }

    /**
     * @return string
     */
    public static function getPrizeType()
    {
        return self::PRIZE_KEY;
    }

    /**
     * @return mixed
     */
    public function sendPrize()
    {
        return null;
    }



    /**
     * @param $prizeHistory PrizesHistory
     * @return bool
     */
    public function savePrize($prizeHistory)
    {
        $prizeHistory->status_key = PrizesHistory::STATUS_ENABLED;
        $prizeHistory->save();

        $points = new LoyalityPoints();
        return $points->addBonus($prizeHistory->amount, $prizeHistory->user_id);
    }

    /**
     * @return boolean
     */
    public static function isAvailable()
    {
        return true;
    }

    /**
     * @return integer
     */
    public function getAmount()
    {
        return $this->currentRandom;
    }

    /**
     * @return integer|null
     */
    public function getPrizeId()
    {
        return null;
    }

    /**
     * @param PrizesHistory $prizeHistory
     * @return bool
     */
    public function discardPrize($prizeHistory)
    {
        $prizeHistory->status_key = PrizesHistory::STATUS_DISABLED;
        return $prizeHistory->save();
    }
}
<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use common\db\ActiveRecord;

/**
 * Модель таблицы "{{%loyality_points}}".
 *
 * @property integer $id 
 * @property integer $user_id ID пользователя
 * @property integer $points Очки лояльности
 * @property string $created_at 
 * @property string $updated_at 
 */
class LoyalityPoints extends ActiveRecord
{
    public $status_key;

    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '{{%loyality_points}}';
    }

    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'points'], 'integer'],
            [['user_id'], 'unique'],
        ]);
    }

    /**
     * @return array Поведения
     */
    public function behaviors()
    {
        $items = parent::behaviors();
        
        return $items;
    }

}

<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use common\db\ActiveRecord;

/**
 * Модель таблицы "{{%converting_job}}".
 *
 * @property integer $id 
 * @property integer $user_id 
 * @property integer $amount 
 * @property integer $result 
 * @property integer $status_key 
 * @property string $created_at 
 * @property string $updated_at 
 */
class ConvertingJob extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_WORKED = 1;

    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '{{%converting_job}}';
    }

    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'amount', 'result'], 'integer'],
        ]);
    }

    /**
     * @return array Поведения
     */
    public function behaviors()
    {
        $items = parent::behaviors();
        
        return $items;
    }

}

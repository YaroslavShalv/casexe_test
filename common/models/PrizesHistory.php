<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use common\db\ActiveRecord;

/**
 * Модель таблицы "{{%prizes_history}}".
 *
 * @property integer $id 
 * @property integer $user_id ID пользователя
 * @property string $prize_key Тип приза
 * @property integer $amount
 * @property integer $prize_id
 * @property integer $status_key
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class PrizesHistory extends ActiveRecord
{
    const CONVERSION = 3;
    const SEND_TO_BANK = 4;

    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '{{%prizes_history}}';
    }

    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'amount', 'prize_id'], 'integer'],
            [['prize_key'], 'string', 'max' => 31],
            [['user_id'], 'required'],
        ]);
    }

    /**
     * @return array Поведения
     */
    public function behaviors()
    {
        $items = parent::behaviors();
        
        return $items;
    }

    /**
     * Get status items list
     * if needed, can be redefined for adding new statuses
     *
     * @return array
     */
    public static function statusItemsList()
    {
        return [
            self::STATUS_ENABLED    => 'Выдан',
            self::STATUS_DISABLED   => 'Отказ от приза',
            self::STATUS_MODERATION => 'Обрабатывается',
            self::CONVERSION        => 'Конверсия денег в баллы',
            self::SEND_TO_BANK      => 'Отправка денег банку',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}

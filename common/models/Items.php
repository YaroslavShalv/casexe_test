<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use common\db\ActiveRecord;

/**
 * Модель таблицы "{{%items}}".
 *
 * @property integer $id 
 * @property string $prize Назване приза
 * @property integer $status_key 
 * @property string $created_at 
 * @property string $updated_at 
 */
class Items extends ActiveRecord
{
    const STATUS_SEND = 3;
    const STATUS_RESERVED = 4;

    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '{{%items}}';
    }

    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['prize'], 'string', 'max' => 255],
        ]);
    }

    /**
     * Get status items list
     * if needed, can be redefined for adding new statuses
     *
     * @return array
     */
    public static function statusItemsList()
    {
        return [
            self::STATUS_ENABLED    => 'Доступен',
            self::STATUS_DISABLED   => 'На модерации',
            self::STATUS_MODERATION => 'Отсутствует',
            self::STATUS_SEND       => 'Отправлен',
            self::STATUS_RESERVED   => 'Пользователь не решил что делать с призом',
        ];
    }


    /**
     * @return array Поведения
     */
    public function behaviors()
    {
        $items = parent::behaviors();
        
        return $items;
    }

}

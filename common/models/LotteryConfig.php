<?php

namespace common\models;

use yii\helpers\ArrayHelper;
use common\db\ActiveRecord;

/**
 * Модель таблицы "{{%lottery_config}}".
 *
 * @property integer $id 
 * @property string $key Ключ
 * @property string $value Значение
 * @property string $description Описание
 * @property integer $status_key
 * @property string $created_at 
 * @property string $updated_at 
 */
class LotteryConfig extends ActiveRecord
{
    /**
     * @return string Название таблицы
     */
    public static function tableName()
    {
        return '{{%lottery_config}}';
    }

    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['key', 'value', 'description'], 'string', 'max' => 255],
        ]);
    }

    /**
     * @return array Поведения
     */
    public function behaviors()
    {
        $items = parent::behaviors();
        
        return $items;
    }

}

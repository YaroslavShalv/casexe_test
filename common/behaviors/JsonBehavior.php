<?php
/**
 * Created by PhpStorm.
 * User: OS
 * Date: 07.03.2017
 * Time: 16:56
 */

namespace common\behaviors;

use common\db\ActiveRecord;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * Class JsonBehavior
 *
 * @package common\behaviors
 */
class JsonBehavior extends Behavior
{
    /** @var ActiveRecord */
    public $owner;

    /**
     * Attributes to convert
     *
     * @var array
     */
    public $jsonAttributes = [];

    /** @inheritdoc */
    public function init()
    {
        parent::init();

        if (!is_array($this->jsonAttributes)) {
            throw new InvalidConfigException('`$jsonAttributes` must be array!');
        }
    }

    /** @inheritdoc */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND     => 'jsonAttributesDecode',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'jsonAttributesEncode',
            ActiveRecord::EVENT_BEFORE_INSERT  => 'jsonAttributesEncode',
            ActiveRecord::EVENT_BEFORE_UPDATE  => 'jsonAttributesEncode',
        ];
    }

    /** Decode json attributes */
    public function jsonAttributesDecode()
    {
        foreach ($this->jsonAttributes as $jsonAttribute) {
            if (is_array($this->owner->{$jsonAttribute})) {
                continue;
            }

            $this->owner->{$jsonAttribute} = is_string($this->owner->{$jsonAttribute}) ? (is_array($json = json_decode($this->owner->{$jsonAttribute},
                true)) ? $json : []) : [];
        }
    }

    /** Encode json attributes */
    public function jsonAttributesEncode()
    {
        if ($this->owner instanceof Model && count($this->owner->getErrors()) > 0) {
            return;
        }

        foreach ($this->jsonAttributes as $jsonAttribute) {
            if (is_string($this->owner->{$jsonAttribute})) {
                continue;
            }

            $this->owner->{$jsonAttribute} = is_array($this->owner->{$jsonAttribute}) ? (is_string($json = json_encode($this->owner->{$jsonAttribute})) ? $json : '') : '';
        }
    }
}
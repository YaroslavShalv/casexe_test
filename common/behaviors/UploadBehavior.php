<?php

namespace common\behaviors;

use Closure;
use common\db\ActiveRecord;
use common\helpers\FileHelper;
use ImageOptimizer\OptimizerFactory;
use Imagick;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class UploadBehavior
 *
 * @package common\behaviors
 */
class UploadBehavior extends Behavior
{
    /** @event действие после загрузки файла */
    const EVENT_AFTER_UPLOAD = 'afterUpload';

    /** @var array названия атрибутов для обработки */
    public $attributes;

    /** @var string */
    public $prefix = 'file_';

    /** @var array сценарии для работы */
    public $scenarios = [];

    /** @var string базовый путь к файлам вида /uploads/advice */
    public $path;

    /** @var bool получать инстанс файла по имени */
    public $instanceByName = false;

    /**
     * @var boolean|callable генерировать уникальное имя для файла
     * true или анонимная функция
     * @see self::generateFileName()
     */
    public $generateNewName = true;

    /** @var boolean $unlinkOnDelete удалять файл с удалением записи */
    public $unlinkOnDelete = true;

    /** @var boolean $deleteTempFile Удалять временный файл после загрузки */
    public $deleteTempFile = true;

    /** @var array для хранения инстансов файлов */
    private $_instances;

    /**
     * Заменяет недопустимые символы в названии
     *
     * #my*  unsaf<e>&file:name?".png
     *
     * @param string $filename имя файла для очистки
     * @return boolean string очищенное имя файла
     */
    public static function sanitize($filename)
    {
        return str_replace([' ', '"', '\'', '&', '/', '\\', '?', '#'], '-', $filename);
    }

    /**
     * Инициализация
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes) || !is_array($this->attributes)) {
            throw new InvalidConfigException('The "attribute" property must be set and must be an array.');
        }
    }

    /** @return array Направление событий на соответствующие методы */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            BaseActiveRecord::EVENT_AFTER_INSERT    => 'afterSave',
            BaseActiveRecord::EVENT_AFTER_UPDATE    => 'afterSave',
            BaseActiveRecord::EVENT_BEFORE_DELETE   => 'beforeDelete',
        ];
    }

    /** Вызывается перед валидацией */
    public function beforeValidate()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;

        if (in_array($model->scenario, $this->scenarios)) {
            foreach ($this->attributes as $attribute) {
                $fileAttribute = $this->prefix . $attribute;
                $fileValue     = isset($model->{$fileAttribute}) ? $model->{$fileAttribute} : null;

                if ($fileValue instanceof UploadedFile) {
                    continue;
                } else {
                    if ($this->instanceByName === true && $file = UploadedFile::getInstanceByName($fileAttribute)) {
                        $this->owner->$fileAttribute = $file;
                    } else {
                        if ($this->instanceByName !== true && $file = UploadedFile::getInstance($model,
                                $fileAttribute)) {
                            $this->owner->$fileAttribute = $file;
                        } else {
                            if (!empty($fileValue) && is_string($fileValue)) {
                                $this->owner->$fileAttribute = $fileValue;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave()
    {
        /** @var ActiveRecord $model */
        $model       = $this->owner;
        $modelFolder = $model::getClassNameShort(true, true);

        $updateModel = false;
        foreach ($this->attributes as $attribute) {
            $fileAttribute = $this->prefix . $attribute;
            if (empty($this->owner->$fileAttribute)) {
                continue;
            }

            $updateModel = true;

            $pk = implode('_', $model->getPrimaryKey(true));

            $fileName = $pk . '_' . $this->getFileName($this->owner->$fileAttribute);

            $url  = FileHelper::fileUrl($modelFolder, $attribute, $pk, "{$this->path}/{$fileName}");
            $path = FileHelper::filePath($modelFolder, $attribute, $pk, 'original', "{$this->path}/{$fileName}");

            if (is_string($path) && FileHelper::createDirectory(dirname($path), 0777)) {
                if ($this->owner->$fileAttribute instanceof UploadedFile) {
                    if ($this->save($this->owner->$fileAttribute, $path)) {
                        if ($this->owner->$fileAttribute->extension == 'jpg') {
                            $this->fixOrientation($path);
                        }
                    } else {
                        continue;
                    }
                } else {
                    if (preg_match('/^(http:\/\/|https:\/\/|\/\/).*/', $this->owner->$fileAttribute)) {
                        $this->deleteTempFile = false;
                        $fromPath             = $this->owner->$fileAttribute;
                    } else {
                        if (preg_match('/^data:([\w\/]+);base64/i', $this->owner->$fileAttribute, $matches)) {
                            $this->deleteTempFile = false;
                            list($type, $data) = explode(';', $this->owner->$fileAttribute);
                            list(, $data) = explode(',', $this->owner->$fileAttribute);
                            $data = base64_decode($data);

                            $tempPath = '/tmp/' . $fileName;
                            file_put_contents($tempPath, $data);
                            $fromPath = Yii::getAlias($tempPath);
                        } else {
                            $fromPath = Yii::getAlias('@frontend/web' . $this->owner->$fileAttribute);
                        }
                    }
                    try {
                        if ($this->deleteTempFile) {
                            if (!rename($fromPath, $path)) {
                                continue;
                            }
                        } else {
                            if (!copy($fromPath, $path)) {
                                continue;
                            }
                        }
                    } catch (\Exception $ex) {
                        unset($this->owner->$fileAttribute);
                        break;
                    }
                }
            } else {
                throw new InvalidArgumentException("Directory specified in 'path' attribute doesn't exist or cannot be created.");
            }

            if (file_exists($path)) {
                //                $this->optimize($path);
                $this->afterUpload();
                $model->setAttribute($attribute, $url);
            } else {
                $model->setAttribute($attribute, '');
            }

            unset($this->owner->$fileAttribute);
        }
        if ($updateModel) {
            $model->update(false, $this->attributes);
        }
    }

    /** Вызывается перед удалением записи */
    public function beforeDelete()
    {
        foreach ($this->attributes as $attribute) {
            if ($this->unlinkOnDelete && $this->owner->{$attribute}) {
                $this->delete($attribute);
            }
        }
    }

    /**
     * Returns a value indicating whether a property can be read.
     *
     * @param string  $name the property name
     * @param boolean $checkVars whether to treat member variables as properties
     *
     * @return boolean whether the property can be read
     * @see canSetProperty()
     */
    public function canGetProperty($name, $checkVars = true)
    {
        if (strpos($name, $this->prefix) !== false) {
            $name = str_replace($this->prefix, '', $name);
        }

        return in_array($name, $this->attributes) ? true : parent::canGetProperty($name, $checkVars);
    }

    /**
     * Returns a value indicating whether a property can be set.
     *
     * @param string  $name the property name
     * @param boolean $checkVars whether to treat member variables as properties
     * @param boolean $checkBehaviors whether to treat behaviors' properties as properties of this component
     *
     * @return boolean whether the property can be written
     * @see canGetProperty()
     */
    public function canSetProperty($name, $checkVars = true, $checkBehaviors = true)
    {
        if (strpos($name, $this->prefix) !== false) {
            $name = str_replace($this->prefix, '', $name);
        }

        return in_array($name, $this->attributes) ? true : parent::canSetProperty($name, $checkVars);
    }

    /**
     * @param string $name
     * @return mixed|null
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if (strpos($name, $this->prefix) !== false) {
            $name = str_replace($this->prefix, '', $name);

            if (isset($this->_instances[$name])) {
                return $this->_instances[$name];
            }

            return null;
        }

        return parent::__get($name);
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @throws \yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {
        if (strpos($name, $this->prefix) !== false) {
            $name = str_replace($this->prefix, '', $name);

            $this->_instances[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * Сохраняет загруженный файл
     *
     * @param UploadedFile $file инстанс загруженного файла
     * @param string       $path путь для сохранения
     * @return boolean статус сохранения файла на сервере
     */
    protected function save($file, $path)
    {
        return $file->saveAs($path, $this->deleteTempFile);
    }

    /**
     * Удаляет файл
     *
     * @param string $attribute
     */
    protected function delete($attribute)
    {
        $path = FileHelper::normalizePath(Yii::getAlias("@frontend/web/" . $this->owner->{$attribute}));
        if (file_exists($path)) {
            unlink($path);
        }
    }

    /**
     * @param UploadedFile|string $file
     * @return string
     */
    protected function getFileName($file)
    {
        if ($this->generateNewName) {
            return $this->generateNewName instanceof Closure ? call_user_func($this->generateNewName,
                $file) : $this->generateFileName($file);
        } else {
            return $this->sanitize($file->name);
        }
    }

    /**
     * Генерирует случайное название для файла
     *
     * @param UploadedFile|string $file
     * @return string
     */
    protected function generateFileName($file)
    {
        if ($file instanceof UploadedFile) {
            $ext = $file->extension;
        } else {
            if (preg_match('/^data:([\w\/]+);base64/i', $file, $matches)) {
                $dataType  = ArrayHelper::getValue($matches, '1');
                $extension = FileHelper::getExtensionsByMimeType($dataType);
                $ext       = array_pop($extension);
            } else {
                $ext = ArrayHelper::getValue(pathinfo($file), 'extension', 'jpg');
                $ext = ArrayHelper::getValue(explode('?', $ext), 0);
            }
        }

        return uniqid() . '.' . $ext;
    }

    /** Вызывается после загрузки файла */
    protected function afterUpload()
    {
        $this->owner->trigger(self::EVENT_AFTER_UPLOAD);
    }

    /**
     * @param $path
     * @throws \ImagickException
     */
    protected function fixOrientation($path)
    {
        if (extension_loaded('imagick')) {
            $image       = new Imagick($path);
            $orientation = $image->getImageOrientation();

            switch ($orientation) {
                case imagick::ORIENTATION_BOTTOMRIGHT:
                    $image->rotateimage("#000", 180); // rotate 180 degrees
                    break;

                case imagick::ORIENTATION_RIGHTTOP:
                    $image->rotateimage("#000", 90); // rotate 90 degrees CW
                    break;

                case imagick::ORIENTATION_LEFTBOTTOM:
                    $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                    break;
            }

            // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
            $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
            $image->writeImage($path);
        } else {
            Yii::error('Extension imagick is not loaded!');
        }
    }

    /** @param $path */
    protected function optimize($path)
    {
        $factory = new OptimizerFactory();

        $info = pathinfo($path);

        $extension = ArrayHelper::getValue($info, 'extension');

        if (in_array($extension, ['jpg', 'png'])) {
            if ($extension == 'jpg') {
                $optimizer = $factory->get('jpegoptim');
            } else {
                if ($extension == 'png') {
                    $optimizer = $factory->get('optipng');
                } else {
                    $optimizer = $factory->get();
                }
            }

            $optimizer->optimize($path);
        }
    }
}
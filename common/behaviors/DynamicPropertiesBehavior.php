<?php

namespace common\behaviors;

use common\db\ActiveRecord;
use Exception;
use yii\base\Behavior;

/**
 * Class DynamicPropertiesBehavior
 *
 * @package common\behaviors
 */
class DynamicPropertiesBehavior extends Behavior
{
    /**
     * Атрибуты формы
     *
     * @var array
     */
    public $attributes = [];

    /**
     * Attributes value
     *
     * @var array
     */
    private $_values = [];

    /**
     * Events list
     *
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'validateProperties',
            ActiveRecord::EVENT_AFTER_INSERT   => 'saveProperties',
            ActiveRecord::EVENT_AFTER_UPDATE   => 'saveProperties',
        ];
    }

    /** Validate properties models */
    public function validateProperties()
    {
        foreach ($this->attributes as $name => $relationName) {
            /** @var \yii\db\ActiveRecord[] $models */
            $models = $this->getPropertyModels($name);

            $validate = true;
            foreach ($models as $model) {
                if ($model->hasErrors()) {
                    $validate = false;
                }
            }

            if (!$validate) {
                $this->owner->addError($name, 'Invalid one is models');
            }
        }
    }

    /** Save properties data */
    public function saveProperties()
    {
        $connection = \Yii::$app->db;

        foreach ($this->attributes as $name => $relationName) {

            if (!$this->hasPropertyModels($name)) {
                continue;
            }

            $relation = $this->getRelation($name);

            /** @var \yii\db\ActiveRecord $model */
            $model        = new $relation->modelClass();
            $tableName    = $model::tableName();
            $tableColumns = $model->attributes();
            $ownerPK      = $this->owner->getPrimaryKey();

            list($bindingColumn) = array_keys($relation->link);

            $transaction = $connection->beginTransaction();
            try {

                // Remove old data
                $connection->createCommand()->delete($tableName,
                    "{$bindingColumn} = {$this->owner->getPrimaryKey()}")->execute();

                // Write new data
                $models = $this->getPropertyModels($name);
                if (!empty($models)) {
                    $insertRows = [];
                    foreach ($models as $model) {

                        if ($model->hasAttribute('created_at')) {
                            $model->created_at = date('Y-m-d H:i:s');
                        }

                        if ($model->hasAttribute('updated_at')) {
                            $model->updated_at = date('Y-m-d H:i:s');
                        }

                        $model->$bindingColumn = $ownerPK;
                        array_push($insertRows, array_values($model->getAttributes()));
                    }

                    $connection->createCommand()->batchInsert($tableName, $tableColumns, $insertRows)->execute();
                }

                $transaction->commit();
            } catch (\yii\db\Exception $ex) {
                $transaction->rollBack();

                throw new \yii\db\Exception(500, 'Ошибка сохранения связей', 0, $ex);
            }
        }
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function getRelation($name)
    {
        $relationName = $this->getRelationName($name);
        return $this->owner->getRelation($relationName);
    }

    /**
     * Returns a value indicating whether a property can be read.
     *
     * @param string  $name the property name
     * @param boolean $checkVars whether to treat member variables as properties
     *
     * @return boolean whether the property can be read
     * @see canSetProperty()
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return array_key_exists($name, $this->attributes) ? true : parent::canGetProperty($name, $checkVars);
    }

    /**
     * Returns a value indicating whether a property can be set.
     *
     * @param string  $name the property name
     * @param boolean $checkVars whether to treat member variables as properties
     * @param boolean $checkBehaviors whether to treat behaviors' properties as properties of this component
     *
     * @return boolean whether the property can be written
     * @see canGetProperty()
     */
    public function canSetProperty($name, $checkVars = true, $checkBehaviors = true)
    {
        return array_key_exists($name, $this->attributes) ? true
            : parent::canSetProperty($name, $checkVars, $checkBehaviors);
    }

    /**
     * @param string $name
     * @return mixed|null
     * @throws \Exception
     */
    public function __get($name)
    {
        $relation = $this->getRelation($name);

        $value = $this->hasPropertyModels($name) ? $this->getPropertyModels($name) : $relation->all();

        return $value;
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if (is_array($value)) {
            $relation = $this->getRelation($name);

            foreach ($value as $data) {
                /** @var \yii\db\ActiveRecord $model */
                $model = new $relation->modelClass();
                $model->setAttributes($data);
                $model->validate();

                $this->_values[$name][] = $model;
            }
        }
    }

    /**
     * Get params
     *
     * @param $attributeName
     *
     * @return mixed
     * @throws Exception
     */
    private function getRelationParams($attributeName)
    {
        if (empty($this->attributes[$attributeName])) {
            throw new Exception("Item \"{$attributeName}\" must be configured");
        }

        return $this->attributes[$attributeName];
    }

    /**
     * Get source attribute name
     *
     * @param $attributeName
     * @return mixed|null
     * @throws \Exception
     */
    private function getRelationName($attributeName)
    {
        $params = $this->getRelationParams($attributeName);

        if (is_string($params)) {
            return $params;
        } elseif (is_array($params) && !empty($params[0])) {
            return $params[0];
        }
        return null;
    }

    /**
     * Get property new models
     *
     * @param $name
     *
     * @return null
     */
    private function getPropertyModels($name)
    {
        if ($this->hasPropertyModels($name)) {
            return $this->_values[$name];
        }
        return [];
    }

    /**
     * Check has set property models
     *
     * @param $name
     *
     * @return null
     */
    private function hasPropertyModels($name)
    {
        return isset($this->_values[$name]);
    }
}
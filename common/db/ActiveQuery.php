<?php

namespace common\db;

use yii\db\ActiveQuery as YiiActiveQuery;

/**
 * Class ActiveQuery
 *
 * @package common\db
 * @see \common\db\ActiveRecord
 */
class ActiveQuery extends YiiActiveQuery
{
    /**
     * Attribute with status_key name in model
     *
     * @var string
     */
    public static $statusKeyAttributeParam = 'statusKeyAttribute';

    /**
     * @param int $key
     *
     * @return $this
     */
    public function status($key = ActiveRecord::STATUS_ENABLED)
    {
        return $this->andWhere([$this->getStatusKeyFieldName() => $key]);
    }

    /**
     * Get status key attribute name
     *
     * @return string
     */
    protected function getStatusKeyFieldName()
    {
        $modelClass = $this->modelClass;
        $paramName  = static::$statusKeyAttributeParam;

        return property_exists($modelClass, $paramName) ? $modelClass::$$paramName : 'status_key';
    }
}
<?php

namespace common\db;

use common\db\base\BaseRelationTrait;
use common\db\base\interfaces\ModelRelationsInterface;
use common\db\base\interfaces\StatusesInterface;
use common\db\base\SearchModelTrait;
use common\db\base\SortableModelTrait;
use common\db\base\StatusesTrait;
use common\db\base\TimestampTrait;
use yii\db\ActiveRecord as YiiActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class ActiveRecord
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status_key
 *
 * @package common\db
 */
class ActiveRecord extends YiiActiveRecord implements StatusesInterface, ModelRelationsInterface
{
    use StatusesTrait;
    use TimestampTrait;
    use SearchModelTrait;
    use SortableModelTrait;
    use BaseRelationTrait;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Determines if status trait must be used
     *
     * @var bool
     */
    public $useStatusTrait = true;

    /**
     * Determines if timestamp trait must be used
     *
     * @var bool
     */
    public $useTimestampTrait = true;

    /**
     * Determines if sortable trait must be used
     *
     * @var bool
     */
    public $useSortableTrait = false;

    /**
     * Search model by id
     *
     * @param integer    $id
     * @param null|array $where
     *
     * @return static
     */
    public static function findByPk($id, $where = null)
    {
        $query = static::find()->andWhere(['id' => $id]);

        $query = is_array($where) ? $query->andWhere($where) : $query;

        return $query->limit(1)->one();
    }

    /**
     * Data for DropDownList, RadioList, CheckboxList
     * Returns data array or empty array
     *
     * @param string $indexField - Field for array keys
     * @param string $labelField - Field for array values
     * @param array  $where - Filtering data
     *
     * @return array Data array
     */
    public static function listAll($indexField = 'id', $labelField = 'name', $where = null)
    {
        $query = static::find()->select([$indexField, $labelField])->asArray();

        $query = is_array($where) ? $query->andWhere($where) : $query;

        return ArrayHelper::map($query->all(), $indexField, $labelField);
    }

    /**
     * Class short name
     *
     * @param bool|true $toLower
     * @param bool|true $pluralize
     *
     * @return mixed|string
     */
    public static function getClassNameShort($toLower = false, $pluralize = false)
    {
        $fullClassName     = static::class;
        $explodedClassName = explode('\\', $fullClassName);
        $classNameShort    = end($explodedClassName);

        if ($toLower) {
            return $classNameShort;
        }

        return false === $pluralize ? mb_strtolower($classNameShort) : Inflector::pluralize($classNameShort);
    }

    /**
     * Get model hash
     *
     * @return string
     */
    public static function modelHash()
    {
        return hash('crc32', static::getClassNameShort());
    }

    /** @inheritdoc */
    public function rules()
    {
        return array_merge($this->useStatusTrait ? $this->statusRules() : [],
            $this->useTimestampTrait ? $this->timestampRules() : [],
            $this->useSortableTrait ? $this->sortableModelRules() : []);
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return ArrayHelper::merge($this->useTimestampTrait ? $this->timestampBehaviors() : [],
            $this->useSortableTrait ? $this->getSortableModelBehaviors() : []);
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge([static::primaryKey()[0] => '#'],
            $this->useStatusTrait ? $this->statusAttributeLabels() : [],
            $this->useTimestampTrait ? $this->timestampAttributeLabels() : [],
            $this->useSortableTrait ? $this->getSortableModelAttributeLabels() : []);
    }

    /**
     * Duplicate entries in the table.
     *
     * @return static|null
     */
    public function duplicate()
    {
        $model              = clone $this;
        $model->isNewRecord = true;

        foreach (static::primaryKey() as $key) {
            $model->{$key} = null;
        }

        return $model->save() ? $model : null;
    }

    /** @inheritdoc */
    public function load($data, $formName = null)
    {
        $loadResult = parent::load($data, $formName);

        return $loadResult && $this->checkRelationsLoad() ? $this->loadRelations($data, $formName) : $loadResult;
    }

    /** @inheritdoc */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $validateResult = parent::validate($attributeNames, $clearErrors);

        return $validateResult && $this->checkRelationsValidate() && !$this->isNewRecord ? $this->validateRelations($attributeNames) : $validateResult;
    }

    /** @inheritdoc */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!$this->checkRelationsSave()) {
            return parent::save($runValidation, $attributeNames);
        }

        return $this->saveAll($runValidation, $attributeNames);
    }

    /**
     * Check updated attribute
     *
     * @param string $attribute Attribute name
     *
     * @return bool Result
     */
    public function isChanged($attribute)
    {
        return null !== ($oldAttr = $this->getOldAttribute($attribute)) && $oldAttr !== $this->getAttribute($attribute);
    }

    /** @inheritdoc */
    public function fields()
    {
        return array_diff(parent::fields(), $this->excludeFields());
    }

    /**
     * Get model exclude fields
     *
     * @return array
     */
    public function excludeFields()
    {
        return [];
    }

    /** @inheritdoc */
    public function createdAttributeName()
    {
        return 'created_at';
    }

    /** @inheritdoc */
    public function updatedAttributeName()
    {
        return 'updated_at';
    }

    /** @inheritdoc */
    public function timeFormat()
    {
        return 'Y-m-d H:i:s';
    }

    /** @inheritdoc */
    public function statusAttributeName()
    {
        return 'status_key';
    }

    /** @inheritdoc */
    public function positionAttributeName()
    {
        return 'position';
    }

    /** @inheritdoc */
    public function getModelRelations()
    {
        return [];
    }

    /** @inheritdoc */
    protected function filterAttributes()
    {
        if ($this->useStatusTrait) {
            $this->getQuery()->andFilterWhere([
                $this->statusAttributeName() => $this->{$this->statusAttributeName()}
            ]);
        }

        if ($this->useTimestampTrait) {
            $this->getQuery()->andFilterWhere([
                'like',
                // to avoid error, if user entered specific symbols
                'CONVERT(' . $this->createdAttributeName() . ' USING utf8)',
                $this->{$this->createdAttributeName()}
            ]);
        }

        if ($this->useTimestampTrait && false !== $this->updatedAttributeName()) {
            $this->getQuery()->andFilterWhere([
                'like',
                // to avoid error, if user entered specific symbols
                'CONVERT(' . $this->updatedAttributeName() . ' USING utf8)',
                $this->{$this->updatedAttributeName()}
            ]);
        }

        if ($this->useSortableTrait) {
            $this->getQuery()->andFilterWhere([
                $this->positionAttributeName() => $this->{$this->positionAttributeName()}
            ]);
        }
    }

    /** @inheritdoc */
    protected function setProviderDefaultSort($dataProvider)
    {
        if ($this->useSortableTrait) {
            $dataProvider->sort->defaultOrder = [$this->positionAttributeName() => SORT_ASC];

            return $dataProvider;
        }

        if ($this->useTimestampTrait) {
            $dataProvider->sort->defaultOrder = [$this->createdAttributeName() => SORT_DESC];
        }

        return $dataProvider;
    }

    /**
     * @inheritdoc
     * @return ActiveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }
}

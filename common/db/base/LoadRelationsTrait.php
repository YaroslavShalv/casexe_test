<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 18:27
 */

namespace common\db\base;

use common\db\base\interfaces\ModelRelationsInterface;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;

/**
 * Class LoadRelationsTrait
 *
 * @package common\db\base
 *
 */
trait LoadRelationsTrait
{
    /**
     * Determines using model relations load
     *
     * @var bool
     */
    public $loadModelRelations = false;

    /** @return array */
    abstract public function getModelRelations();

    /**
     * Check if model relations must be loaded
     *
     * @return bool
     */
    protected function checkRelationsLoad()
    {
        return $this->loadModelRelations && count($this->getModelRelations()) > 0;
    }

    /**
     * @param      $data
     * @param null $formName
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    protected function loadRelations($data, $formName = null)
    {
        /* @var $this ActiveRecord|ModelRelationsInterface */

        $formName = null !== $formName ? $formName : $this->formName();

        if (false === array_key_exists($formName, $data)) {
            return false;
        }

        $data = $data[$formName];

        $loadResult = true;

        foreach ($this->getModelRelations() as $relAlias => $relName) {
            if (is_int($relAlias)) {
                $relAlias = $relName;
            }

            if (false === array_key_exists($relAlias,
                    $data) || $this->hasAttribute($relAlias) || !($relQuery = $this->getRelationQuery($relName))) {
                $loadResult = false;

                break;
            }

            // load relation model data
            $relationLoadResult = $relQuery->multiple ? $this->loadModels($relQuery,
                $data[$relAlias]) : $this->loadModel($relQuery, $data[$relAlias]);

            if (!$relationLoadResult) {
                $loadResult = false;

                break;
            }

            $this->populateRelation($relName, $relationLoadResult);
        }

        return $loadResult;
    }

    /**
     * @param string $relName
     *
     * @return bool|ActiveQueryInterface
     */
    abstract protected function getRelationQuery($relName);

    /**
     * @param ActiveQuery $relQuery
     * @param array       $data
     *
     * @return array|bool
     */
    private function loadModels($relQuery, $data)
    {
        $models = [];

        foreach ($data as $index => $modelData) {
            if (!is_int($index) || !($model = $this->loadModel($relQuery, $modelData))) {
                return false;
            }

            $models[] = $model;
        }

        return $models;
    }

    /**
     * @param ActiveQuery $relQuery
     * @param array       $data
     *
     * @return bool|ActiveRecord
     */
    private function loadModel($relQuery, $data)
    {
        $link = [];

        // get link attributes
        foreach ($relQuery->link as $_relationKey => $_relatedKey) {
            $link[$_relationKey] = $this->{$_relatedKey};
            //unset variables to avoid of replacing it from post
            unset($data[$_relationKey]);
        }

        $modelClass = $relQuery->modelClass;
        $model      = new $modelClass($link);

        foreach ($model::primaryKey() as $pkName) {
            if (isset($data[$pkName])) {
                $model[$pkName] = $data[$pkName];
            }
        }

        return $model->load($data, '') ? $model : false;
    }
}
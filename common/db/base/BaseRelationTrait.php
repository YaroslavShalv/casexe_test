<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 20:02
 */

namespace common\db\base;

use yii\db\ActiveQuery;

/**
 * Class BaseRelationTrait
 *
 * @package common\db\base
 */
trait BaseRelationTrait
{
    use LoadRelationsTrait;
    use ValidateRelationsTrait;
    use SaveRelationsTrait;

    /**
     * Check exist of relation getter and return query or false
     * if method not found or method does not return a query
     *
     * @param string $relName
     * @return bool|ActiveQuery
     */
    protected function getRelationQuery($relName)
    {
        // check getter with relation exist
        if (false === method_exists($this, ($relMethod = 'get' . ucfirst($relName)))) {
            return false;
        }

        // check getter returns ActiveQuery
        if (!(($relQuery = $this->{$relMethod}()) instanceof ActiveQuery)) {
            return false;
        }

        return $relQuery;
    }
}

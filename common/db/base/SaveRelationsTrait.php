<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 19:45
 */

namespace common\db\base;

use common\db\base\interfaces\ModelRelationsInterface;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Query;

/**
 * Class SaveRelationsTrait
 *
 * @package common\db\base
 *
 * @method boolean save
 * @method static Connection getDb()
 */
trait SaveRelationsTrait
{
    /**
     * Determines using model relations load
     *
     * @var bool
     */
    public $saveModelRelations = false;

    /** @return array */
    abstract public function getModelRelations();

    /**
     * Check if model relations must be loaded
     *
     * @return bool
     */
    protected function checkRelationsSave()
    {
        return $this->saveModelRelations && count($this->getModelRelations()) > 0;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function saveAll($runValidation = true, $attributeNames = null)
    {
        /* @var $this ActiveRecord|ModelRelationsInterface */

        $transaction = static::getDb()->beginTransaction();

        $saveResult = true;

        try {
            if (!parent::save($runValidation, $attributeNames)) {
                $transaction->rollBack();

                return false;
            }

            foreach ($this->getModelRelations() as $relName) {
                if (!$this->saveRelation($relName, $runValidation)) {
                    $saveResult = false;

                    break;
                }
            }

            $saveResult ? $transaction->commit() : $transaction->rollBack();
        } catch (Exception $e) {
            $transaction->rollBack();

            return false;
        }

        return $saveResult;
    }

    /**
     * @param string $relName
     * @return bool|ActiveQueryInterface
     */
    abstract protected function getRelationQuery($relName);

    /**
     * @param      $relName
     * @param bool $runValidation
     * @return bool
     * @throws \yii\db\Exception
     */
    private function saveRelation($relName, $runValidation = true)
    {
        /* @var $this ActiveRecord|ModelRelationsInterface */
        /* @var $relQuery ActiveQuery */

        if (!($relQuery = $this->getRelationQuery($relName)) || !isset($this->relatedRecords[$relName])) {
            return false;
        }

        $oldModels    = $relQuery->all();
        $resultModels = $notDeletePk = [];

        list($existedModels, $notExistedModels, $oldModels) = $this->sortModels($relName, $oldModels);

        // try to save existed models
        foreach ($existedModels as $existedModel) {
            /* @var $existedModel ActiveRecord */

            if (!$existedModel->save($runValidation)) {
                return false;
            }

            $notDeletePk    = $this->addNotDeletePk($existedModel, $notDeletePk);
            $resultModels[] = $existedModel;
        }

        unset($existedModels);

        // try to save not existed models
        foreach ($notExistedModels as $notExistedModel) {
            /* @var ActiveRecord $notExistedModel */

            // if exist not used oldModel, take not used model, set new attributes without pk
            if (count($oldModels) > 0) {
                $oldModel = array_pop($oldModels);
                $pk       = $notExistedModel::primaryKey();

                foreach ($notExistedModel->attributes as $_key => $_value) {
                    if (!in_array($_key, $pk)) {
                        $oldModel->{$_key} = $_value;
                    }
                }

                $notExistedModel = $oldModel;
            }

            foreach ($relQuery->link as $relationKey => $relatedKey) {
                $notExistedModel->{$relationKey} = $this->{$relatedKey};
            }

            if (!$notExistedModel->save($runValidation)) {
                return false;
            }

            $notDeletePk = $this->addNotDeletePk($notExistedModel, $notDeletePk);

            $resultModels[] = $notExistedModel;
        }

        unset($oldModels, $notExistedModels);

        $this->populateRelation($relName, $resultModels);

        $modelClass = $relQuery->modelClass;

        static::getDb()->createCommand()->delete($modelClass::tableName(),
            $this->generateDeleteCondition($notDeletePk, $relQuery))->execute();

        return true;
    }

    /**
     * @param string $relName
     * @param array  $oldModels
     * @return array
     */
    private function sortModels($relName, $oldModels)
    {
        /* @var ActiveRecord $this */

        $existedModels = $notExistedModels = [];

        // sort models
        // models without pk will putted in ine array
        // existed models will put in another array
        // after sort $oldModels will contain all unused models
        foreach ($this->relatedRecords[$relName] as $index => $relatedRecord) {
            if ($this->getIsPrimaryKeyEmpty($relatedRecord)) {
                $notExistedModels[$index] = $relatedRecord;

                continue;
            }

            foreach ($oldModels as $oldIndex => $oldModel) {
                if ($this->comparePrimaryKey($relatedRecord, $oldModel)) {
                    $oldModel->load($relatedRecord->attributes, '');
                    $existedModels[] = $oldModel;
                    unset($oldModels[$oldIndex]);
                }
            }
        }

        return [
            $existedModels,
            $notExistedModels,
            $oldModels,
        ];
    }

    /**
     * @param ActiveRecordInterface $modelFirst
     * @param ActiveRecordInterface $modelLast
     *
     * @return boolean
     */
    private function comparePrimaryKey($modelFirst, $modelLast)
    {
        foreach ($modelFirst->getPrimaryKey(true) as $key => $value) {
            if ($modelLast->{$key} != $value) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param ActiveRecord $model
     *
     * @return boolean
     */
    private function getIsPrimaryKeyEmpty($model)
    {
        foreach ($model->getPrimaryKey(true) as $_key => $_value) {
            if (empty($_value)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param ActiveRecordInterface $model
     * @param array                 $notDeletePk
     * @return mixed
     */
    private function addNotDeletePk($model, $notDeletePk)
    {
        foreach ($model->getPrimaryKey(true) as $_key => $_value) {
            $notDeletePk[$_key][] = $_value;
        }

        return $notDeletePk;
    }

    /**
     * @param array       $notDeletePk
     * @param ActiveQuery $relQuery
     *
     * @return array
     */
    private function generateDeleteCondition($notDeletePk, $relQuery)
    {
        // create query object to build a query where
        $query = new Query();

        foreach ($notDeletePk as $_key => $_items) {
            $query->andWhere(['not', [$_key => $_items]]);
        }

        foreach ($relQuery->link as $relatedKey => $relationKey) {
            $query->andWhere([$relationKey => $this->{$relationKey}]);
        }

        return $query->where;
    }
}
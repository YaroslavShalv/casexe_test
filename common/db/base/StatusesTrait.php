<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 13:45
 */

namespace common\db\base;

use common\db\base\interfaces\StatusesInterface;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\helpers\ArrayHelper;

/**
 * Class StatusesTrait
 *
 * @package common\models\base
 *
 * @method static string tableName()
 * @method static Connection getDb()
 *
 * @property $status array|mixed
 */
trait StatusesTrait
{
    /**
     * @param string $condition
     * @param array  $params
     * @return int
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    public static function approveAll($condition = '', array $params = [])
    {
        return static::changeStatusCommand(StatusesInterface::STATUS_ENABLED, $condition, $params);
    }

    /**
     * @param string $condition
     * @param array  $params
     * @return int
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    public static function disapproveAll($condition = '', array $params = [])
    {
        return static::changeStatusCommand(StatusesInterface::STATUS_DISABLED, $condition, $params);
    }

    /**
     * Get statuses list
     *
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getStatuses($key = null, $defaultValue = null)
    {
        $items = static::statusItemsList();

        return null !== $key ? ArrayHelper::getValue($items, $key, $defaultValue) : $items;
    }

    /**
     * Get status items list
     * if needed, can be redefined for adding new statuses
     *
     * @return array
     */
    public static function statusItemsList()
    {
        return [
            StatusesInterface::STATUS_ENABLED    => 'Опубликовано',
            StatusesInterface::STATUS_DISABLED   => 'Отклонено',
            StatusesInterface::STATUS_MODERATION => 'На модерации',
        ];
    }

    /**
     * @param        $status
     * @param string $condition
     * @param array  $params
     * @return int
     * @throws \ReflectionException
     * @throws \yii\db\Exception
     */
    protected static function changeStatusCommand($status, $condition = '', array $params = [])
    {
        $reflection = new \ReflectionClass(static::class);

        $command = static::getDb()->createCommand();
        $command->update(static::tableName(), [
            $reflection->newInstanceWithoutConstructor()->statusAttributeName() => $status
        ], $condition, $params);

        return $command->execute();
    }

    /** Get rules for statuses */
    public function statusRules()
    {
        $attribute = $this->statusAttributeName();

        return [
            [[$attribute], 'integer'],
            [[$attribute], 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    /**
     * Get status attribute labels
     *
     * @return array
     */
    public function statusAttributeLabels()
    {
        return [
            $this->statusAttributeName() => 'Статус',
            'status'                     => 'Статус',
        ];
    }

    /**
     * @param bool $runValidation
     *
     * @return bool
     */
    public function approve($runValidation = true)
    {
        $this->{$this->statusAttributeName()} = StatusesInterface::STATUS_ENABLED;

        return $this->save($runValidation);
    }

    /**
     * @param bool $runValidation
     *
     * @return bool
     */
    public function disapprove($runValidation = true)
    {
        $this->{$this->statusAttributeName()} = StatusesInterface::STATUS_DISABLED;

        return $this->save($runValidation);
    }

    /**
     * Get status name
     *
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public function getStatus($defaultValue = null)
    {
        /* @var $this ActiveRecord */

        return null !== $this->{$this->statusAttributeName()} ? static::getStatuses($this->{$this->statusAttributeName()},
            $defaultValue) : $defaultValue;
    }

    /**
     * Status key attribute name
     *
     * @return string
     */
    abstract public function statusAttributeName();
}

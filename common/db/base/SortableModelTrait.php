<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 15:27
 */

namespace common\db\base;

use common\behaviors\SortableGridBehavior;

/**
 * Class SortableModelTrait
 *
 * @package common\db\base
 */
trait SortableModelTrait
{
    /**
     * Get sortable model rules
     *
     * @return array
     */
    public function sortableModelRules()
    {
        return [
            [[$this->positionAttributeName()], 'integer'],
        ];
    }

    /**
     * Get sortable model behaviors
     *
     * @return array
     */
    public function getSortableModelBehaviors()
    {
        return [
            'sort' => [
                'class'             => SortableGridBehavior::class,
                'sortableAttribute' => $this->positionAttributeName(),
            ]
        ];
    }

    /**
     * Get sortable model attribute labels
     *
     * @return array
     */
    public function getSortableModelAttributeLabels()
    {
        return [
            $this->positionAttributeName() => 'Позиция'
        ];
    }

    /**
     * Get position attribute name
     *
     * @return string
     */
    abstract public function positionAttributeName();
}

<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 13:42
 */

namespace common\db\base;

use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

/**
 * Class SearchModelTrait
 *
 * @package common\models\base
 *
 * @method static ActiveQuery find()
 * @method static string class
 */
trait SearchModelTrait
{
    /**
     * Query builder object
     *
     * @var null|ActiveQuery
     */
    protected $_mainQuery;

    /**
     * Search models
     *
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->getQuery(),
        ]);

        $this->setProviderDefaultSort($dataProvider);

        if (false !== empty($params[static::getClassNameShort()])) {
            $this->load($params);
        } else {
            $this->setAttributes($params);
        }

        $this->filterAttributes();

        return $dataProvider;
    }

    /**
     * Build query with filter attributes
     *
     * @return null|ActiveQuery Query builder
     */
    public function buildQuery()
    {
        $this->_mainQuery = static::find();

        return $this->_mainQuery;
    }

    /**
     * Get main query
     *
     * @return null|ActiveQuery
     */
    public function getQuery()
    {
        return null !== $this->_mainQuery ? $this->_mainQuery : $this->buildQuery();
    }

    /**
     * Method for realize attributes sort in query
     */
    abstract protected function filterAttributes();

    /**
     * Method for set default sort for query
     *
     * @param DataProviderInterface $dataProvider
     * @return DataProviderInterface
     */
    abstract protected function setProviderDefaultSort($dataProvider);
}

<?php
/**
 * Created by PhpStorm.
 * User: OS
 * Date: 07.03.2017
 * Time: 9:59
 */

namespace common\db\base;

use common\db\base\interfaces\ModelRelationsInterface;
use Yii;
use yii\base\Model;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;

/**
 * Class ValidateRelationsTrait
 *
 * @package common\db\base
 */
trait ValidateRelationsTrait
{
    /**
     * Determines using model relations load
     *
     * @var bool
     */
    public $validateModelRelations = false;

    /**
     * @return array
     */
    abstract public function getModelRelations();

    /**
     * Check if model relations must be validated
     *
     * @return bool
     */
    protected function checkRelationsValidate()
    {
        /* @var $this ModelRelationsInterface */

        return $this->validateModelRelations && count($this->getModelRelations()) > 0;
    }

    /**
     * @param null $attributeNames
     *
     * @return boolean
     */
    protected function validateRelations($attributeNames = null)
    {
        /* @var $this ActiveRecord */

        $validateResult = true;

        foreach ($this->getModelRelations() as $relName) {
            if (!($relQuery = $this->getRelationQuery($relName))) {
                $this->addErrors([$relName => Yii::t('common', "Wrong relation name {name}!", ['name' => $relName])]);
                $validateResult = false;

                break;
            }

            if (is_array($attributeNames) && !in_array($relName, $attributeNames)) {
                continue;
            }

            $relRecords = $relQuery->multiple ? $this->{$relName} : [$this->{$relName}];

            if (!Model::validateMultiple($relRecords)) {
                $this->populateRelation($relName, $relRecords);

                $validateResult = false;
            }
        }

        return $validateResult;
    }

    /**
     * @param string $relName
     * @return bool|ActiveQueryInterface
     */
    abstract protected function getRelationQuery($relName);
}
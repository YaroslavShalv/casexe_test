<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 18:20
 */

namespace common\db\base\interfaces;

use yii\db\ActiveRecordInterface;

/**
 * Interface LoadRelationsInterface
 *
 * @package common\db\base
 */
interface ModelRelationsInterface extends ActiveRecordInterface
{
    /**
     * Get model relations list
     * Model must have this relation, what are implemented, like getter
     *
     * @return array
     */
    public function getModelRelations();
}
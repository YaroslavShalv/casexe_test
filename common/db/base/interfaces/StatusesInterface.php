<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 13:49
 */

namespace common\db\base\interfaces;

use yii\db\ActiveRecordInterface;

/**
 * Interface StatusesInterface
 *
 * @package common\models\base
 */
interface StatusesInterface extends ActiveRecordInterface
{
    /** Константы статусов */
    const STATUS_MODERATION = 0;
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 2;

    /**
     * Get statuses list
     *
     * @param null $key
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public static function getStatuses($key = null, $defaultValue = null);

    /**
     * Get status name
     *
     * @param null $defaultValue
     *
     * @return array|mixed
     */
    public function getStatus($defaultValue = null);
}
<?php
/**
 * Created by PhpStorm.
 * User: OS
 * Date: 30.03.2017
 * Time: 15:48
 */

namespace common\db\base\interfaces;

use yii\db\ActiveRecordInterface;

/**
 * Interface SeoModelInterface
 *
 * @package common\db\base\interfaces
 */
interface SeoModelInterface extends ActiveRecordInterface
{
    /**
     * Get model seo attributes
     * Must returns an array associative array, where key is name of seo attribute
     * and value is name of attribute in model
     *
     * @return array
     */
    public function getSeoAttributes();

    /**
     * Get model title
     *
     * @return string
     */
    public function getTitle();
}
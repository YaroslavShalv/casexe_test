<?php
/**
 * Created by PhpStorm.
 * User: EG
 * Date: 13.03.2017
 * Time: 19:29
 */

namespace common\db\base;

use common\models\ModelCount;

/**
 * Class ModelCountTrait
 *
 * @property ModelCount $likes
 * @property ModelCount $views
 * @property integer    $likeCount
 * @property integer    $viewsCount
 * @package common\db\base
 */
trait ModelCountTrait
{
    /** @return \yii\db\ActiveQuery */
    public function getLikes()
    {
        return $this->hasOne(ModelCount::class, ['identity_id' => 'id'])->where([
            'identity'    => static::modelHash(),
            'identity_id' => $this->id,
            'type'        => ModelCount::TYPE_LIKE,
        ]);
    }

    /** @return \yii\db\ActiveQuery */
    public function getViews()
    {
        return $this->hasOne(ModelCount::class, ['identity_id' => 'id'])->where([
            'identity'    => static::modelHash(),
            'identity_id' => $this->id,
            'type'        => ModelCount::TYPE_VIEW,
        ]);
    }

    /** @return int */
    public function getLikeCount()
    {
        return (int)$this->getLikes()->select(['count'])->scalar();
    }

    /** @return int */
    public function getViewsCount()
    {
        return (int)$this->getViews()->select(['count'])->scalar();
    }
}
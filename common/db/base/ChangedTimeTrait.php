<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 14:15
 */

namespace common\db\base;

use common\helpers\DateHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class ChangedTimeTrait
 *
 * @package common\models\base
 *
 * @property boolean $timestampBehaviorEnabled
 * @property string  $created_at
 * @property string  $updated_at
 * @property string  $updated
 * @property string  $created
 */
trait ChangedTimeTrait
{
    /**
     * Created at attribute name
     *
     * @var string
     */
    public static $createdAtAttribute = 'created_at';

    /**
     * Updated at attribute name
     *
     * @var string
     */
    public static $updatedAtAttribute = 'updated_at';

    /**
     * Created at and update attributes time format
     *
     * @var string
     */
    public static $createdAtTimeFormat = 'Y-m-d H:i:s';

    /**
     * Enable behavior for created_at and updated_at attributes
     *
     * @var bool
     */
    public $timeStampBehaviorEnabled = true;

    public $createdAtStart;
    public $createdAtEnd;

    public $updatedAtStart;
    public $updatedAtEnd;

    /**
     * Get changed time attributes rules
     *
     * @return array
     */
    public function changedTimeRules()
    {
        /* @var $this ActiveRecord */

        $items = [];

        if ($this->hasAttribute(static::$createdAtAttribute)) {
            $items[] = ['createdAtStart', 'safe'];
            $items[] = ['createdAtEnd', 'safe'];
            $items[] = [static::$createdAtAttribute, 'string'];
        }

        if ($this->hasAttribute(static::$updatedAtAttribute)) {
            $items[] = ['updatedAtStart', 'safe'];
            $items[] = ['updatedAtEnd', 'safe'];
            $items[] = [static::$updatedAtAttribute, 'string'];
        }

        return $items;
    }

    /**
     * Get changed time behaviors
     *
     * @return array
     */
    public function changedTimeBehaviors()
    {
        /* @var $this ActiveRecord */

        $items = [];

        if ($this->timeStampBehaviorEnabled && $this->hasProperty(static::$createdAtAttribute)) {
            $items['date'] = [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => static::$createdAtAttribute,
                'updatedAtAttribute' => $this->hasProperty(static::$updatedAtAttribute) ? static::$updatedAtAttribute : static::$createdAtAttribute,
                'value'              => function () {
                    return date(static::$createdAtTimeFormat);
                },
            ];
        }

        return $items;
    }

    /**
     * Get changed time attribute labels
     *
     * @return array
     */
    public function changedTimeAttributeLabels()
    {
        /* @var $this ActiveRecord */

        $items = [];

        if ($this->hasAttribute(static::$createdAtAttribute)) {
            $items[static::$createdAtAttribute] = 'Дата создания';
            $items['created']                   = 'Дата создания';
        }

        if ($this->hasAttribute(static::$updatedAtAttribute)) {
            $items[static::$updatedAtAttribute] = 'Дата обновления';
            $items['updated']                   = 'Дата обновления';
        }

        return $items;
    }

    /**
     * Дата создания
     *
     * @param string $format
     * @param bool   $plural
     * @param null   $defaultValue
     *
     * @return string
     */
    public function getCreated($format = 'd M Y', $plural = true, $defaultValue = null)
    {
        if (empty($this->{static::$createdAtAttribute})) {
            return $defaultValue;
        }

        return DateHelper::dateFormatted($this->{static::$createdAtAttribute}, $format, $plural);
    }

    /**
     * Дата обновления
     *
     * @param string $format
     * @param bool   $plural
     * @param null   $defaultValue
     *
     * @return string
     */
    public function getUpdated($format = 'd M Y', $plural = true, $defaultValue = null)
    {
        if (empty($this->{static::$updatedAtAttribute})) {
            return $defaultValue;
        }

        return DateHelper::dateFormatted($this->{static::$updatedAtAttribute}, $format, $plural);
    }

    /** @return false|string */
    public function getCreatedDate()
    {
        $timeStampDate = date_create_from_format(static::$createdAtTimeFormat, $this->{static::$createdAtAttribute});

        return false !== $timeStampDate ? date('d.m.Y в H:i', $timeStampDate->getTimestamp()) : '';
    }
}
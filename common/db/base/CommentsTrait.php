<?php
/**
 * Created by PhpStorm.
 * User: EG
 * Date: 15.03.2017
 * Time: 17:08
 */

namespace common\db\base;

use yii2mod\comments\models\CommentModel;
use yii2mod\moderation\enums\Status;

/**
 * Class CommentsTrait
 *
 * @property CommentModel[] $comments
 * @property integer        $commentsCount
 * @package common\db\base
 */
trait CommentsTrait
{

    /** @return mixed */
    public function getComments()
    {
        return $this->hasMany(CommentModel::class, ['entityId' => 'id'])->where([
            'entity'   => self::modelHash(),
            'entityId' => $this->id,
            'status'   => Status::APPROVED,
        ]);
    }

    /** @return mixed */
    public function getCommentsCount()
    {
        return $this->getComments()->count();
    }

}
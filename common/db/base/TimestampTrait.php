<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.03.17
 * Time: 14:15
 */

namespace common\db\base;

use common\helpers\DateHelper;
use yii\behaviors\TimestampBehavior;

/**
 * Class TimestampTrait
 *
 * @package common\models\base
 *
 * @property string $updated
 * @property string $created
 */
trait TimestampTrait
{
    /**
     * Get changed time attributes rules
     *
     * @return array
     */
    public function timestampRules()
    {
        return array_merge([
            [[$this->createdAttributeName()], 'string'],
        ], false !== $this->updatedAttributeName() ? [
            [[$this->updatedAttributeName()], 'string']
        ] : []);
    }

    /**
     * Get changed time behaviors
     *
     * @return array
     */
    public function timestampBehaviors()
    {
        return [
            'timestamp' => [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => $this->createdAttributeName(),
                'updatedAtAttribute' => $this->updatedAttributeName(),
                'value'              => function () {
                    return date($this->timeFormat());
                },
            ],
        ];
    }

    /**
     * Get changed time attribute labels
     *
     * @return array
     */
    public function timestampAttributeLabels()
    {
        return array_merge([
            $this->createdAttributeName() => 'Дата создания',
            'created'                     => 'Дата создания',
        ], false !== $this->updatedAttributeName() ? [
            $this->updatedAttributeName() => 'Дата обновления',
            'updated'                     => 'Дата обновления',
        ] : []);
    }

    /**
     * Дата создания
     *
     * @param string $format
     * @param bool   $plural
     * @param null   $defaultValue
     *
     * @return string
     */
    public function getCreated($format = 'd M Y', $plural = true, $defaultValue = null)
    {
        return null !== $this->{$this->createdAttributeName()} ? DateHelper::dateFormatted($this->{$this->createdAttributeName()},
            $format, $plural) : $defaultValue;
    }

    /**
     * Дата обновления
     *
     * @param string $format
     * @param bool   $plural
     * @param null   $defaultValue
     *
     * @return string
     */
    public function getUpdated($format = 'd M Y', $plural = true, $defaultValue = null)
    {
        return null !== $this->{$this->updatedAttributeName()} ? DateHelper::dateFormatted($this->{$this->updatedAttributeName()},
            $format, $plural) : $defaultValue;
    }

    /**
     * Get created attribute name
     *
     * @return string
     */
    abstract public function createdAttributeName();

    /**
     * Get updated attribute name
     *
     * @return string|false
     */
    abstract public function updatedAttributeName();

    /**
     * Get time used time format
     *
     * @return string
     */
    abstract public function timeFormat();
}

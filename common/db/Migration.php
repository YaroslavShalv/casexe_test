<?php

namespace common\db;

use yii\base\InvalidConfigException;

/**
 * Base migration with implemented safeDown and dropTable
 *
 * @package common\db
 */
class Migration extends \yii\db\Migration
{
    /** @var string Table name for migrate */
    protected $_tableName;

    /** @var string Table options for migrate */
    protected $_tableOptions;

    /** @var bool добавить колонку created_at */
    protected $_table_created_at = true;

    /** @var bool добавить колонку updated_at */
    protected $_table_updated_at = true;

    /** @var bool добавить колонку status_key */
    protected $_table_status_key = true;

    /** @var int значение по-умолчанию для колонки status_key */
    protected $_table_status_key_default = 0;

    /** @throws \yii\base\InvalidConfigException */
    public function init()
    {
        if (is_null($this->_tableName)) {
            throw new InvalidConfigException('$_tableName must be set!');
        }

        if ($this->db->driverName === 'mysql' && $this->_tableOptions !== false) {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $this->_tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        parent::init();
    }

    /**
     * Метод создания таблицы
     * Добавляет поля по-умолчанию
     *
     * @param string $table
     * @param array  $columns
     * @param null   $options
     */
    public function createTable($table, $columns, $options = null)
    {
        if ($this->_table_status_key && !isset($columns['status_key'])) {
            $columns['status_key'] = $this->smallInteger(1)->notNull()->defaultValue($this->_table_status_key_default)->comment('Статус');
        }
        if ($this->_table_created_at && !isset($columns['created_at'])) {
            $columns['created_at'] = $this->dateTime()->notNull()->comment('Дата создания');
        }
        if ($this->_table_updated_at && !isset($columns['updated_at'])) {
            $columns['updated_at'] = $this->dateTime()->notNull()->comment('Дата редактирования');
        }

        parent::createTable($table, $columns, $options);
    }

    /** Migration down */
    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}

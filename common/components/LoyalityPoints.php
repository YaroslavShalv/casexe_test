<?php

namespace common\components;


use common\models\LoyalityPoints as LoyalityPointsModel;

class LoyalityPoints
{

    /**
     * @param $bonus
     * @param $userId
     * @return bool
     */
    public function addBonus($bonus, $userId)
    {
        $model = $this->getBonusByUserId($userId);

        $model->points += $bonus;

        return $model->save();
    }


    /**
     * @param $id
     * @return LoyalityPointsModel
     */
    public function getBonusByUserId($id)
    {
        $model = LoyalityPointsModel::find()->where(['user_id' => $id])->one();
        if(empty($model)){
            $model = new LoyalityPointsModel();
            $model->user_id = $id;
            $model->points = 0;
            $model->save();
        }

        return $model;
    }
}
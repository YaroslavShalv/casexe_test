<?php

namespace common\components\customExport\exportXlsx;

use common\components\customExport\BaseFileExport;
use common\components\customExport\xlsxwriter\XLSXWriter;
use common\helpers\ArrayHelper;
use common\helpers\FileHelper;

/**
 * Class XlsxExport
 * @package common\components\customExport\exportXlsx
 *
 * @inheritdoc
 * @property string $currentSheetName
 * @property string $tempDir
 */
class XlsxExport extends BaseFileExport
{
    private $currentSheetName = 'book_1';
    private $tempDir = 'book_1';

    /**
     * @var XLSXWriter
     */
    private $excelWriter = null;

    /**
     * @return array
     */
    public function generate()
    {

        if(!$this->fileExists()) {
            $this->is_new = true;
        }

        $this->setXlsxWriter();
        $this->writeTitles();
        $this->writeData();
        $this->saveFile();

        return $this->returnJson();
    }


    /**
     * @return mixed
     */
    private function saveFile()
    {
        if ($this->expect < $this->total) {
            file_put_contents($this->getFilePath(), serialize($this->excelWriter));
        }
        else {
            $this->excelWriter->writeToFile($this->getFilePath());
            $this->excelWriter->clearTempData();
        }

        return true;
    }

    /**
     *
     */
    private function writeData()
    {
        $exportData = $this->getData();

        foreach ($exportData as $_key => $_row) {
            $this->excelWriter->writeSheetRow($this->currentSheetName, $_row, $this->dataConfigs);
        }
    }

    /**
     * @return bool
     */
    private function writeTitles()
    {
        if(!$this->is_new) {
            return false;
        }

        if ($this->excelWriter instanceof XLSXWriter) {
            $headersNames = $this->getTitles();
            $variableTypes = $this->variableTypes;

            $headers = [];
            foreach ($headersNames as $k => $name) {
                $headers[$name] = ArrayHelper::getValue($variableTypes, $k, 'string');
            }

            $this->excelWriter->writeSheetHeader($this->currentSheetName, $headers, $this->titleConfigs);
        }

        return true;
    }

    /**
     * @return XLSXWriter
     */
    private function setXlsxWriter()
    {

        if (is_file($this->getFilePath())) {
            $this->excelWriter = unserialize(file_get_contents($this->getFilePath()));
        }
        else {
            $this->tempDir = dirname($this->getFilePath());
            FileHelper::createDirectory($this->tempDir);

            $this->excelWriter = new XLSXWriter(FILE_APPEND);
            $this->excelWriter->setTempDir($this->tempDir);
        }

        return $this->excelWriter;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return 'xlsx';
    }
}
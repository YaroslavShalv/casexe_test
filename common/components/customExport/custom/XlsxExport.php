<?php

namespace common\components\customExport\custom;

use common\components\customExport\BaseFileExport;

class XlsxExport extends BaseFileExport
{
    /** @var \PHPExcel */
    private $objPHPExcel = null;

    /**
     * @return array
     */
    public function generate()
    {

        if(!$this->fileExists()) {
//            $this->writeBom();
            $this->is_new = true;
        }

        $this->setObjPHPExcel();
        $this->setActiveSheet(0);
        $this->writeTitles();
        $this->writeData();
        $this->saveFile();

        return $this->returnJson();
    }


    /**
     * @return mixed
     */
    private function saveFile()
    {
        $objWriter = new \PHPExcel_Writer_Excel2007($this->objPHPExcel);
        $objWriter->save($this->getFilePath());
    }

    /**
     * @return \PHPExcel_Worksheet
     */
    private function writeData()
    {
        $lastRow = $this->objPHPExcel->getActiveSheet()->getHighestRow()+1;
        return $this->objPHPExcel->getActiveSheet()->fromArray($this->getData(), NULL, 'A' . $lastRow);
    }

    /**
     * @return bool
     */
    private function writeTitles()
    {
        if(!$this->is_new) {
            return false;
        }

        $this->objPHPExcel->getActiveSheet()->fromArray($this->getTitles(), 'A1');
    }


    /**
     * @param $index
     * @return \PHPExcel_Worksheet
     */
    private function setActiveSheet($index)
    {
        return $this->objPHPExcel->setActiveSheetIndex($index);
    }

    /**
     * @return \PHPExcel
     */
    private function setObjPHPExcel()
    {
        if($this->is_new){
            $objPHPExcel = new \PHPExcel();
        } else {
            $objPHPExcel = \PHPExcel_IOFactory::load($this->getFilePath());
        }
        return $this->objPHPExcel = $objPHPExcel;
    }


    /**
     * @return string
     */
    public function getExtension()
    {
        return 'xlsx';
    }
}
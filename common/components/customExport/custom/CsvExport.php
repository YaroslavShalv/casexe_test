<?php

namespace common\components\customExport\custom;

use common\components\customExport\BaseFileExport;

class CsvExport extends BaseFileExport
{
    const DELIMETER = ';';

    /**
     * @var resource
     */
    private $file;

    /**
     * @return array
     */
    public function generate()
    {
        if(!$this->fileExists()) {
            $this->writeTitles();
        }

        foreach ($this->getData() as $row) {
            $this->writeRow($row);
        }

        $this->closeFile();

        return $this->returnJson();
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return 'csv';
    }

    /**
     * @return resource
     */
    private function openFile()
    {
        return $this->file = fopen($this->getFilePath(), 'a');
    }

    /**
     * @return bool
     */
    private function closeFile()
    {
        return !empty($this->getFile()) ? fclose($this->getFile()) : false;
    }


    /**
     * @return resource
     */
    private function getFile()
    {
        return empty($this->file) ? $this->openFile() : $this->file;
    }


    /**
     * @return int
     */
    private function writeTitles()
    {
        fwrite($this->getFile(), "\xEF\xBB\xBF");
        return $this->writeRow($this->getTitles());
    }

    /**
     * @param $row
     * @return int
     */
    private function writeRow($row)
    {
        return fputcsv($this->getFile(), $row, self::DELIMETER);
    }

}
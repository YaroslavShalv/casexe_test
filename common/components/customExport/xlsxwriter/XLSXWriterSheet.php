<?php


namespace common\components\customExport\xlsxwriter;

/**
 * @property string $fileName
 * @property string $sheetName
 * @property string $xmlName
 * @property integer $rowsCount
 * @property XLSXWriterBufferWriter $fileWriter
 * @property array $columns
 * @property array $mergeCells
 * @property integer $maxCellTagStart
 * @property integer $maxCellTagEnd
 * @property boolean $finalized
 */
class XLSXWriterSheet
{
    public $fileName;
    public $sheetName;
    public $xmlName;
    public $rowsCount = 0;
    public $fileWriter;
    public $columns = [];
    public $mergeCells = [];
    public $maxCellTagStart = 0;
    public $maxCellTagEnd = 0;
    public $finalized = false;

    /**
     * XLSXWriter_Sheet constructor.
     * @param string $sheetName
     * @param string $fileName
     * @param string $xmlName
     */
    public function __construct($sheetName, $fileName, $xmlName)
    {
        $this->sheetName = $sheetName;
        $this->fileName = $fileName;
        $this->xmlName = $xmlName;

        $this->fileWriter = new XLSXWriterBufferWriter($this->fileName);
    }
}
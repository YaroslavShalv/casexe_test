<?php


namespace common\components\customExport\xlsxwriter;


/**
 * @property resource $fd
 * @property string $buffer
 * @property boolean $checkUtf8
 * @property string $fileName
 */
class XLSXWriterBufferWriter
{
    protected $fd = null;
    protected $buffer = '';
    protected $checkUtf8 = false;
    protected $fileName;

    /**
     * XLSXWriterBufferWriter constructor.
     * @param string $filename
     * @param string $fopenMode
     * @param bool $checkUtf8
     */
    public function __construct($filename, $fopenMode = 'w', $checkUtf8 = false)
    {
        $this->fileName = $filename;
        $this->checkUtf8 = $checkUtf8;

        $this->fd = fopen($filename, $fopenMode);
        if ($this->fd === false) {
            XLSXWriter::log("Unable to open $filename for writing.");
        }
    }

    public function __destruct()
    {
        $this->close();
    }

    public function __sleep()
    {
        $this->close();

        return ['fileName', 'checkUtf8', 'buffer'];
    }

    public function __wakeup()
    {
        $this->fd = fopen($this->fileName, 'r+');
        if ($this->fd !== false) {
            fseek($this->fd, 0, SEEK_END);
        }
    }

    /**
     * @param string $string
     */
    public function write($string)
    {
        $this->buffer .= $string;
        if (isset($this->buffer[8191])) {
            $this->purge();
        }
    }

    public function close()
    {
        $this->purge();
        if ($this->fd) {
            fclose($this->fd);
            $this->fd = null;
        }
    }

    /**
     * @return bool|int
     */
    public function ftell()
    {
        if ($this->fd) {
            $this->purge();
            return ftell($this->fd);
        }
        return -1;
    }

    /**
     * @param int $pos
     * @return int
     */
    public function fseek($pos)
    {
        if ($this->fd) {
            $this->purge();
            return fseek($this->fd, $pos);
        }
        return -1;
    }

    protected function purge()
    {
        if ($this->fd) {
            if ($this->checkUtf8 && !self::isValidUTF8($this->buffer)) {
                XLSXWriter::log("Error, invalid UTF8 encoding detected.");
                $this->checkUtf8 = false;
            }
            fwrite($this->fd, $this->buffer);
            $this->buffer = '';
        }
    }

    /**
     * @param string $string
     * @return bool
     */
    protected static function isValidUTF8($string)
    {
        if (function_exists('mb_check_encoding')) {
            return mb_check_encoding($string, 'UTF-8') ? true : false;
        }

        return preg_match("//u", $string) ? true : false;
    }
}
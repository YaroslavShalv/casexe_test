<?php

namespace common\components\customExport\spout;

use Box\Spout\Common\Type;

class SpoutCsvExport extends SpoutExport
{
    /**
     * @return string
     */
    public function getType()
    {
        return Type::CSV;
    }


    /**
     * @return string
     */
    public function getExtension()
    {
        return 'csv';
    }
}
<?php

namespace common\components\customExport\spout;

use Box\Spout\Common\Type;

class SpoutXslxExport extends SpoutExport
{
    /**
     * @return string
     */
    public function getType()
    {
        return Type::XLSX;
    }


    /**
     * @return string
     */
    public function getExtension()
    {
        return 'xlsx';
    }
}
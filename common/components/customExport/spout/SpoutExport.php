<?php

namespace common\components\customExport\spout;

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\WriterInterface;
use common\components\customExport\BaseFileExport;

abstract class SpoutExport extends BaseFileExport
{
    /**
     * @var WriterInterface
     */
    protected $writer;

    /**
     * @return array
     */
    public function generate()
    {

        if(!$this->fileExists()) {
            $this->is_new = true;
        }

        $this->setWriter();
        $this->writeTitles();
        $this->writeData();
        $this->saveFile();

        return $this->returnJson();
    }


    private function saveFile()
    {
        $this->writer->close();
    }


    private function writeData()
    {
        $this->writer->addRows($this->getData());
    }

    /**
     * @return bool
     */
    private function writeTitles()
    {
        if(!$this->is_new) {
            return false;
        }

        $this->writer->addRow($this->getTitles());
    }


    /**
     * @return \Box\Spout\Writer\WriterInterface
     */
    private function setWriter()
    {
        $this->writer = WriterFactory::create($this->getType());
        $this->writer->openToFile($this->getFilePath());
        return $this->writer;
    }


    abstract  public function getType();
}
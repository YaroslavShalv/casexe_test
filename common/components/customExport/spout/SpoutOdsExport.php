<?php

namespace common\components\customExport\spout;

use Box\Spout\Common\Type;

class SpoutOdsExport extends SpoutExport
{
    /**
     * @return string
     */
    public function getType()
    {
        return Type::ODS;
    }


    /**
     * @return string
     */
    public function getExtension()
    {
        return 'ods';
    }
}
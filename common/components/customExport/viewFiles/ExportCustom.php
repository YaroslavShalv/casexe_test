<?php

namespace common\components\customExport\viewFiles;


use yii\web\AssetBundle;
class ExportCustom extends AssetBundle {

    public $sourcePath = '@common/components/customExport/viewFiles';

    public $css = [
        'export/export.css',
    ];
    public $js = [
        'export/export.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
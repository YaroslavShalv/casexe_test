var custom_export = new CustomExport();


function CustomExport() {

    this.step = 500; //limit
    this.offset = 0;
    this.total = 0;
    this.fileUrl = '';
    this.name = '';
    this.type = 'csv';

    this.loading = false;

    this.init = function () {
        $('#custom-export-type').select2({"allowClear":false,"theme":"krajee","width":"100%","placeholder":"","language":"ru-RU"});
    };


    this.paramToUrl = function (obj) {
        var str = "";
        for (var key in obj) {
            if (str != "") {
                str += "&";
            }
            str += key + "=" + encodeURIComponent(obj[key]);
        }
        return window.location.search ? '&' + str : '?' + str;
    };

    this.openPopup = function () {
        console.log('test');
        var self = this;
        var data = {
            cexport_action : "custom-export-total",
        };

        this.hideBlocks();
        this.request(data, function (msg) {
            self.total = msg.total;

            $('#all_rows').text(msg.total);
            $("#export-modal-custom").modal('show');
        });
    };

    this.startExport = function () {
        this.offset = 0;
        this.name = (new Date()).getTime();
        this.type =  $("#custom-export-type").val();
        this.updateProgressbar();
        this.hideBlocks();

        $('.progress-block').show();

        this.generateFile();
    };


    this.generateFile = function () {
        var self = this;
        console.log('tad');
        console.log(this.offset + ' ' + this.total);

        if(this.offset >= this.total || this.step == null) {
            self.showResult('done');
            return false;
        }

        var data = {
            cexport_limit : this.step,
            cexport_offset: this.offset,
            cexport_type : this.type,
            cexport_filename : this.name,
            cexport_action : "custom-export",
        };

        this.request(data, function (msg) {
            console.log(msg);
            if(msg.response == 'ok') {
                self.offset += self.step;
                self.fileUrl = msg.url;
                self.updateProgressbar();
                self.generateFile();
            } else {
                self.showResult('error');
            }
        });

    };

    this.hideBlocks = function () {
        $('.progress-block').hide();
        $('.result-block').hide();
        $('.alert').hide();
    };


    this.showResult = function (type) {
        $('.progress-block').hide();
        if(type == 'error') {
            $('.result-block').show();
            $('.alert-danger').show();
        }

        if(type == 'done') {
            $('#file_link').attr('href', this.fileUrl).text(this.fileUrl);
            $('.result-block').show();
            $('.alert-success').show();
        }

    };

    this.updateProgressbar = function () {
        var persent = Math.ceil(100 / this.total * this.offset);
        var offset = this.offset > this.total ?  this.total : this.offset;

        $("#exported_rows").text(offset);
        $('.export-progress .progress-bar').css('width', persent + "%");
    };


    this.request = function (data, callback) {
        var self = this;
        if(this.loading) {
            return false;
        }
        this.loading = true;

        $.ajax({
            method: "GET",
            url: window.location.href + self.paramToUrl(data),
            // dataType: 'json'
        }).done(function (msg) {
            self.loading = false;
            callback(msg);
        }).fail(function (msg) {
            self.loading = false;
        });
    }

}


window.onLoad = function (e) {
   custom_export.init();
};
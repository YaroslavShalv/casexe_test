<?php
use common\components\customExport\CustomExport;

\common\components\customExport\viewFiles\ExportCustom::register($this);
?>

<div id="export-modal-custom" class="modal fade bd-example-modal-lg" tabindex="2" role="dialog" aria-labelledby="label" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title" id="myLargeModalLabel">Экспорт</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-export">
                    <div class="row">
                        <div class="col-md-4" >
                            <label for="custom-export-type">Выберите тип файла: </label>
                            <div class="export_select_block">
                                <select id="custom-export-type">
                                    <?php foreach (CustomExport::getTypes() as $k => $type) : ?>
                                        <option value="<?= $k ?>"><i class="text-primary fa fa-file-code-o"></i><?= $type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button onclick="custom_export.startExport()"  id="exportByType" class="btn btn-primary">Экспорт</button>
                        </div>
                    </div>
                </div>

                <div class="progress-block">
                    <div class="export-progress progress">
                        <div class="progress-bar-striped progress-bar progress-bar-success progress-bar-animated active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                    </div>
                    <div>
                        <span id="exported_rows">0</span>/<span id="all_rows">0</span>
                    </div>
                </div>
                <div class="result-block">
                    <div class="alert alert-success" role="alert">
                        Данные удачно экспортированы. Ссылка на файл: <a id="file_link" href="export.php" target="_blank">export.php</a>
                    </div>
                    <div class="alert alert-danger" role="alert">
                        Ошибка экспорта
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
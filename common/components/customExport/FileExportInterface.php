<?php

namespace common\components\customExport;


interface FileExportInterface
{
    public function setName($name);

    public function getName();

    public function setTitles($titles);

    public function getTitles();

    public function setData($data);

    public function getData();

    public function generate();

    public function setPath($path);

    public function getPath();

    public function getExtension();

    public function getFilePath();

    public function fileExists();
}


?>
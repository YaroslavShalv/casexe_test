<?php

namespace common\components\customExport;

use common\components\customExport\custom\CsvExport;
use common\components\customExport\custom\XlsxExport;
use common\components\customExport\spout\SpoutCsvExport;
use common\components\customExport\spout\SpoutOdsExport;
use common\components\customExport\spout\SpoutXslxExport;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;


/**
 * Class CustomExport
 * @package common\components
 *
 * @property string $driver
 * @property ActiveDataProvider $dataProvider
 * @property string $type
 * @property string $fileName
 * @property int $limit
 * @property int $offset
 * @property string|null $action
 * @property array $config
 * @property array $titles
 * @property array $data
 * @property object $queryResult
 * @property array $variableTypes
 * @property array $titleConfigs
 * @property array $dataConfigs
 */
class CustomExport
{
    /**
     * Extensions
     */
    const TYPE_CSV = 'csv';
    const TYPE_EXCEL = 'excel';
    const TYPE_ODS = 'ods';

    /**
     * Ajax actions
     */
    const ACTION_TOTAL = 'custom-export-total';
    const ACTION_EXPORT = 'custom-export';

    /**
     * File generation type
     */
    const DRIVER_CUSTOM = 'custom';
    const DRIVER_SPOUT = 'spout';
    const DRIVER_XLSX_WRITER = 'XLSXWriter';

    /**
     * @var string custo
     */
    public $driver = self::DRIVER_XLSX_WRITER;
    /**
     * @var ActiveDataProvider|null
     */
    public $dataProvider = null;
    /**
     * @var string
     */
    public $type = self::TYPE_CSV;
    /**
     * @var int
     */
    public $limit = 500;
    /**
     * @var int
     */
    public $offset = 0;
    /**
     * @var string|null
     */
    public $action = null;
    /**
     * @var string
     */
    public $fileName;
    /**
     * @var array
     */
    public $config = null;
    /**
     * @var array
     */
    public $titles = [];
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var object
     */
    public $queryResult;
    /**
     * @var array
     */
    public $variableTypes = [];
    /**
     * @var array
     */
    public $titleConfigs = [];
    /**
     * @var array
     */
    public $dataConfigs = [];



    /**
     * CustomExport constructor.
     * @param ActiveDataProvider $dataProvider
     * @param string $config
     */
    public function __construct(ActiveDataProvider $dataProvider, $config)
    {
        $this->driver = self::getDriver();

        $get = Yii::$app->request->get();

        $this->dataProvider = $dataProvider;
        $this->setConfig($config);

        $this->type = ArrayHelper::getValue($get, 'cexport_type', self::TYPE_CSV);
        $this->action = ArrayHelper::getValue($get, 'cexport_action', null);
        $this->limit = ArrayHelper::getValue($get, 'cexport_limit', null);
        $this->offset = ArrayHelper::getValue($get, 'cexport_offset', 0);
        $this->fileName = ArrayHelper::getValue($get,'cexport_filename', microtime(true));
    }


    public static function getDriver()
    {
        return ArrayHelper::getValue(Yii::$app->params, 'customExportDriver', self::DRIVER_XLSX_WRITER);
    }


    /**
     * @param null|string $driver
     * @return array|mixed
     */
    public function getDrivers($driver = null)
    {
        $drivers = [
            self::DRIVER_CUSTOM => 'Custom',
            self::DRIVER_SPOUT => 'Spout',
            self::DRIVER_XLSX_WRITER => 'XLSXWriter',
        ];

        return !is_null($driver) ? ArrayHelper::getValue($drivers, $driver, self::DRIVER_CUSTOM) : $drivers;
    }

    /**
     * @param $as_number
     * @return array|int
     */
    public function getTotal($as_number = null)
    {
        $total = $this->dataProvider->query->limit(null)->offset(null)->count();
        return !is_null($as_number) ? $total : ['total' => $total];
    }


    /**
     * @return array
     */
    public function makeAction()
    {
        $result = ['error' => 'undefined action'];
        if($this->action == self::ACTION_TOTAL) {
            $result = $this->getTotal();
        }

        if($this->action == self::ACTION_EXPORT) {
            $result = $this->generateFile();
        }

        return $result;
    }


    /**
     * @param null $type
     * @return array|mixed
     */
    public static function getTypes($type = null)
    {
        $types = [
            self::TYPE_CSV => 'CSV',
            self::TYPE_EXCEL => 'XLSX',
        ];

        if(self::getDriver() == self::DRIVER_SPOUT) {
            $types[self::TYPE_ODS] = 'ODS';
        }

        return !is_null($type) ? ArrayHelper::getValue($types, $type, null) : $types;
    }


    /**
     * @return array
     */
    public function generateFile()
    {
        $this->prepareData();
        if($this->driver == self::DRIVER_CUSTOM) {
            if($this->type == self::TYPE_CSV) {
                $model = new CsvExport();
            } elseif($this->type == self::TYPE_EXCEL) {
                $model = new XlsxExport();
            } else {
                return ['error' => 'wrong file format'];
            }
        }
        if($this->driver == self::DRIVER_XLSX_WRITER) {
            if($this->type == self::TYPE_CSV) {
                $model = new CsvExport();
            } elseif($this->type == self::TYPE_EXCEL) {
                $model = new \common\components\customExport\exportXlsx\XlsxExport();
            } else {
                return ['error' => 'wrong file format'];
            }
        } elseif($this->driver == self::DRIVER_SPOUT) {
            if($this->type == self::TYPE_CSV) {
                $model = new SpoutCsvExport();
            } elseif($this->type == self::TYPE_EXCEL) {
                $model = new SpoutXslxExport();
            } elseif($this->type == self::TYPE_ODS) {
                $model = new SpoutOdsExport();
            } else {
                return ['error' => 'wrong file format'];
            }
        } else {
            return ['error' => 'wrong driver'];
        }

        /** @var $model BaseFileExport */
        $model->setTitles($this->titles);
        $model->setData($this->data);
        $model->setName($this->fileName);
        $model->setTitleConfig($this->titleConfigs);
        $model->setDataConfig($this->dataConfigs);
        $model->setTotal($this->getTotal(true));
        $model->setExpect((int)$this->offset + (int)$this->limit);
        $model->setVariableTypes($this->variableTypes);

        return $model->generate();
    }


    public function prepareData()
    {
        $query = $this->dataProvider->query;

        if(!empty($this->limit)) {
            $query->limit($this->limit)->offset($this->offset);
        } else {
            $query->limit(null)->offset(null);
        }

        $this->queryResult = $query->all();

        $this->prepareTitles();

        $data = [];
        foreach ($this->queryResult as $result) {
            $tmpArray = [];
            foreach ($this->getConfig() as $c) {
                if(empty($c['value'])) {
                    $tmpArray[] = ArrayHelper::getValue($result, $c['attribute'], null);
                }elseif(!empty($c['value']) && is_string($c['value'])) {
                    $tmpArray[] = ArrayHelper::getValue($result, $c['value'], null);
                }elseif(!empty($c['value'])) {
                    $tmpArray[] = $c['value']($result);
                }
            }

            $data[] = $tmpArray;
        }

        return $this->data = $data;
    }

    /**
     * @return array
     */
    public function prepareTitles()
    {
        $titles = [];
        $firstQueryResult = ArrayHelper::getValue($this->queryResult, 0, null);

        if(null === $firstQueryResult) {
            return $titles;
        }


        foreach ($this->getConfig() as $c) {
            if(!empty($c['header'])) {
                $titles[] = $c['header'];
            } elseif(!empty($c['attribute'])) {
                $titles[] = $firstQueryResult->getAttributeLabel($c['attribute']);
            } else {
                $titles[] = '';
            }

            $this->variableTypes[] = ArrayHelper::getValue($c, 'variableType', 'string');
            $this->titleConfigs[] = ArrayHelper::getValue($c, 'headerConfig', ['font-style'=>'bold']);
            $this->dataConfigs[] = ArrayHelper::getValue($c, 'dataConfig', []);

        }


        return $this->titles = $titles;
    }


    /**
     * @return bool
     */
    public function checkGenerate()
    {
        return in_array($this->action, $this->getActionTypes()) ? true : false;
    }



    /**
     * @param $key
     * @return array|mixed
     */
    public function getActionTypes($key = null)
    {
        $array = [
            self::ACTION_TOTAL => self::ACTION_TOTAL,
            self::ACTION_EXPORT => self::ACTION_EXPORT,
        ];

        return !is_null($key) ? ArrayHelper::getValue($array, $key, null) : $array;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = require($config);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

}
<?php

namespace common\components\customExport;

use common\helpers\FileHelper;
use Faker\Provider\File;
use Yii;
use yii\helpers\Url;

/**
 * Class BaseFileExport
 * @package common\components\customExport
 *
 * @property string $name
 * @property array $titles
 * @property array $data
 * @property string $path
 * @property bool $is_new
 * @property string $fileUrl
 * @property integer $total
 * @property integer $expect
 * @property array $variableTypes
 * @property array $titleConfigs
 * @property array $dataConfigs
 */
abstract class BaseFileExport implements FileExportInterface
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var array
     */
    protected $titles;
    /**
     * @var array
     */
    protected $data;
    /**
     * @var string
     */
    protected $path;
    /**
     * @var bool
     */
    protected $is_new;
    /**
     * @var string
     */
    protected $fileUrl;
    /**
     * @var integer
     */
    protected $total;
    /**
     * @var integer
     */
    protected $expect;
    /**
     * @var array
     */
    protected $variableTypes;
    /**
     * @var array
     */
    protected $titleConfigs;
    /**
     * @var array
     */
    protected $dataConfigs;


    /**
     * @param $titleConfigs
     */
    public function setTitleConfig($titleConfigs)
    {
        $this->titleConfigs = $titleConfigs;
    }


    /**
     * @param $dataConfigs
     */
    public function setDataConfig($dataConfigs)
    {
        $this->dataConfigs = $dataConfigs;
    }


    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name . '.' . $this->getExtension();
    }


    /**
     * @param $variableTypes
     */
    public function setVariableTypes($variableTypes)
    {
        $this->variableTypes = $variableTypes;
    }


    /**
     * @param $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }


    /**
     * @param $expect
     */
    public function setExpect($expect)
    {
        $this->expect = $expect;
    }


    /**
     * @return string
     */
    public function getName()
    {
        if(empty($this->name)) {
            $this->setName(microtime(true));
        }

        return $this->name;
    }


    /**
     * @param $titles
     */
    public function setTitles($titles)
    {
        $this->titles = $titles;
    }


    /**
     * @return array
     */
    public function getTitles()
    {
        return $this->titles;
    }


    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    abstract public function generate();


    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->getPath() . $this->getName();
    }


    /**
     * @return bool
     */
    public function fileExists()
    {
        return file_exists($this->getFilePath());
    }


    /**
     * @param $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    /**
     * @return string
     */
    public function getFileUrl()
    {
        return '/uploads/' . $this->fileUrl;
    }


    /**
     * @return array
     */
    public function returnJson()
    {
        return ['url' => Url::to($this->getFileUrl(), true), 'response' => 'ok'];
    }


    /**
     * @return string
     */
    public function getPath()
    {
        $relativePath = empty($this->path) ? 'files/' . $this->getExtension() . '/' . FileHelper::dynamicPath(time()) . '/' : FileHelper::normalizePath($this->path);
        $this->fileUrl = $relativePath . $this->getName();
        $path = Yii::getAlias('@frontend/web/uploads/') . $relativePath;

        FileHelper::createDirectory($path);
        return $path;
    }

    /**
     * @return string
     */
    abstract public function getExtension();
}
<?php

namespace common\components;

use common\helpers\ArrayHelper;
use common\models\prizes\MoneyPrize;

/**
 * Class ConvertingComponent
 * @package common\components
 *
 * @property array $configs
 */
class ConvertingComponent
{
    private $configs;

    const RATIO_KEY = 'loyaloty_point_ratio';

    /**
     * @param $amount
     * @return int
     */
    public function convert($amount)
    {
        if($amount <= 0) {
            return 0;
        }

        $ratio = ArrayHelper::getValue($this->getConfigs(), self::RATIO_KEY, 1);

        return round($amount * $ratio);
    }

    /**
     * @return array Configs
     */
    public function getConfigs()
    {
        if(!empty($this->configs)){
            return $this->configs;
        }

        return $this->configs = MoneyPrize::getConfigs();
    }
}
<?php

namespace common\components;

use common\helpers\ArrayHelper;
use common\helpers\HttpError;
use common\helpers\RandomHelper;
use common\models\ConvertingJob;
use common\models\LotteryConfig;
use common\models\prizes\base\BasePrizeModel;
use common\models\prizes\BonusPointsPrize;
use common\models\prizes\ItemPrize;
use common\models\prizes\MoneyPrize;
use common\models\PrizesHistory;

class PrizeGenerator
{
    /**
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function generatePrize()
    {
        $prizeType = self::getRandomPrizeType();
        if(is_null($prizeType)) {
            HttpError::the404('Призы закончились');
        }

        return  self::savePrizeHistory(new $prizeType);
    }


    /**
     * @param $prizeClass BasePrizeModel
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function savePrizeHistory($prizeClass)
    {
        $prizeClass->randomPrize();
        $data = [
            'prize_id' => $prizeClass->getPrizeId(),
            'user_id' => \Yii::$app->user->id,
            'prize_key' => $prizeClass::getPrizeType(),
            'amount' => $prizeClass->getAmount(),
            'status_key' => 0,
            'name' => $prizeClass->getName(),
            'element_name' => $prizeClass->getItemName(),
        ];

        $model = new PrizesHistory();
        $model->load($data, '');

        if($model->save()) {
            $data['id'] = $model->id;
            $prizeClass->sendPrize();
            return $data;
        } else {
            HttpError::the403('Ошибка сохранения истории');
        }
    }


    /**
     * @return null|BasePrizeModel
     */
    public static function getRandomPrizeType()
    {
        $prizeTypes = [];
        foreach (self::getPrizesTypesList() as $type) {
            if($type::isAvailable()) {
                $prizeTypes[] = $type;
            }
        }

        $randomKey = RandomHelper::getRandom(0, count($prizeTypes) - 1); //-1 потому что нумерация с 0

        return ArrayHelper::getValue($prizeTypes, $randomKey, null);
    }


    /**
     * @param $type
     * @return null|BasePrizeModel
     */
    public function getBasePrizeModelByType($type)
    {
        foreach (self::getPrizesTypesList() as $prizeElement)
        {
            if($prizeElement::getPrizeType() == $type) {
                return new $prizeElement;
            }
        }

        return null;
    }


    /**
     * @return BasePrizeModel[]
     */
    public static function getPrizesTypesList()
    {
        return [
//            BonusPointsPrize::class,
//            ItemPrize::class,
            MoneyPrize::class,
        ];
    }


    /**
     * @param $id
     * @throws \yii\web\NotFoundHttpException
     */
    public function addConvertJob($id)
    {
        $model = $this->getPrizeHistoryById($id);

        $convertModel = new ConvertingJob();

        $convertModel->user_id = $model->user_id;
        $convertModel->amount = $model->amount;
        $convertModel->status_key = ConvertingJob::STATUS_NEW;
        $convertModel->save();

        $model->status_key = PrizesHistory::CONVERSION;
        $model->save();

        MoneyPrize::returnMoney($id);
    }


    /**
     * @param $id
     * @return PrizesHistory
     * @throws \yii\web\NotFoundHttpException
     */
    public function getPrizeHistoryById($id)
    {
        $model = PrizesHistory::find()->where(['id' => $id])
            ->andWhere(['status_key' => PrizesHistory::STATUS_MODERATION])->one();

        if(empty($model)) {
            HttpError::the404('Приз не найден');
        }

        return $model;
    }


    /**
     * @param $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function addApprove($id)
    {
        $model = $this->getPrizeHistoryById($id);

        $baseModel = $this->getBasePrizeModelByType($model->prize_key);

        return $baseModel->savePrize($model);
    }


    /**
     * @param $id
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function discard($id)
    {
        $model = $this->getPrizeHistoryById($id);

        $baseModel = $this->getBasePrizeModelByType($model->prize_key);

        return $baseModel->discardPrize($model);
    }

}

